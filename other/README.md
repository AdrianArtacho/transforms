# TransForms #

![repo:TRAN:standalone-neutral](https://docs.google.com/drawings/d/e/2PACX-1vRwD0HPaghh-rLQa12_s9XefENCDDVbUC9kKhfmh2OI1Pkg1uwLq0bkXVgV3vOAptmRdn9QQNProKKk/pub?w=927&h=130)

Here is the links to the live.device (OS-agnostic):

[TransForms M4L device](https://drive.google.com/file/d/1n2fc7KhcXs0aX6z7RFfFmx9BeL5TLq9E/view?usp=sharing)

[comment]: # ( [TransForms standalone (MacOS) ](https://drive.google.com/file/d/1S77-di_IMzQWnBZw3znFr_aIa_J3Zepk/view?usp=sharing) )


(*) In order to have polyphonic Pitch Bend, the Max For Live device needs to be used together with additional routing devices.

work in progress... - - -
 
## OSC messages to & from TransForms App ##
 
 OSC messages from the PolytempoNetwork App to the TransForms App use the port 22574 and these are the ones implemented so far:
 
 **TransForms app**
 (OSC port IN 22574)
 
 ![repo:TRAN:osc-reference](https://docs.google.com/drawings/d/e/2PACX-1vQWFXDnAn2Izj7OO08ttLzMsfLh8o6jFuQMR6tZIT3D30tYDaUEFiAhUT2UgWLHshmg1VEB_d5ffRu5/pub?w=996&h=252)
 
