# ToDo #

## Working prototype ##
Highest priority features, essential bugs.

- make [TransForms] work with tuning devices (pitch bend type, etc.)
- make more elaborated .ptsco with Markers
- {addSender} manually to the score
- parse .ptsco? stick to .txt?


## Features ##
Desired, could be postponed to next working prototype.

- [PTN-control] encapsulate the different possible processing of incoming data as abstractions, and have a way (menu) to choose between them.


## Low priority fixes ##



## Improvements ##
Low priority, prettify.

- [shear_kslider] doesn't load the saved keyboard layouts on startup (standalone/m4l)
- Interface crammed, make some room. Have mein options/umenus left to the keyboards
- bach score visualisation, right to the keyboards block
- make layout of the README links consistent overall
- have 1 [live.thisdevice] initialize everything in the patch
- the bach module seems unnecessary, and doesn't work in the standalone...