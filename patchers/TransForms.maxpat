{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 2,
			"revision" : 2,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 204.0, 116.0, 1296.0, 960.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"workspacedisabled" : 1,
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-223",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1436.0, 1061.0, 111.0, 22.0 ],
					"text" : "14 103, 26 5, 116 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-225",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 1115.0, 931.0, 34.0, 22.0 ],
					"text" : "sel 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-224",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 195.0, 223.0, 150.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 64.0, 115.5, 59.0, 20.0 ],
					"text" : "Panic!"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-221",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1115.0, 986.0, 70.0, 22.0 ],
					"text" : "s MidiPanic"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-219",
					"maxclass" : "live.button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1115.5, 957.0, 15.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 48.0, 117.5, 15.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "off", "on" ],
							"parameter_invisible" : 2,
							"parameter_longname" : "live.button",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.button",
							"parameter_type" : 2
						}

					}
,
					"varname" : "live.button"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-220",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1238.0, 1133.0, 50.0, 22.0 ],
					"text" : "/tf/P 6."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-186",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1268.5, 1055.5, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 11.0,
					"format" : 6,
					"id" : "obj-190",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1211.5, 1055.5, 51.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 69.75, 93.0, 30.0, 21.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-215",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1211.5, 1086.5, 77.0, 22.0 ],
					"text" : "prepend /tf/P"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.741176470588235, 0.168627450980392, 0.870588235294118, 1.0 ],
					"id" : "obj-217",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1211.5, 977.5, 45.0, 22.0 ],
					"text" : "r ---CC"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-218",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 181.0, 200.0, 628.0, 488.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-25",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 241.0, 43.0, 32.0, 22.0 ],
									"text" : "21 6"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-22",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 186.0, 284.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 186.0, 152.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 87.0, 43.0, 32.0, 22.0 ],
									"text" : "21 2"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 186.0, 326.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 186.0, 192.0, 53.0, 22.0 ],
									"text" : "route 21"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 427.0, 206.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 427.0, 165.0, 57.0, 22.0 ],
									"text" : "route CC"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 380.0, 123.0, 66.0, 22.0 ],
									"text" : "route done"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-182",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 327.0, 44.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.501960784313725, 0.227450980392157, 0.227450980392157, 1.0 ],
									"id" : "obj-180",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 327.0, 13.0, 107.0, 22.0 ],
									"text" : "r ---start-sequence"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 327.0, 89.0, 72.0, 22.0 ],
									"text" : "patcherargs"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 186.0, 83.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-10", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 1 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-182", 0 ],
									"source" : [ "obj-180", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-182", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-2", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "ksliderWhite",
								"default" : 								{
									"color" : [ 1, 1, 1, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjBlue-1",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-1",
								"default" : 								{
									"accentcolor" : [ 0, 0.533333, 0.168627, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 1211.5, 1006.5, 101.0, 35.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p route-PRESET @CC 21"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-216",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1059.0, 1013.0, 50.0, 22.0 ],
					"text" : "21 6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-214",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1019.5, 961.0, 69.0, 22.0 ],
					"text" : "prepend 21"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-212",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1019.5, 931.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-211",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 917.0, 1081.0, 104.0, 22.0 ],
					"text" : "/tf/centernote \"72\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-207",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 917.0, 1044.0, 95.0, 22.0 ],
					"text" : "/tf/centernote 70"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-206",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 731.5, 727.0, 71.0, 22.0 ],
					"text" : "fromsymbol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-196",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 649.5, 535.0, 30.0, 22.0 ],
					"text" : "\"69\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-203",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1289.5, -3.0, 97.0, 35.0 ],
					"text" : ";\rmax maxwindow"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-194",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1289.5, -41.0, 37.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 106.75, 115.5, 31.5, 17.0 ],
					"text" : "debug"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1248.0, 511.0, 104.0, 22.0 ],
					"text" : "print OSC-receive"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-188",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1404.5, 936.0, 54.0, 22.0 ],
					"text" : "deferlow"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1404.5, 980.0, 75.0, 22.0 ],
					"text" : "print sibelius"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-181",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1404.5, 833.0, 35.0, 22.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.470588235294118, 0.470588235294118, 0.470588235294118, 1.0 ],
					"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "color",
					"gradient" : 1,
					"id" : "obj-108",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1404.5, 769.0, 70.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 948.0, 142.0, 60.0, 22.0 ],
					"text" : "CC string",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-82",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1404.5, 873.0, 51.0, 22.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 2611.0, 783.0, 276.0, 233.0 ],
						"bglocked" : 0,
						"openinpresentation" : 1,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-122",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1059.24370551109314, 668.008404731750488, 182.0, 22.0 ],
									"text" : "sprintf ~C101\\,0 ~C100\\,0 ~C6\\,%i"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.345098039215686, 0.345098039215686, 0.345098039215686, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-120",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1011.336141705513, 748.0, 168.907563805580139, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 74.151269316673279, 186.0, 188.848730683326721, 22.0 ],
									"text" : "~C101, 0 ~C100, 0 ~C6, 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-119",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 711.24370551109314, 491.218492448329926, 49.5, 22.0 ],
									"text" : "t i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-118",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 711.24370551109314, 459.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-121",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 711.24370551109314, 668.008404731750488, 83.0, 22.0 ],
									"text" : "sprintf ~B0\\,%i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-103",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 729.74370551109314, 430.285718977451324, 31.0, 22.0 ],
									"text" : "int 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-99",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 880.24370551109314, 293.218492448329926, 72.0, 22.0 ],
									"text" : "prepend set"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-95",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 1205.5, 427.0, 37.0, 22.0 ],
									"text" : "* 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-88",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 1156.0, 427.0, 41.0, 22.0 ],
									"text" : "* -100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-87",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "bang", "int", "int", "int" ],
									"patching_rect" : [ 1106.5, 380.672273218631744, 118.0, 22.0 ],
									"text" : "t b i i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-86",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1194.25629448890686, 327.033613264560699, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-84",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 763.49370551109314, 289.0, 29.5, 22.0 ],
									"text" : "64"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-82",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 741.74370551109314, 611.218492448329926, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-80",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 741.74370551109314, 531.0, 134.0, 22.0 ],
									"text" : "scale 0. 127. -100. 100."
								}

							}
, 							{
								"box" : 								{
									"fontface" : 1,
									"fontsize" : 11.0,
									"id" : "obj-78",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 645.28992360830307, 344.033613264560699, 63.0, 19.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 74.151269316673279, 146.0, 68.0, 19.0 ],
									"text" : "PB value",
									"textjustification" : 1
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 16.0,
									"id" : "obj-79",
									"maxclass" : "live.numbox",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 711.24370551109314, 340.033613264560699, 68.0, 23.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 74.151269316673279, 161.033613264560699, 68.0, 23.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_linknames" : 1,
											"parameter_longname" : "PBvalue",
											"parameter_shortname" : "pb",
											"parameter_type" : 0,
											"parameter_unitstyle" : 0
										}

									}
,
									"varname" : "PBvalue"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 880.24370551109314, 577.0, 60.0, 22.0 ],
									"text" : "clip 0 127"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-76",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 880.24370551109314, 611.218492448329926, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 880.24370551109314, 531.0, 127.0, 22.0 ],
									"text" : "scale -100. 100. 0 128"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1106.5, 289.0, 70.0, 22.0 ],
									"text" : "loadmess 1"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 1,
									"fontsize" : 9.0,
									"id" : "obj-68",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1054.5, 347.033613264560699, 55.0, 17.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.151269316673279, 189.0, 41.0, 17.0 ],
									"text" : "Range",
									"textjustification" : 1
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 16.0,
									"id" : "obj-69",
									"maxclass" : "live.numbox",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 1106.5, 344.033613264560699, 68.0, 23.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 39.151269316673279, 186.0, 33.0, 23.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "PB-range",
											"parameter_shortname" : "range",
											"parameter_type" : 0,
											"parameter_unitstyle" : 0
										}

									}
,
									"varname" : "PB-range"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 1,
									"fontsize" : 11.0,
									"id" : "obj-67",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 810.28992360830307, 344.033613264560699, 62.71007639169693, 19.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 4.151269316673279, 146.0, 68.0, 19.0 ],
									"text" : "PB (cents)",
									"textjustification" : 1
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-63",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 983.092436194419861, 301.033613264560699, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-64",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 971.092436194419861, 340.033613264560699, 32.0, 22.0 ],
									"text" : "65.5"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.345098039215686, 0.345098039215686, 0.345098039215686, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-65",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 663.336141705513, 748.0, 66.907563805580139, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 144.092436194419861, 161.033613264560699, 118.907563805580139, 22.0 ],
									"text" : "~B0, 0"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 16.0,
									"id" : "obj-66",
									"maxclass" : "live.numbox",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 880.24370551109314, 340.033613264560699, 68.0, 23.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 4.151269316673279, 161.033613264560699, 68.0, 23.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_linknames" : 1,
											"parameter_longname" : "sib-centernote[1]",
											"parameter_mmax" : 1200.0,
											"parameter_mmin" : -1200.0,
											"parameter_shortname" : "sib/cn",
											"parameter_type" : 0,
											"parameter_unitstyle" : 0
										}

									}
,
									"varname" : "sib-centernote[1]"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.345098039215686, 0.345098039215686, 0.345098039215686, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-60",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 691.24370551109314, 165.0, 105.907563805580139, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 74.151269316673279, 109.0, 188.848730683326721, 22.0 ],
									"text" : "/tf/centernote 65.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-59",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 2,
											"revision" : 2,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 1474.0, 56.0, 1612.0, 960.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-20",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 95.352943241596222, 711.600840926170349, 29.5, 22.0 ],
													"text" : "i"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-18",
													"linecount" : 3,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 41.0, 809.0, 50.0, 49.0 ],
													"text" : "/tf/centernote 65.5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-15",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 493.0, 153.0, 29.5, 22.0 ],
													"text" : "96."
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-13",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 420.0, 89.0, 32.0, 22.0 ],
													"text" : "68.3"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-2",
													"maxclass" : "number",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 95.352943241596222, 473.915965139865875, 33.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 95.352943241596222, 507.915965139865875, 52.0, 22.0 ],
													"text" : "gate 2 1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-112",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 128.352943241596222, 809.0, 95.0, 22.0 ],
													"text" : "/tf/centernote $1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-55",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 128.352943241596222, 777.915965139865875, 19.0, 22.0 ],
													"text" : "t l"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-50",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 266.352943241596222, 777.915965139865875, 19.0, 22.0 ],
													"text" : "t l"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-47",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 266.352943241596222, 671.600840926170349, 139.0, 22.0 ],
													"text" : "sprintf ~C14\\,%i ~C86\\,%i"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-46",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 266.352943241596222, 608.915965139865875, 37.0, 22.0 ],
													"text" : "zl.rev"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-45",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 317.28781732916832, 531.915965139865875, 29.5, 22.0 ],
													"text" : "i"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-44",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 317.28781732916832, 496.915965139865875, 40.0, 22.0 ],
													"text" : "* 100."
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-43",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 266.352943241596222, 570.915965139865875, 70.0, 22.0 ],
													"text" : "pack i i"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-36",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 287.201673924922943, 425.915965139865875, 29.5, 22.0 ],
													"text" : "1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-34",
													"maxclass" : "number",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 203.352943241596222, 473.915965139865875, 33.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-27",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 203.352943241596222, 501.915965139865875, 82.0, 22.0 ],
													"text" : "gate 2 1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-19",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 287.28781732916832, 393.915965139865875, 24.0, 24.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-17",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 287.28781732916832, 361.915965139865875, 49.0, 22.0 ],
													"text" : "route 0."
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 287.28781732916832, 313.915965139865875, 43.5, 22.0 ],
													"text" : "- 0."
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 311.78781732916832, 271.915965139865875, 29.5, 22.0 ],
													"text" : "i"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-121",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 203.352943241596222, 711.600840926170349, 90.0, 22.0 ],
													"text" : "sprintf ~C86\\,%i"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-120",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 5,
													"outlettype" : [ "float", "int", "float", "float", "int" ],
													"patching_rect" : [ 243.635506197810173, 99.0, 109.869748175144196, 22.0 ],
													"text" : "t f i f f 2"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-56",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 243.635481881136911, 38.999999139865878, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-57",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 266.35291892492296, 859.915965139865875, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-58",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 128.352921749778716, 859.915965139865875, 30.0, 30.0 ]
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-18", 1 ],
													"order" : 1,
													"source" : [ "obj-112", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-58", 0 ],
													"order" : 0,
													"source" : [ "obj-112", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-17", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-120", 2 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"midpoints" : [ 344.005254372954369, 128.0, 104.852943241596222, 128.0 ],
													"order" : 1,
													"source" : [ "obj-120", 4 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-27", 1 ],
													"source" : [ "obj-120", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 1 ],
													"source" : [ "obj-120", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-34", 0 ],
													"midpoints" : [ 344.005254372954369, 135.915965139865875, 212.852943241596222, 135.915965139865875 ],
													"order" : 0,
													"source" : [ "obj-120", 4 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-120", 3 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-50", 0 ],
													"source" : [ "obj-121", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-120", 0 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-120", 0 ],
													"source" : [ "obj-15", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-19", 0 ],
													"source" : [ "obj-17", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-44", 0 ],
													"source" : [ "obj-17", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-36", 0 ],
													"source" : [ "obj-19", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-55", 0 ],
													"source" : [ "obj-20", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-121", 0 ],
													"source" : [ "obj-27", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-43", 0 ],
													"source" : [ "obj-27", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-20", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-55", 0 ],
													"source" : [ "obj-3", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-27", 0 ],
													"source" : [ "obj-34", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"order" : 1,
													"source" : [ "obj-36", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-34", 0 ],
													"order" : 0,
													"source" : [ "obj-36", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-46", 0 ],
													"source" : [ "obj-43", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-45", 0 ],
													"source" : [ "obj-44", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-43", 1 ],
													"source" : [ "obj-45", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-47", 0 ],
													"source" : [ "obj-46", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-50", 0 ],
													"source" : [ "obj-47", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 1 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-57", 0 ],
													"source" : [ "obj-50", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-112", 0 ],
													"source" : [ "obj-55", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-120", 0 ],
													"source" : [ "obj-56", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 778.151269316673279, 121.008404731750488, 75.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p centernote"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 1,
									"fontsize" : 11.0,
									"id" : "obj-49",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 699.336141705513, 78.033613264560699, 76.815127611160278, 19.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 4.151269316673279, 109.0, 68.0, 19.0 ],
									"text" : "Centernote"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 910.0, 40.0, 29.5, 22.0 ],
									"text" : "67."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 898.0, 79.0, 32.0, 22.0 ],
									"text" : "65.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 171.0, 632.0, 71.0, 22.0 ],
									"text" : "fromsymbol"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 171.0, 548.0, 93.0, 22.0 ],
									"text" : "substitute 44 32"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-23",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 434.0, 94.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-22",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 171.0, 820.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 54.0, 567.0, 50.0, 22.0 ],
									"text" : "67"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 171.0, 449.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.345098039215686, 0.345098039215686, 0.345098039215686, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-123",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 786.24370551109314, 189.0, 66.907563805580139, 35.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 74.151269316673279, 86.033613264560699, 188.848730683326721, 22.0 ],
									"text" : "~C14, 50 ~C86, 65"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 16.0,
									"id" : "obj-115",
									"maxclass" : "live.numbox",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 778.151269316673279, 78.033613264560699, 68.0, 23.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 4.151269316673279, 86.033613264560699, 68.0, 23.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_linknames" : 1,
											"parameter_longname" : "sib-centernote",
											"parameter_shortname" : "sib/cn",
											"parameter_type" : 0,
											"parameter_unitstyle" : 1
										}

									}
,
									"varname" : "sib-centernote"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-110",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 584.008410394191742, 223.529414296150208, 81.932773470878601, 22.0 ],
									"text" : "~C116, 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-108",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 558.798326075077057, 174.0, 90.0, 22.0 ],
									"text" : "sprintf ~C%i\\,%i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-107",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 558.798326075077057, 121.008404731750488, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-106",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 604.17647784948349, 84.03361439704895, 50.0, 22.0 ],
									"text" : "116 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 171.0, 730.411770701408386, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-102",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 171.0, 589.873955070972443, 40.0, 22.0 ],
									"text" : "itoa"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-101",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 135.0, 380.672273218631744, 55.0, 22.0 ],
									"text" : "t l l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-100",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 135.0, 491.218492448329926, 55.0, 22.0 ],
									"text" : "zl.slice 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-97",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 171.0, 414.285718977451324, 57.0, 22.0 ],
									"text" : "zl.sub 67"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-93",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 299.159667253494263, 370.58823949098587, 185.29411917924881, 22.0 ],
									"text" : "67 49 49 54 32 44 32 48"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-91",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "list" ],
									"patching_rect" : [ 135.0, 334.453785300254822, 40.0, 22.0 ],
									"text" : "atoi"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-90",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 135.0, 276.470591366291046, 89.0, 22.0 ],
									"text" : "ASCII-split 126"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-89",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 317.0, 263.470591366291046, 113.865546941757202, 22.0 ],
									"text" : "\"C116 , 0\""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 80.0, 94.0, 24.0, 24.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 4.0, 2.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 80.0, 233.0, 74.0, 23.0 ],
									"text" : "route bang"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 80.0, 201.0, 66.0, 23.0 ],
									"text" : "route text"
								}

							}
, 							{
								"box" : 								{
									"autoscroll" : 0,
									"bangmode" : 1,
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-16",
									"keymode" : 1,
									"linecount" : 2,
									"lines" : 1,
									"maxclass" : "textedit",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "int", "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 80.0, 141.0, 238.0, 55.0 ],
									"presentation" : 1,
									"presentation_linecount" : 2,
									"presentation_rect" : [ 4.0, 28.0, 259.0, 52.0 ],
									"tabmode" : 0,
									"text" : "~C14,103 ~C26,5 ~C14,15 ~C115,0 ~C14,6 ~C116,0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 156.0, 29.0, 372.0, 20.0 ],
									"text" : "~C117,1 ~C14,103 ~C26,5 ~C14,50 ~C115,0 ~C116,1"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-100", 1 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 1 ],
									"source" : [ "obj-100", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-100", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-100", 0 ],
									"source" : [ "obj-101", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 0 ],
									"source" : [ "obj-101", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-102", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-118", 0 ],
									"source" : [ "obj-103", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-107", 0 ],
									"order" : 0,
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"order" : 1,
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-107", 0 ],
									"source" : [ "obj-106", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 1 ],
									"order" : 0,
									"source" : [ "obj-107", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"order" : 1,
									"source" : [ "obj-107", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-110", 1 ],
									"source" : [ "obj-108", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-115", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"source" : [ "obj-115", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-119", 0 ],
									"source" : [ "obj-118", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 0 ],
									"source" : [ "obj-119", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 0 ],
									"source" : [ "obj-119", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-65", 1 ],
									"source" : [ "obj-121", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-120", 1 ],
									"source" : [ "obj-122", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-90", 0 ],
									"source" : [ "obj-20", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-115", 0 ],
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-102", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-123", 1 ],
									"source" : [ "obj-59", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 1 ],
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"source" : [ "obj-63", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"source" : [ "obj-64", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"source" : [ "obj-66", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"source" : [ "obj-69", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-69", 0 ],
									"source" : [ "obj-70", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-77", 0 ],
									"source" : [ "obj-74", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-79", 0 ],
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-76", 0 ],
									"source" : [ "obj-77", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-103", 1 ],
									"order" : 0,
									"source" : [ "obj-79", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-118", 0 ],
									"order" : 1,
									"source" : [ "obj-79", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-82", 0 ],
									"source" : [ "obj-80", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"source" : [ "obj-82", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-79", 0 ],
									"source" : [ "obj-84", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-69", 0 ],
									"source" : [ "obj-86", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-103", 0 ],
									"midpoints" : [ 1116.0, 417.0, 739.24370551109314, 417.0 ],
									"source" : [ "obj-87", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-122", 0 ],
									"source" : [ "obj-87", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-88", 0 ],
									"source" : [ "obj-87", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"source" : [ "obj-87", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 1 ],
									"order" : 0,
									"source" : [ "obj-88", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 3 ],
									"order" : 1,
									"source" : [ "obj-88", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-89", 1 ],
									"order" : 0,
									"source" : [ "obj-90", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-91", 0 ],
									"order" : 1,
									"source" : [ "obj-90", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-101", 0 ],
									"order" : 1,
									"source" : [ "obj-91", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-93", 1 ],
									"order" : 0,
									"source" : [ "obj-91", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-101", 0 ],
									"source" : [ "obj-93", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 2 ],
									"order" : 0,
									"source" : [ "obj-95", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 4 ],
									"order" : 1,
									"source" : [ "obj-95", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-97", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"source" : [ "obj-99", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1404.5, 906.0, 61.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p Sibelius"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-209",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1166.0, 703.5, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-208",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1276.0, 625.0, 129.0, 22.0 ],
					"text" : "prepend /tf/fact_ramp"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-205",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1211.5, 714.0, 240.0, 22.0 ],
					"text" : "26 2200, 14 100, 14 30, 115 0, 14 40, 116 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-204",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1284.5, 685.0, 124.0, 22.0 ],
					"text" : "26 1200, 14 15, 116 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-202",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1088.0, 687.0, 68.0, 22.0 ],
					"text" : "14 0, 117 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-195",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 967.0, 687.0, 74.0, 22.0 ],
					"text" : "14 22, 116 0"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.741176470588235, 0.168627450980392, 0.870588235294118, 1.0 ],
					"id" : "obj-193",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1284.5, 846.0, 47.0, 22.0 ],
					"text" : "s ---CC"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-191",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1184.0, 624.0, 23.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-189",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1248.0, 624.0, 23.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-187",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1105.0, 625.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-182",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 996.0, 625.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.741176470588235, 0.168627450980392, 0.870588235294118, 1.0 ],
					"id" : "obj-170",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1166.0, 547.0, 45.0, 22.0 ],
					"text" : "r ---CC"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-177",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 183.0, 102.0, 1296.0, 960.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-87",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 109.0, 128.0, 50.0, 22.0 ],
									"text" : "116 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-82",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 270.0, 370.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 535.0, 576.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-75",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 886.0, 667.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 987.0, 678.0, 32.0, 22.0 ],
									"text" : "16 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-72",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 974.0, 646.0, 32.0, 22.0 ],
									"text" : "14 6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-68",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 963.0, 617.0, 32.0, 22.0 ],
									"text" : "15 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 948.0, 591.0, 39.0, 22.0 ],
									"text" : "14 15"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 938.25637686252594, 560.0, 32.0, 22.0 ],
									"text" : "26 5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-38",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 925.25637686252594, 530.0, 45.0, 22.0 ],
									"text" : "14 103"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-84",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1055.102527499198914, 694.57752388715744, 184.0, 22.0 ],
									"text" : "14 103, 26 5, 14 100, 14 5, 116 2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-85",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1043.95827329158783, 747.474577635526657, 270.0, 22.0 ],
									"text" : "14 102, 26 22, 14 30, 115 5, 14 100, 14 40, 116 6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-83",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 548.0, 728.0, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-81",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 548.0, 684.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-79",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 400.5, 865.0, 34.0, 22.0 ],
									"text" : "sel 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-78",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 369.0, 771.0, 50.5, 22.0 ],
									"text" : "t f f"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-71",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 866.0, 809.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-69",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 767.0, 806.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-66",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 400.5, 801.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-64",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 400.5, 830.0, 36.0, 22.0 ],
									"text" : ">= 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-59",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 306.0, 704.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-55",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 400.5, 704.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-50",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 508.0, 728.0, 29.5, 22.0 ],
									"text" : "-1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 369.0, 733.0, 50.5, 22.0 ],
									"text" : "* 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 274.5, 733.0, 50.5, 22.0 ],
									"text" : "* 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-45",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 507.0, 380.0, 47.0, 22.0 ],
									"text" : "sel 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-67",
									"linecount" : 3,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 620.0, 347.0, 295.0, 47.0 ],
									"text" : "Between 0 <> 99 adds two centesimal points.\n100 makes it negative\n101 <> 127  adds zeroes (103 == 3 zeroes)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-63",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 463.0, 617.0, 91.0, 22.0 ],
									"text" : "* 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-62",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 535.0, 515.0, 69.0, 22.0 ],
									"text" : "prepend 10"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 535.0, 547.0, 31.0, 22.0 ],
									"text" : "pow"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-60",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 535.0, 482.777798652648926, 82.0, 22.0 ],
									"text" : "expr $i1 - 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 391.0, 380.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-25",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 391.0, 347.0, 47.0, 22.0 ],
									"text" : "split 99"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-57",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 331.0, 1068.26061737537384, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-56",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 409.000000000000057, 1001.794827282428741, 126.282046735286656, 22.0 ],
									"text" : "5.3 6.4 2200"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-54",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 123.076915740966797, 1001.794827282428741, 134.615379571914673, 22.0 ],
									"text" : "1. 5000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-52",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 275.0, 1001.794827282428741, 75.0, 22.0 ],
									"text" : "route 111111"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-51",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 925.25637686252594, 865.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-204",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1009.102527499198914, 847.57752388715744, 141.0, 22.0 ],
									"text" : "14 103, 26 5, 14 5, 116 2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 23.111106395721436, 478.333326488733292, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 89.0, 632.064667344093323, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-42",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 275.0, 954.502476513385773, 55.0, 22.0 ],
									"text" : "$2 $1 $3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-40",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 198.009947121143341, 167.089549362659454, 53.0, 35.0 ],
									"text" : "116 115 14 26"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-205",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 997.95827329158783, 900.474577635526657, 227.0, 22.0 ],
									"text" : "14 102, 26 22, 14 30, 115 5, 14 40, 116 6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 214.0, 482.777798652648926, 29.5, 22.0 ],
									"text" : "t i 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 214.0, 412.777795314788818, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 214.0, 449.444463729858398, 52.0, 22.0 ],
									"text" : "gate 1 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "int", "int" ],
									"patching_rect" : [ 463.0, 422.008292675018311, 176.0, 22.0 ],
									"text" : "t i 1 111111"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.184313725490196, 0.086274509803922, 0.517647058823529, 1.0 ],
									"id" : "obj-35",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 23.111106395721436, 445.888889312744141, 143.0, 22.0 ],
									"text" : "r ---MilisecondsDealtWith"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.184313725490196, 0.086274509803922, 0.517647058823529, 1.0 ],
									"id" : "obj-34",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 21.111106395721436, 417.888889312744141, 145.0, 22.0 ],
									"text" : "s ---MilisecondsDealtWith"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "float" ],
									"patching_rect" : [ 341.5, 617.0, 46.5, 22.0 ],
									"text" : "t b f"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-26",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 369.0, 657.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 341.5, 576.0, 69.0, 22.0 ],
									"text" : "+ 0."
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-29",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 391.0, 525.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 391.0, 490.0, 82.0, 22.0 ],
									"text" : "expr $f1 / 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 535.0, 213.497512429952621, 38.019900560379085, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-27",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 190.0, 918.0, 62.0, 35.0 ],
									"text" : "111111. 1. 5000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 275.0, 918.0, 207.0, 22.0 ],
									"text" : "pack f f i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 463.0, 213.497512429952621, 38.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 391.0, 213.497512429952621, 38.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "int", "int", "int", "int" ],
									"patching_rect" : [ 319.0, 173.51741299033165, 235.0, 22.0 ],
									"text" : "unpack 116 115 14 26"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.184313725490196, 0.086274509803922, 0.517647058823529, 1.0 ],
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 89.0, 604.0, 133.0, 22.0 ],
									"text" : "r ---DecimaltsDealtWith"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.184313725490196, 0.086274509803922, 0.517647058823529, 1.0 ],
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 89.0, 576.0, 135.0, 22.0 ],
									"text" : "s ---DecimaltsDealtWith"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "float" ],
									"patching_rect" : [ 247.0, 617.0, 46.5, 22.0 ],
									"text" : "t b f"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-22",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 274.5, 657.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 247.0, 576.0, 73.000000000000028, 22.0 ],
									"text" : "+ 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 89.0, 666.36318364739418, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-12",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 301.0, 525.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 301.0, 490.0, 82.0, 22.0 ],
									"text" : "expr $f1 / 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 174.0, 307.0, 50.0, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 247.0, 258.208951145410538, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 275.0, 1068.26061737537384, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 247.0, 307.0, 307.0, 22.0 ],
									"text" : "route 116 115 14 26"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 319.0, 213.497512429952621, 39.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 318.890548497438601, 138.009950280189514, 57.0, 22.0 ],
									"text" : "route CC"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 467.000000000000057, 137.800994396209717, 66.0, 22.0 ],
									"text" : "route done"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-182",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 414.000000000000057, 58.800994396209717, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.501960784313725, 0.227450980392157, 0.227450980392157, 1.0 ],
									"id" : "obj-180",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 414.000000000000057, 27.800994396209717, 107.0, 22.0 ],
									"text" : "r ---start-sequence"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 414.000000000000057, 103.800994396209717, 72.0, 22.0 ],
									"text" : "patcherargs"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 247.0, 27.800994396209717, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-10", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"order" : 0,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 1 ],
									"order" : 1,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 1 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 1 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"source" : [ "obj-14", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"order" : 0,
									"source" : [ "obj-14", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-14", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 1 ],
									"order" : 0,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"order" : 1,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-82", 0 ],
									"order" : 1,
									"source" : [ "obj-14", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 4 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-18", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-182", 0 ],
									"source" : [ "obj-180", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-182", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"order" : 0,
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"order" : 1,
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-2", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-204", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-205", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 3 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"source" : [ "obj-23", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-45", 0 ],
									"source" : [ "obj-25", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 1 ],
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-3", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"source" : [ "obj-3", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-3", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 1 ],
									"midpoints" : [ 629.5, 454.0, 629.0, 454.0, 629.0, 904.0, 462.0, 904.0, 462.0, 904.0, 378.5, 904.0 ],
									"source" : [ "obj-31", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"source" : [ "obj-31", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-63", 0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-37", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"source" : [ "obj-38", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"order" : 0,
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"order" : 1,
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-27", 1 ],
									"order" : 1,
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"order" : 0,
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"order" : 1,
									"source" : [ "obj-44", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 0 ],
									"order" : 0,
									"source" : [ "obj-44", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"source" : [ "obj-45", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 0 ],
									"source" : [ "obj-45", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"order" : 1,
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-62", 0 ],
									"order" : 0,
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"order" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 1 ],
									"order" : 1,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-55", 0 ],
									"order" : 0,
									"source" : [ "obj-50", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"order" : 1,
									"source" : [ "obj-50", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-51", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"order" : 0,
									"source" : [ "obj-52", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 1 ],
									"order" : 1,
									"source" : [ "obj-52", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-56", 1 ],
									"order" : 0,
									"source" : [ "obj-52", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-57", 0 ],
									"order" : 1,
									"source" : [ "obj-52", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"source" : [ "obj-53", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"source" : [ "obj-55", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 1 ],
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 2 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-62", 0 ],
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-77", 0 ],
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"source" : [ "obj-62", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 2 ],
									"source" : [ "obj-63", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-69", 0 ],
									"order" : 0,
									"source" : [ "obj-64", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-79", 0 ],
									"order" : 1,
									"source" : [ "obj-64", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 0 ],
									"source" : [ "obj-66", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"source" : [ "obj-68", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"source" : [ "obj-74", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-63", 1 ],
									"source" : [ "obj-77", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 1 ],
									"source" : [ "obj-78", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"source" : [ "obj-78", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 0 ],
									"source" : [ "obj-79", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-83", 0 ],
									"source" : [ "obj-81", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-55", 0 ],
									"order" : 0,
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"order" : 1,
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-84", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-85", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "ksliderWhite",
								"default" : 								{
									"color" : [ 1, 1, 1, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjBlue-1",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-1",
								"default" : 								{
									"accentcolor" : [ 0, 0.533333, 0.168627, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 1166.0, 576.0, 129.0, 35.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p route-fact_ramps @CC 116 115 14 26"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-180",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1166.0, 656.0, 129.0, 22.0 ],
					"text" : "prepend /tf/fact_goal"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.501960784313725, 0.227450980392157, 0.227450980392157, 1.0 ],
					"id" : "obj-176",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1381.5, 236.5, 107.0, 22.0 ],
					"text" : "r ---start-sequence"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-175",
					"maxclass" : "live.text",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1086.0, 218.0, 79.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 4.0, 5.0, 72.0, 23.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "M4L device",
					"texton" : "Standalone",
					"varname" : "live.text"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.501960784313725, 0.227450980392157, 0.227450980392157, 1.0 ],
					"id" : "obj-77",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 550.0, -16.5, 107.0, 22.0 ],
					"text" : "r ---start-sequence"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-174",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1099.0, 857.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 98.75, 93.0, 39.5, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 11.0,
					"format" : 6,
					"id" : "obj-155",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1046.0, 625.0, 49.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 3.0, 93.0, 33.0, 21.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 11.0,
					"format" : 6,
					"id" : "obj-145",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 939.0, 625.0, 51.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 37.0, 93.0, 30.75, 21.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-171",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 982.0, 361.0, 38.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 3.0, 115.5, 43.0, 17.0 ],
					"text" : "4 10"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-213",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 939.0, 656.0, 99.0, 22.0 ],
					"text" : "prepend /tf/factor"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.741176470588235, 0.168627450980392, 0.870588235294118, 1.0 ],
					"id" : "obj-210",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1046.0, 547.0, 45.0, 22.0 ],
					"text" : "r ---CC"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.741176470588235, 0.168627450980392, 0.870588235294118, 1.0 ],
					"id" : "obj-201",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 939.0, 547.0, 45.0, 22.0 ],
					"text" : "r ---CC"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.741176470588235, 0.168627450980392, 0.870588235294118, 1.0 ],
					"id" : "obj-200",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1099.0, 782.0, 45.0, 22.0 ],
					"text" : "r ---CC"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.741176470588235, 0.168627450980392, 0.870588235294118, 1.0 ],
					"id" : "obj-199",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 891.0, 328.0, 47.0, 22.0 ],
					"text" : "s ---CC"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.741176470588235, 0.168627450980392, 0.870588235294118, 1.0 ],
					"id" : "obj-198",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 609.5, 658.0, 45.0, 22.0 ],
					"text" : "r ---CC"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-197",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 1474.0, 56.0, 1612.0, 960.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 72.0, 534.0, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 326.0, 504.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 326.0, 448.0, 29.5, 22.0 ],
									"text" : "-1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 214.5, 576.0, 130.5, 22.0 ],
									"text" : "* 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 326.0, 385.0, 47.0, 22.0 ],
									"text" : "sel 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 215.0, 309.0, 130.0, 22.0 ],
									"text" : "split 99"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 96.0, 12.0, 118.0, 22.0 ],
									"text" : "14 100, 14 25, 117 2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 523.0, 301.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 456.0, 266.0, 86.0, 22.0 ],
									"text" : "unpack 117 14"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.184313725490196, 0.086274509803922, 0.517647058823529, 1.0 ],
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 33.0, 499.0, 133.0, 22.0 ],
									"text" : "r ---DecimaltsDealtWith"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.184313725490196, 0.086274509803922, 0.517647058823529, 1.0 ],
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 33.0, 471.0, 135.0, 22.0 ],
									"text" : "s ---DecimaltsDealtWith"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-25",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 172.0, 67.0, 38.0, 22.0 ],
									"text" : "117 5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "float" ],
									"patching_rect" : [ 187.0, 512.0, 46.5, 22.0 ],
									"text" : "t b f"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-22",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 214.5, 627.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 187.0, 471.0, 46.5, 22.0 ],
									"text" : "+ 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 33.0, 534.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-12",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 215.0, 418.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 215.0, 385.0, 82.0, 22.0 ],
									"text" : "expr $f1 / 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 130.0, 393.0, 50.0, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 187.0, 197.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 136.0, 40.0, 74.0, 22.0 ],
									"text" : "14 22, 117 3"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 214.5, 669.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 187.0, 262.0, 75.0, 22.0 ],
									"text" : "route 117 14"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 456.0, 301.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 456.0, 214.0, 57.0, 22.0 ],
									"text" : "route CC"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 409.0, 172.0, 66.0, 22.0 ],
									"text" : "route done"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-182",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 356.0, 93.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.501960784313725, 0.227450980392157, 0.227450980392157, 1.0 ],
									"id" : "obj-180",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 356.0, 62.0, 107.0, 22.0 ],
									"text" : "r ---start-sequence"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 356.0, 138.0, 72.0, 22.0 ],
									"text" : "patcherargs"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 187.0, 108.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-10", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 1 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 1 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"order" : 0,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-14", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"order" : 1,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"order" : 1,
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"order" : 0,
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-17", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 2 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-182", 0 ],
									"source" : [ "obj-180", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-182", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-2", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"source" : [ "obj-23", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 1 ],
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "ksliderWhite",
								"default" : 								{
									"color" : [ 1, 1, 1, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjBlue-1",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-1",
								"default" : 								{
									"accentcolor" : [ 0, 0.533333, 0.168627, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 1046.0, 576.0, 107.0, 35.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p route-maxfactor @CC 117 14"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-192",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 2446.0, 237.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"color" : [ 0.184313725490196, 0.086274509803922, 0.517647058823529, 1.0 ],
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 288.0, 415.0, 133.0, 22.0 ],
									"text" : "r ---DecimaltsDealtWith"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.184313725490196, 0.086274509803922, 0.517647058823529, 1.0 ],
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 288.0, 387.0, 135.0, 22.0 ],
									"text" : "s ---DecimaltsDealtWith"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-196",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 5.0, 128.0, 69.0, 22.0 ],
									"text" : "prepend 87"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-193",
									"maxclass" : "dial",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 5.0, 78.0, 40.0, 40.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 97.0, 349.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 97.0, 277.0, 53.0, 22.0 ],
									"text" : "route 87"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 366.0, 239.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 366.0, 199.0, 57.0, 22.0 ],
									"text" : "route CC"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 319.0, 157.0, 66.0, 22.0 ],
									"text" : "route done"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-182",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 266.0, 78.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.501960784313725, 0.227450980392157, 0.227450980392157, 1.0 ],
									"id" : "obj-180",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 266.0, 47.0, 107.0, 22.0 ],
									"text" : "r ---start-sequence"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 266.0, 123.0, 72.0, 22.0 ],
									"text" : "patcherargs"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 97.0, 93.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-10", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 1 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-182", 0 ],
									"source" : [ "obj-180", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-182", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-196", 0 ],
									"source" : [ "obj-193", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-196", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-2", 1 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "ksliderWhite",
								"default" : 								{
									"color" : [ 1, 1, 1, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjBlue-1",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-1",
								"default" : 								{
									"accentcolor" : [ 0, 0.533333, 0.168627, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 1099.0, 815.0, 66.0, 35.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p route-K @CC 87"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-185",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 1474.0, 56.0, 1612.0, 960.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-50",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 316.0, 546.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 305.0, 654.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-42",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 73.75, 606.0, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-40",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 187.0, 589.0, 148.0, 22.0 ],
									"text" : "* 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 316.0, 456.0, 29.5, 22.0 ],
									"text" : "-1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 316.0, 423.0, 47.0, 22.0 ],
									"text" : "sel 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 285.0, 352.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-27",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 215.0, 423.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 215.0, 389.0, 120.0, 22.0 ],
									"text" : "split 99"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 17.0, 260.0, 54.0, 22.0 ],
									"text" : "deferlow"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 17.0, 222.0, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.184313725490196, 0.086274509803922, 0.517647058823529, 1.0 ],
									"id" : "obj-35",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 17.0, 183.0, 143.0, 22.0 ],
									"text" : "r ---MilisecondsDealtWith"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 186.0, 222.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 187.0, 260.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 187.0, 296.0, 52.0, 22.0 ],
									"text" : "gate 1 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 186.0, 183.0, 53.0, 22.0 ],
									"text" : "route 26"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 494.0, 254.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 427.0, 209.0, 86.0, 22.0 ],
									"text" : "unpack 116 14"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.184313725490196, 0.086274509803922, 0.517647058823529, 1.0 ],
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 30.0, 574.0, 133.0, 22.0 ],
									"text" : "r ---DecimaltsDealtWith"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.184313725490196, 0.086274509803922, 0.517647058823529, 1.0 ],
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 30.0, 546.0, 135.0, 22.0 ],
									"text" : "s ---DecimaltsDealtWith"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-25",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 241.0, 43.0, 38.0, 22.0 ],
									"text" : "116 5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "float" ],
									"patching_rect" : [ 187.0, 647.0, 46.5, 22.0 ],
									"text" : "t b f"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-22",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 214.5, 687.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 187.0, 546.0, 46.5, 22.0 ],
									"text" : "+ 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 30.0, 606.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-12",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 215.0, 493.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 215.0, 460.0, 82.0, 22.0 ],
									"text" : "expr $f1 / 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 186.0, 152.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 87.0, 43.0, 118.0, 22.0 ],
									"text" : "14 100, 14 22, 116 3"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 214.5, 729.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 187.0, 352.0, 75.0, 22.0 ],
									"text" : "route 116 14"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 427.0, 254.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 427.0, 165.0, 57.0, 22.0 ],
									"text" : "route CC"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 380.0, 123.0, 66.0, 22.0 ],
									"text" : "route done"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-182",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 327.0, 44.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.501960784313725, 0.227450980392157, 0.227450980392157, 1.0 ],
									"id" : "obj-180",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 327.0, 13.0, 107.0, 22.0 ],
									"text" : "r ---start-sequence"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 327.0, 89.0, 72.0, 22.0 ],
									"text" : "patcherargs"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 186.0, 83.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-10", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 1 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 1 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-14", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"order" : 1,
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"order" : 0,
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-27", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"source" : [ "obj-17", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 1 ],
									"source" : [ "obj-18", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-182", 0 ],
									"source" : [ "obj-180", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-182", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-2", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"order" : 1,
									"source" : [ "obj-23", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"order" : 0,
									"source" : [ "obj-23", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-3", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 1 ],
									"source" : [ "obj-50", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 2 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "ksliderWhite",
								"default" : 								{
									"color" : [ 1, 1, 1, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjBlue-1",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-1",
								"default" : 								{
									"accentcolor" : [ 0, 0.533333, 0.168627, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 939.0, 576.0, 99.0, 35.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p route-factor @CC 116 14"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.501960784313725, 0.227450980392157, 0.227450980392157, 1.0 ],
					"id" : "obj-184",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -21.375, 931.0, 107.0, 22.0 ],
					"text" : "r ---start-sequence"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.501960784313725, 0.227450980392157, 0.227450980392157, 1.0 ],
					"id" : "obj-183",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 334.5, -167.5, 107.0, 22.0 ],
					"text" : "r ---start-sequence"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.501960784313725, 0.227450980392157, 0.227450980392157, 1.0 ],
					"id" : "obj-179",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 550.0, -105.0, 109.0, 22.0 ],
					"text" : "s ---start-sequence"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-178",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 2446.0, 237.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"color" : [ 0.184313725490196, 0.086274509803922, 0.517647058823529, 1.0 ],
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 246.0, 364.0, 133.0, 22.0 ],
									"text" : "r ---DecimaltsDealtWith"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.184313725490196, 0.086274509803922, 0.517647058823529, 1.0 ],
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 246.0, 336.0, 135.0, 22.0 ],
									"text" : "s ---DecimaltsDealtWith"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 97.0, 349.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 97.0, 277.0, 53.0, 22.0 ],
									"text" : "route 86"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 366.0, 239.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 366.0, 199.0, 57.0, 22.0 ],
									"text" : "route CC"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 319.0, 157.0, 66.0, 22.0 ],
									"text" : "route done"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-182",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 266.0, 78.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.501960784313725, 0.227450980392157, 0.227450980392157, 1.0 ],
									"id" : "obj-180",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 266.0, 47.0, 107.0, 22.0 ],
									"text" : "r ---start-sequence"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 266.0, 123.0, 72.0, 22.0 ],
									"text" : "patcherargs"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 97.0, 93.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-10", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 1 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-182", 0 ],
									"source" : [ "obj-180", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-182", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-2", 1 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "ksliderWhite",
								"default" : 								{
									"color" : [ 1, 1, 1, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjBlue-1",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-1",
								"default" : 								{
									"accentcolor" : [ 0, 0.533333, 0.168627, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 609.5, 697.0, 110.0, 35.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p route-centernote @CC 86"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-173",
					"maxclass" : "live.menu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 24.0, -4.0, 100.0, 19.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 3.0, 73.0, 136.0, 19.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "tempered", "generative", "polytempo" ],
							"parameter_longname" : "live.menu[1]",
							"parameter_mmax" : 2,
							"parameter_shortname" : "live.menu[1]",
							"parameter_type" : 2
						}

					}
,
					"varname" : "live.menu"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-172",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 12.974495351314545, 974.0, 38.301009297370911, 24.0 ],
					"presentation" : 1,
					"presentation_linecount" : 2,
					"presentation_rect" : [ 781.0, 141.0, 35.375, 24.0 ],
					"text" : "notes on/off",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"activebgoncolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ],
					"id" : "obj-167",
					"maxclass" : "live.toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ -21.375, 1014.0, 25.375, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 780.0, 141.0, 36.375, 24.0 ],
					"saved_attribute_attributes" : 					{
						"activebgoncolor" : 						{
							"expression" : ""
						}
,
						"valueof" : 						{
							"parameter_enum" : [ "off", "on" ],
							"parameter_linknames" : 1,
							"parameter_longname" : "notesOn/OFF",
							"parameter_mmax" : 1,
							"parameter_shortname" : "noteON/OFF",
							"parameter_type" : 2
						}

					}
,
					"varname" : "notesOn/OFF"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-169",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -5.0, 710.0, 50.0, 22.0 ],
					"text" : "9 30"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-164",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -133.198990702629089, 1067.0, 100.0, 22.0 ],
					"text" : "print Hz-encoded"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-168",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -79.0, 974.0, 50.0, 49.0 ],
					"text" : "coll 57 57 196.88"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-166",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ -127.698990702629089, 913.704328298568726, 89.0, 22.0 ],
					"text" : "midiHz-decode"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.6, 0.6, 0.6, 1.0 ],
					"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "color",
					"fontsize" : 8.0,
					"gradient" : 1,
					"id" : "obj-165",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -118.198990702629089, 613.0, 120.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 888.0, 70.5, 99.0, 17.0 ],
					"text" : "coll 57 57 221."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-163",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -68.817207336425781, 797.849497556686401, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-162",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -120.198990702629089, 709.204328298568726, 74.0, 22.0 ],
					"text" : "prepend coll"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-161",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ -120.198990702629089, 676.204328298568726, 57.0, 22.0 ],
					"text" : "route coll"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.933333333333333, 0.0, 1.0 ],
					"id" : "obj-160",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -120.198990702629089, 647.204328298568726, 175.0, 22.0 ],
					"text" : "r ---#0_TransForms_M4L_send"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-159",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ -120.198990702629089, 750.704328298568726, 89.0, 22.0 ],
					"text" : "midiHz-encode"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.933333333333333, 0.0, 1.0 ],
					"id" : "obj-134",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 748.0, 630.0, 177.0, 22.0 ],
					"text" : "s ---#0_TransForms_M4L_send"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-154",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -21.375, 974.0, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-158",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -21.375, 1067.0, 52.0, 22.0 ],
					"text" : "gate 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-149",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 564.0, 1075.0, 50.0, 22.0 ],
					"text" : "57 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-157",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 79.0, 1372.0, 787.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 159.0, 272.0, 37.0, 22.0 ],
									"text" : "zl.rev"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 342.0, 292.0, 50.0, 22.0 ],
									"text" : "54 57"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 73.262029945850372, 214.43849641084671, 50.0, 22.0 ],
									"text" : "57"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 90.0, 154.545450031757355, 46.0, 22.0 ],
									"text" : "route 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 90.0, 99.465237736701965, 37.0, 22.0 ],
									"text" : "zl.rev"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 336.363626539707184, 144.919781863689423, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 252.941169083118439, 147.593578577041626, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 262.727267682552338, 92.465240657329559, 47.0, 22.0 ],
									"text" : "unpack"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 159.0, 350.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 90.0, 37.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 1 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"order" : 1,
									"source" : [ "obj-11", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"order" : 0,
									"source" : [ "obj-11", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"order" : 2,
									"source" : [ "obj-11", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-3", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 145.0, 941.0, 88.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p onlyNotesOn"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-156",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 93.0, 948.0, 50.0, 22.0 ],
					"text" : "57 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-148",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1046.0, 656.0, 107.0, 22.0 ],
					"text" : "prepend dial_MAX"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-152",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1099.0, 886.0, 115.0, 22.0 ],
					"text" : "prepend dial_THRU"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-153",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 748.0, 516.0, 34.0, 22.0 ],
					"text" : "flush"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-151",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 748.0, 484.0, 34.0, 22.0 ],
					"text" : "sel 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-150",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 835.0, 333.0, 44.0, 20.0 ],
					"text" : "flush"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-140",
					"maxclass" : "live.toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 817.0, 328.0, 70.5, 30.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 502.0, 106.5, 18.0, 9.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "off", "on" ],
							"parameter_linknames" : 1,
							"parameter_longname" : "Flush",
							"parameter_mmax" : 1,
							"parameter_shortname" : "flush",
							"parameter_type" : 2
						}

					}
,
					"varname" : "Flush"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-133",
					"maxclass" : "live.toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 705.0, 328.0, 32.0, 30.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 522.0, 103.5, 13.0, 13.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "off", "on" ],
							"parameter_linknames" : 1,
							"parameter_longname" : "nano_pedal",
							"parameter_mmax" : 1,
							"parameter_shortname" : "nanoPedal",
							"parameter_type" : 2
						}

					}
,
					"varname" : "nano_pedal"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-147",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 731.5, 798.0, 82.0, 22.0 ],
					"text" : "centernote $1"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-141",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 731.5, 769.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-139",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 731.5, 697.0, 109.0, 22.0 ],
					"text" : "route /tf/centernote"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.933333333333333, 0.0, 1.0 ],
					"id" : "obj-126",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 731.5, 663.0, 187.0, 22.0 ],
					"text" : "r ---#0_TransForms_M4L_receive"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 11.0,
					"id" : "obj-146",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 166.0, 187.0, 60.0, 19.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 121.0, 133.0, 23.0, 19.0 ],
					"text" : "st"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-144",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 145.0, 670.0, 50.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 64.0, 135.0, 32.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"textcolor" : 						{
							"expression" : ""
						}
,
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "CenterNote",
							"parameter_mmax" : 1000.0,
							"parameter_shortname" : "center",
							"parameter_type" : 0,
							"parameter_unitstyle" : 8
						}

					}
,
					"textcolor" : [ 0.827450980392157, 0.411764705882353, 0.086274509803922, 1.0 ],
					"varname" : "CenterNote"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-143",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 1706.0, 175.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 171.0, 316.0, 50.0, 22.0 ],
									"text" : "72."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 108.0, 279.0, 47.0, 22.0 ],
									"text" : "zl.nth 2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 171.0, 253.0, 50.0, 22.0 ],
									"text" : "64 72."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 81.0, 218.0, 46.0, 22.0 ],
									"text" : "route 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 171.0, 205.0, 50.0, 22.0 ],
									"text" : "64 72."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 81.0, 138.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 81.0, 176.0, 37.0, 22.0 ],
									"text" : "zl.rev"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 114.0, 89.0, 50.0, 22.0 ],
									"text" : "72. 64"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-21",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 81.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-22",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 108.0, 343.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"order" : 1,
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 1 ],
									"order" : 0,
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 1 ],
									"order" : 0,
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"order" : 1,
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 1 ],
									"order" : 0,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"order" : 1,
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 145.0, 640.5, 105.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p filterout-noteoffs"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-142",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 291.5, 511.0, 50.0, 22.0 ],
					"text" : "72. 64"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-136",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 185.0, 1014.0, 50.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 64.0, 152.0, 32.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "NoteName",
							"parameter_mmax" : 1000.0,
							"parameter_shortname" : "NoteName",
							"parameter_type" : 0,
							"parameter_unitstyle" : 8
						}

					}
,
					"varname" : "NoteName"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 246.0, 1014.0, 50.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 2.75, 152.0, 65.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "FreqHz",
							"parameter_mmax" : 10000.0,
							"parameter_shortname" : "FreqHz",
							"parameter_type" : 0,
							"parameter_unitstyle" : 3
						}

					}
,
					"varname" : "FreqHz"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-129",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 89.375, 1091.0, 90.0, 22.0 ],
					"text" : "midiin 57 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 44.375, 1014.0, 87.0, 22.0 ],
					"text" : "prepend midiin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 44.375, 798.0, 109.0, 22.0 ],
					"text" : "t l l l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 185.0, 1073.0, 77.0, 22.0 ],
					"text" : "coll $1 $1 $2"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.470588235294118, 0.470588235294118, 0.470588235294118, 1.0 ],
					"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "color",
					"gradient" : 1,
					"id" : "obj-109",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 123.875, 1124.0, 35.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 817.875, 142.0, 35.0, 22.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-80",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1088.0, 295.0, 50.0, 35.0 ],
					"text" : "midiin 57 0"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.933333333333333, 0.0, 1.0 ],
					"id" : "obj-138",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 185.0, 1124.0, 177.0, 22.0 ],
					"text" : "s ---#0_TransForms_M4L_send"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 10.0,
					"id" : "obj-137",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 291.5, 1073.0, 78.0, 20.0 ],
					"text" : "coll 57 57 221."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-135",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 185.0, 1040.0, 47.0, 22.0 ],
					"text" : "pack i f"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-132",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 412.0, 1029.0, 83.0, 22.0 ],
					"text" : "loadmess 442"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-131",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "int", "int" ],
					"patching_rect" : [ 412.0, 996.5, 83.0, 22.0 ],
					"text" : "live.thisdevice"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-130",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 412.0, 1081.0, 51.0, 22.0 ],
					"text" : "base $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-127",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 412.0, 1061.0, 44.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 2.75, 135.0, 65.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "BaseFreq",
							"parameter_mmax" : 10000.0,
							"parameter_shortname" : "BaseFreq",
							"parameter_type" : 0,
							"parameter_unitstyle" : 3
						}

					}
,
					"varname" : "BaseFreq"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-119",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 246.0, 980.0, 97.0, 22.0 ],
					"text" : "mtof @base 442"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-113",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 246.0, 941.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1381.5, 274.0, 241.0, 35.0 ],
					"text" : "port-send 8585, host 127.0.0.1, sendtoggle 0, port-receive 22574"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-122",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 24.0, -160.0, 293.0, 49.0 ],
					"text" : "1 TF_mode 2, 2 klider_mode 2, 3 pb_mode 2, 4 pb_range 2, 5 bach_tonedivision 4, 6 bach_accidentals 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 442.0, 340.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 591.0, 478.0, 85.0, 22.0 ],
					"text" : "accidentals $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-116",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 591.0, 436.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 591.0, 382.0, 89.0, 22.0 ],
					"text" : "tonedivision $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 591.0, 340.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 550.0, 49.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-103",
					"maxclass" : "newobj",
					"numinlets" : 7,
					"numoutlets" : 7,
					"outlettype" : [ "", "", "", "", "", "", "" ],
					"patching_rect" : [ 24.0, -53.0, 1040.0, 22.0 ],
					"text" : "route TF_mode klider_mode pb_mode pb_range bach_tonedivision bach_accidentals"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-99",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 334.5, -133.0, 39.0, 22.0 ],
					"text" : "dump"
				}

			}
, 			{
				"box" : 				{
					"coll_data" : 					{
						"count" : 6,
						"data" : [ 							{
								"key" : 1,
								"value" : [ "TF_mode", 2 ]
							}
, 							{
								"key" : 2,
								"value" : [ "klider_mode", 2 ]
							}
, 							{
								"key" : 3,
								"value" : [ "pb_mode", 2 ]
							}
, 							{
								"key" : 4,
								"value" : [ "pb_range", 2 ]
							}
, 							{
								"key" : 5,
								"value" : [ "bach_tonedivision", 4 ]
							}
, 							{
								"key" : 6,
								"value" : [ "bach_accidentals", 1 ]
							}
 ]
					}
,
					"fontsize" : 18.0,
					"id" : "obj-94",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 24.0, -96.0, 243.0, 29.0 ],
					"saved_object_attributes" : 					{
						"embed" : 1,
						"precision" : 6
					}
,
					"text" : "coll ---#0_TransForms_status"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 550.0, -140.0, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-125",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 362.5, 790.0, 57.0, 20.0 ],
					"text" : "cents"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-121",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "pb_encode.maxpat",
					"numinlets" : 3,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 379.625, 899.0, 158.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 3.0, 53.5, 138.5, 21.0 ],
					"varname" : "BendType",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-120",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 443.625, 841.0, 61.0, 20.0 ],
					"text" : "Range"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-118",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 637.625, 951.0, 44.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 2,
							"parameter_linknames" : 1,
							"parameter_longname" : "PBdecode",
							"parameter_mmax" : 100.0,
							"parameter_mmin" : -100.0,
							"parameter_shortname" : "pb",
							"parameter_type" : 0,
							"parameter_unitstyle" : 1
						}

					}
,
					"varname" : "PBdecode"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 449.125, 873.0, 44.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 93.0, 135.0, 30.5, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "ST-range",
							"parameter_mmax" : 100.0,
							"parameter_mmin" : -100.0,
							"parameter_shortname" : "STrange",
							"parameter_type" : 0,
							"parameter_unitstyle" : 0
						}

					}
,
					"varname" : "ST-range"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-110",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 659.125, 980.0, 50.0, 22.0 ],
					"text" : "hires 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 518.625, 841.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-102",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 637.625, 918.0, 71.5, 22.0 ],
					"text" : "pb_decode"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-101",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 449.125, 948.0, 58.0, 22.0 ],
					"text" : "xbendout"
				}

			}
, 			{
				"box" : 				{
					"attr" : "hires",
					"id" : "obj-83",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -39.833344000000011, 855.0, 113.833344000000011, 44.0 ],
					"text_width" : 65.999992000000006
				}

			}
, 			{
				"box" : 				{
					"align" : 1,
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 0.0 ],
					"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 0.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "gradient",
					"color" : [ 0.992230892181396, 0.612652897834778, 0.138494849205017, 1.0 ],
					"fontsize" : 13.0,
					"id" : "obj-86",
					"items" : [ "calculate...", ",", "pedal held" ],
					"maxclass" : "umenu",
					"menumode" : 3,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 291.5, 175.0, 128.0, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 309.0, 99.5, 84.0, 23.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"linecount" : 6,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1444.5, 430.0, 140.0, 89.0 ],
					"text" : ";\rmax launchbrowser https://bitbucket.org/AdrianArtacho/transforms/src/master/other/README.md"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.470588235294118, 0.470588235294118, 0.470588235294118, 1.0 ],
					"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "color",
					"fontsize" : 11.0,
					"gradient" : 1,
					"id" : "obj-74",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1444.5, 388.0, 56.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 76.5, 6.0, 63.0, 21.0 ],
					"text" : "README",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-67",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 311.875, 789.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-12",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 365.5, 341.0, 76.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 365.5, 306.0, 113.0, 22.0 ],
									"text" : "expr pow($f1\\,$f2)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 288.25, 363.0, 40.0, 20.0 ],
									"text" : "pow",
									"textcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 185.5, 363.0, 40.0, 20.0 ],
									"text" : "linear"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-6",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 185.5, 341.0, 72.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-4",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 277.5, 341.0, 77.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 277.5, 306.0, 61.5, 22.0 ],
									"text" : "pow"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-27",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 279.0, 81.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "float" ],
									"patching_rect" : [ 279.0, 111.0, 60.0, 22.0 ],
									"text" : "t b f"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.309804, 0.298039, 0.298039, 0.2 ],
									"contdata" : 1,
									"id" : "obj-25",
									"ignoreclick" : 1,
									"maxclass" : "multislider",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 109.0, 165.0, 64.0, 140.0 ],
									"setminmax" : [ 0.0, 1.0 ],
									"slidercolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
									"thickness" : 1
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.309495, 0.299387, 0.299789, 0.49 ],
									"contdata" : 1,
									"id" : "obj-2",
									"maxclass" : "multislider",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 279.0, 142.0, 20.0, 140.0 ],
									"setminmax" : [ 0.0, 1.0 ],
									"slidercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
									"thickness" : 4
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"order" : 0,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"order" : 1,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"order" : 2,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 1 ],
									"order" : 0,
									"source" : [ "obj-26", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 1 ],
									"order" : 1,
									"source" : [ "obj-26", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"midpoints" : [ 287.0, 336.0, 249.0, 336.0, 249.0, 130.0, 118.5, 130.0 ],
									"order" : 1,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"order" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1553.5, 745.0, 54.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p testing"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 7,
					"numoutlets" : 2,
					"outlettype" : [ "int", "" ],
					"patching_rect" : [ 89.375, 859.0, 116.0, 22.0 ],
					"text" : "midiformat @hires 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-90",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "float" ],
					"patching_rect" : [ 311.625, 841.0, 87.0, 22.0 ],
					"text" : "t f f"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 311.625, 873.0, 36.0, 22.0 ],
					"text" : "+ 50."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 379.625, 873.0, 44.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 93.0, 152.0, 48.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "CentDeviation",
							"parameter_mmax" : 100.0,
							"parameter_mmin" : -100.0,
							"parameter_shortname" : "Cents",
							"parameter_type" : 0,
							"parameter_unitstyle" : 1
						}

					}
,
					"varname" : "CentDeviation"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 89.375, 637.0, 46.0, 22.0 ],
					"text" : "pack i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 922.5, 931.0, 50.0, 22.0 ],
					"text" : "0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 856.5, 931.0, 50.0, 22.0 ],
					"text" : "0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 790.5, 931.0, 50.0, 22.0 ],
					"text" : "885."
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-4",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "PTN-control.maxpat",
					"numinlets" : 1,
					"numoutlets" : 4,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "float", "", "" ],
					"patching_rect" : [ 821.5, 744.0, 217.0, 168.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 566.0, 3.0, 215.0, 165.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "float" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 1909.0, 111.0, 1177.0, 764.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 445.5, 433.0, 110.5, 22.0 ],
									"text" : "t f f"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 657.0, 373.5, 63.0, 20.0 ],
									"text" : "+/- cents"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 469.0, 367.0, 62.0, 33.0 ],
									"text" : "rounded noteout"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 707.0, 520.5, 50.0, 22.0 ],
									"text" : "0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-100",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 642.5, 441.0, 50.0, 22.0 ],
									"text" : "t f f"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-99",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 537.5, 674.5, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-98",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 566.5, 520.5, 50.0, 22.0 ],
									"text" : "57."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-96",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 537.5, 481.0, 124.0, 22.0 ],
									"text" : "expr $f1 + ($f2 / 100.)"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-94",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 642.5, 410.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-92",
									"index" : 3,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 673.5, 680.5, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-89",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 476.5, 600.0, 35.0, 22.0 ],
									"text" : "clear"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-87",
									"linecount" : 3,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 284.0, 508.0, 150.0, 47.0 ],
									"text" : "correspondence between keydowns and outputted midinote"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-85",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 83.5, 109.0, 104.0, 20.0 ],
									"text" : "notedown >>"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-83",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 144.5, 417.0, 50.0, 22.0 ],
									"text" : "pack i 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-82",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 213.0, 417.0, 50.0, 22.0 ],
									"text" : "57 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-80",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "bang" ],
									"patching_rect" : [ 98.5, 350.0, 65.5, 22.0 ],
									"text" : "t i b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-79",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 83.5, 308.0, 34.0, 22.0 ],
									"text" : "sel 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-78",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 144.5, 386.0, 37.5, 22.0 ],
									"text" : "i 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 163.0, 308.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-75",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 83.5, 233.0, 98.5, 22.0 ],
									"text" : "t i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 67.5, 547.0, 50.0, 22.0 ],
									"text" : "57 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-72",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 98.5, 481.0, 50.0, 22.0 ],
									"text" : "pack i 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-71",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 39.0, 350.0, 50.0, 22.0 ],
									"text" : "57"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-68",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 83.5, 270.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"precision" : 6
									}
,
									"text" : "coll ---#0_keydowns"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-66",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 301.0, 447.0, 37.0, 22.0 ],
									"text" : "zl.rev"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-65",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 469.0, 520.5, 50.0, 22.0 ],
									"text" : "57 54"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 445.5, 481.0, 46.0, 22.0 ],
									"text" : "pack i i"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-60",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 445.5, 404.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-56",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 301.0, 238.5, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-54",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 301.0, 191.0, 95.0, 22.0 ],
									"text" : "unpack f f"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 346.0, 147.0, 50.0, 22.0 ],
									"text" : "57 54"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-51",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 301.0, 147.0, 37.0, 22.0 ],
									"text" : "zl.rev"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-50",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 157.0, 183.0, 50.0, 22.0 ],
									"text" : "57"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 2,
											"revision" : 2,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 1883.0, 193.0, 640.0, 747.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-49",
													"linecount" : 2,
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 110.0, 523.0, 108.0, 33.0 ],
													"text" : "noteout (rounded to closest key)"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-47",
													"linecount" : 2,
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 296.0, 523.0, 134.0, 33.0 ],
													"text" : "difference with the noteout (in cents)"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-44",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 242.5, 523.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-39",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 242.5, 465.0, 135.0, 22.0 ],
													"text" : "expr ($f2 - $f1) * 100."
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-37",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 242.5, 432.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-33",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 50.0, 399.0, 211.5, 22.0 ],
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-27",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 358.5, 370.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-25",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 371.0, 267.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-20",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 428.0, 25.0, 29.5, 22.0 ],
													"text" : "10",
													"textcolor" : [ 0.968627450980392, 0.968627450980392, 0.968627450980392, 0.0 ]
												}

											}
, 											{
												"box" : 												{
													"floatoutput" : 1,
													"id" : "obj-16",
													"maxclass" : "dial",
													"min" : -10.0,
													"mode" : 1,
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 428.0, 59.0, 112.0, 112.0 ],
													"size" : 20.0
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-15",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 173.0, 41.0, 29.5, 22.0 ],
													"text" : "60"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-12",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 240.0, 72.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-10",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 110.0, 72.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-8",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 50.0, 72.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 3,
													"outlettype" : [ "float", "float", "float" ],
													"patching_rect" : [ 110.0, 126.0, 89.0, 22.0 ],
													"text" : "t f f f"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-6",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 243.0, 602.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-3",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 50.0, 298.0, 327.5, 22.0 ],
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-2",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 428.0, 303.0, 150.0, 34.0 ],
													"text" : "simply multiply by factor\n"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-38",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 50.0, 523.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-35",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 50.0, 465.0, 149.0, 22.0 ],
													"text" : "+ 0."
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-34",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 50.0, 370.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-32",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 340.0, 39.0, 22.0 ],
													"text" : "round"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-31",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 50.0, 254.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-29",
													"maxclass" : "newobj",
													"numinlets" : 3,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 220.0, 209.0, 22.0 ],
													"text" : "expr $f1 + ($f3 * $f1)"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-28",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 50.0, 188.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-24",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 160.0, 79.0, 22.0 ],
													"text" : "expr $f1 - $f2"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-41",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 50.0, 25.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-42",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 110.0, 25.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-43",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 240.0, 25.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-45",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 50.0, 602.0, 30.0, 30.0 ]
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-24", 1 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-29", 1 ],
													"source" : [ "obj-1", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-35", 1 ],
													"source" : [ "obj-1", 2 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-29", 2 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-15", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-16", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-20", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-28", 0 ],
													"source" : [ "obj-24", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-39", 1 ],
													"source" : [ "obj-27", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-29", 0 ],
													"source" : [ "obj-28", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-31", 0 ],
													"source" : [ "obj-29", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-27", 0 ],
													"source" : [ "obj-3", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-32", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-31", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-34", 0 ],
													"source" : [ "obj-32", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-35", 0 ],
													"source" : [ "obj-33", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-37", 0 ],
													"source" : [ "obj-33", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-33", 0 ],
													"source" : [ "obj-34", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-38", 0 ],
													"source" : [ "obj-35", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-39", 0 ],
													"source" : [ "obj-37", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-45", 0 ],
													"source" : [ "obj-38", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-44", 0 ],
													"source" : [ "obj-39", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-8", 0 ],
													"source" : [ "obj-41", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-42", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-12", 0 ],
													"source" : [ "obj-43", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-44", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-24", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 434.0, 329.0, 227.5, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p stretching-pitch-space"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-40",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 188.0, 109.0, 132.0, 22.0 ],
									"text" : "route 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 188.0, 70.0, 38.0, 22.0 ],
									"text" : "zl.rev"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 346.0, 82.0, 51.0, 22.0 ],
									"text" : "0 57"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-27",
									"index" : 4,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 957.0, 117.5, 31.0, 31.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-19",
									"index" : 5,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1079.0, 117.5, 31.0, 31.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 301.0, 270.0, 49.5, 22.0 ],
									"text" : "t f f"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 434.0, 367.0, 30.5, 22.0 ],
									"text" : "t f f"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 301.0, 417.0, 49.5, 22.0 ],
									"text" : "pack i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 301.0, 481.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"precision" : 6
									}
,
									"text" : "coll ---#0_keydowns"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 2,
											"revision" : 2,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 1706.0, 175.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-11",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 171.0, 316.0, 50.0, 22.0 ],
													"text" : "59"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-9",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 108.0, 279.0, 47.0, 22.0 ],
													"text" : "zl.nth 2"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-8",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 171.0, 253.0, 50.0, 22.0 ],
													"text" : "20 59"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 81.0, 218.0, 46.0, 22.0 ],
													"text" : "route 0"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-5",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 171.0, 205.0, 50.0, 22.0 ],
													"text" : "20 59"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 81.0, 138.0, 19.0, 22.0 ],
													"text" : "t l"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 81.0, 176.0, 37.0, 22.0 ],
													"text" : "zl.rev"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-3",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 114.0, 89.0, 50.0, 22.0 ],
													"text" : "59 20"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-21",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 81.0, 40.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-22",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 108.0, 343.0, 30.0, 30.0 ]
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"order" : 0,
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"order" : 1,
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"order" : 1,
													"source" : [ "obj-21", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 1 ],
													"order" : 0,
													"source" : [ "obj-21", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-8", 1 ],
													"order" : 0,
													"source" : [ "obj-6", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-9", 0 ],
													"order" : 1,
													"source" : [ "obj-6", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 1 ],
													"order" : 0,
													"source" : [ "obj-9", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-22", 0 ],
													"order" : 1,
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 563.25, 181.0, 105.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p filterout-noteoffs"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 538.25, 238.5, 51.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-17",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 444.5, 674.5, 31.0, 31.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 226.0, 28.0, 94.0, 33.0 ],
									"text" : "keyboard input (midinote)"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-12",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 188.0, 27.5, 31.0, 31.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 772.0, 183.0, 102.0, 20.0 ],
									"text" : "curbing"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 590.25, 238.5, 85.0, 20.0 ],
									"text" : "central note"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-7",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 720.0, 238.5, 51.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 843.0, 238.5, 51.0, 22.0 ],
									"text" : "0."
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 720.0, 117.5, 31.0, 31.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 538.25, 117.5, 31.0, 31.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"source" : [ "obj-10", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 1 ],
									"order" : 0,
									"source" : [ "obj-100", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-92", 0 ],
									"order" : 1,
									"source" : [ "obj-100", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 1 ],
									"source" : [ "obj-100", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"order" : 0,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"order" : 1,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 1 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 1 ],
									"order" : 0,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"order" : 1,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 1 ],
									"order" : 0,
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-40", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"order" : 1,
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-94", 0 ],
									"source" : [ "obj-46", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"source" : [ "obj-50", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 1 ],
									"order" : 0,
									"source" : [ "obj-51", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 0 ],
									"order" : 1,
									"source" : [ "obj-51", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-56", 0 ],
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 1 ],
									"midpoints" : [ 386.5, 468.0, 482.0, 468.0 ],
									"source" : [ "obj-54", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-56", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"order" : 1,
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-65", 1 ],
									"order" : 0,
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-66", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-79", 0 ],
									"source" : [ "obj-68", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 2 ],
									"midpoints" : [ 729.5, 315.0, 652.0, 315.0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"order" : 0,
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 1 ],
									"order" : 1,
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-68", 0 ],
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-77", 0 ],
									"source" : [ "obj-75", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 1 ],
									"source" : [ "obj-77", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-83", 0 ],
									"source" : [ "obj-78", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-71", 1 ],
									"order" : 1,
									"source" : [ "obj-79", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 0 ],
									"order" : 0,
									"source" : [ "obj-79", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 0 ],
									"source" : [ "obj-8", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"source" : [ "obj-80", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"source" : [ "obj-80", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"order" : 0,
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-82", 1 ],
									"order" : 1,
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-89", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-100", 0 ],
									"source" : [ "obj-94", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-98", 1 ],
									"order" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-99", 0 ],
									"order" : 1,
									"source" : [ "obj-96", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 89.375, 692.0, 241.5, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p TransForms_calculation"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 810.0, 492.0, 46.0, 20.0 ],
					"text" : "default",
					"textjustification" : 2
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 949.0, 492.0, 101.0, 20.0 ],
					"text" : "pedal pressed"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-53",
					"items" : [ "tempered", ",", "generative", ",", "polytempo" ],
					"labelclick" : 1,
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 148.0, -4.0, 162.5, 23.0 ],
					"varname" : "DeviceMode"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-49",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "OSC-routing.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1164.5, 317.0, 236.0, 148.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 781.0, 3.5, 236.0, 139.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1463.0, 321.0, 137.0, 20.0 ],
					"text" : "(Polytempo)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1419.0, 320.0, 42.0, 22.0 ],
					"text" : "47522"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 967.0, 420.0, 128.0, 20.0 ],
					"text" : "ignore midi channel"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 928.0, 419.0, 39.0, 22.0 ],
					"text" : "$1 $2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1245.75, 179.0, 52.0, 22.0 ],
					"text" : "pack i i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 749.0, 212.0, 97.0, 22.0 ],
					"text" : "switch 2 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-61",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "bang" ],
					"patching_rect" : [ 928.0, 212.0, 29.5, 22.0 ],
					"text" : "t i b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 954.0, 168.0, 71.0, 20.0 ],
					"text" : "Standalone"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 928.0, 260.0, 29.5, 22.0 ],
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 928.0, 293.0, 97.0, 22.0 ],
					"text" : "switch 2 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 967.0, 212.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-93",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 749.0, 400.0, 58.0, 22.0 ],
					"text" : "toggle $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 749.0, 371.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-88",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 858.0, 375.0, 47.0, 47.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-84",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "int", "int" ],
					"patching_rect" : [ 481.0, 384.0, 83.0, 22.0 ],
					"text" : "live.thisdevice"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1073.0, 70.0, 39.0, 22.0 ],
					"text" : "$2 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1164.5, 175.0, 52.0, 22.0 ],
					"text" : "pack i i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-76",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 1245.75, 141.0, 52.0, 22.0 ],
					"text" : "notein"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1193.5, 23.0, 64.0, 22.0 ],
					"text" : "controllers"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.6, 0.6, 0.6, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.6, 0.6, 0.6, 1.0 ],
					"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "color",
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-71",
					"items" : [ "visuals IAC Bus 1", ",", "visuals #2", ",", "nanoKONTROL2 SLIDER/KNOB", ",", "UM-2 Port 1", ",", "Vienna Instruments MIDI", ",", "to Max 1", ",", "to Max 2" ],
					"labelclick" : 1,
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1193.5, 92.0, 123.5, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 4.0, 30.0, 135.5, 23.0 ],
					"varname" : "InputMenu"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-72",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1193.5, 59.0, 55.0, 23.0 ],
					"text" : "midiinfo"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-52",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 1164.5, 141.0, 52.0, 22.0 ],
					"text" : "ctlin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 858.0, 461.0, 31.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 858.0, 430.0, 29.5, 22.0 ],
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 858.0, 492.0, 89.0, 22.0 ],
					"text" : "gate 2 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 575.0, 114.0, 92.0, 35.0 ],
					"text" : "\"Akai APC Keys 25\" 25 48"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "int", "int" ],
					"patching_rect" : [ 550.0, -171.0, 83.0, 22.0 ],
					"text" : "live.thisdevice"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 453.0, 49.0, 98.0, 20.0 ],
					"text" : "Initial settings",
					"textjustification" : 2
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 749.0, 287.0, 121.000000000000114, 22.0 ],
					"text" : "route 67 64 90"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "live.toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 783.0, 328.0, 32.0, 30.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 548.0, 103.5, 13.0, 13.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "off", "on" ],
							"parameter_linknames" : 1,
							"parameter_longname" : "Pedal64",
							"parameter_mmax" : 1,
							"parameter_shortname" : "ped64",
							"parameter_type" : 2
						}

					}
,
					"varname" : "Pedal64"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "live.toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 749.0, 328.0, 32.0, 30.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 535.0, 103.5, 13.0, 13.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "off", "on" ],
							"parameter_linknames" : 1,
							"parameter_longname" : "Ped67",
							"parameter_mmax" : 1,
							"parameter_shortname" : "ped67",
							"parameter_type" : 2
						}

					}
,
					"varname" : "Ped67"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 8,
					"outlettype" : [ "", "", "", "int", "int", "", "int", "" ],
					"patching_rect" : [ 727.0, 70.0, 232.5, 22.0 ],
					"text" : "midiparse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 89.375, 899.0, 47.0, 22.0 ],
					"text" : "midiout"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.933333333333333, 0.0, 1.0 ],
					"id" : "obj-21",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1164.5, 479.0, 189.0, 22.0 ],
					"text" : "s ---#0_TransForms_M4L_receive"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.933333333333333, 0.0, 1.0 ],
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1164.5, 283.0, 175.0, 22.0 ],
					"text" : "r ---#0_TransForms_M4L_send"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.470588235294118, 0.470588235294118, 0.470588235294118, 1.0 ],
					"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "color",
					"gradient" : 1,
					"id" : "obj-14",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1530.25, 10.0, 55.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 899.0, 142.0, 47.0, 22.0 ],
					"text" : "partials",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.470588235294118, 0.470588235294118, 0.470588235294118, 1.0 ],
					"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "color",
					"gradient" : 1,
					"id" : "obj-12",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1459.75, 10.0, 38.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 856.0, 142.0, 42.0, 22.0 ],
					"text" : "sheet",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1530.25, 67.0, 35.0, 22.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1530.25, 112.0, 51.0, 22.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1459.75, 67.0, 35.0, 22.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1459.75, 112.0, 51.0, 22.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 481.0, 445.0, 94.0, 35.0 ],
					"text" : "tonedivision 4, accidentals 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 550.0, 14.0, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 766.0, 172.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 76.0, 111.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-21",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 291.0, 45.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 151.0, 111.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 540.0, 422.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-83",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 76.0, 174.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-81",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 76.0, 143.0, 94.0, 22.0 ],
									"text" : "sel 112"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-76",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 4,
									"outlettype" : [ "int", "int", "int", "int" ],
									"patching_rect" : [ 44.5, 53.0, 50.5, 22.0 ],
									"text" : "key"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 76.0, 284.0, 29.5, 22.0 ],
									"text" : "t l l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 451.0, 10.0, 70.0, 22.0 ],
									"text" : "loadmess 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 519.0, 318.0, 35.0, 22.0 ],
									"text" : "set 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 482.0, 290.0, 93.0, 22.0 ],
									"text" : "sel 0 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 482.0, 318.0, 35.0, 22.0 ],
									"text" : "set 0"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-13",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 451.0, 354.0, 82.0, 23.0 ],
									"text" : "settoggle $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 349.0, 48.5, 20.0, 20.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-15",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 349.0, 77.5, 65.0, 23.0 ],
									"text" : "toggle $1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 482.0, 228.5, 80.0, 23.0 ],
									"text" : "route toggle"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 482.0, 258.5, 20.0, 20.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 111.0, 367.0, 150.0, 20.0 ],
									"text" : "to umenu"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 166.0, 15.0, 150.0, 20.0 ],
									"text" : "from umenu"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 76.0, 362.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 131.0, 10.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"order" : 0,
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"order" : 1,
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"order" : 2,
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"order" : 0,
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"order" : 1,
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 1 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"source" : [ "obj-76", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-8", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-83", 0 ],
									"source" : [ "obj-81", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"order" : 1,
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"order" : 0,
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 400.5, 225.0, 74.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p usb_pedal"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 727.0, 23.0, 40.0, 22.0 ],
					"text" : "midiin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 481.0, 415.0, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-66",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 24.0, 352.0, 80.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 178.0, 287.0, 60.0, 20.0 ],
					"text" : "freq. (Hz)"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-42",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 291.5, 287.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 492.0, 163.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 47.0, 302.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 435.0, 191.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 436.0, 111.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 436.0, 79.0, 102.0, 22.0 ],
									"text" : "expr $f1 * (-1) + 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 435.0, 45.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 435.0, 163.0, 52.0, 22.0 ],
									"text" : "gate 1 1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-33",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 435.0, 8.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 435.0, 219.0, 91.0, 22.0 ],
									"text" : "send cell2block"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "bang" ],
									"patching_rect" : [ 64.0, 45.0, 100.0, 22.0 ],
									"text" : "t i b"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-30",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 172.0, 532.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-28",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 64.0, 532.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-25",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 64.0, 495.0, 39.0, 22.0 ],
									"text" : "/ 100."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-62",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 172.0, 495.0, 103.0, 22.0 ],
									"text" : "bach.mc2f @out t"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-59",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 444.0, 394.0, 104.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-23",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 172.0, 578.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-21",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 64.0, 457.0, 95.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-18",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 172.0, 457.0, 95.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 64.0, 410.0, 127.0, 22.0 ],
									"text" : "t f f"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 64.0, 176.0, 100.0, 22.0 ],
									"text" : "expr $f1 - $f2 + 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 145.0, 147.0, 68.0, 22.0 ],
									"text" : "route offset"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.941176, 0.690196, 0.196078, 1.0 ],
									"id" : "obj-27",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 145.0, 116.0, 85.0, 22.0 ],
									"text" : "r shear_global"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 64.0, 357.0, 55.0, 22.0 ],
									"text" : "zl.slice 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 64.0, 211.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 134.0, 279.5, 414.0, 22.0 ],
									"text" : "4531.174095 4531.174095 4480.99 4531.174095"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.882486820220947, 0.637522101402283, 0.242347598075867, 1.0 ],
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 64.0, 246.0, 70.0, 22.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"precision" : 6
									}
,
									"text" : "coll 88keys"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 64.0, 578.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 64.0, 8.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-12", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-62", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"order" : 2,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 1 ],
									"midpoints" : [ 73.5, 275.0, 538.5, 275.0 ],
									"order" : 0,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"order" : 1,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 1 ],
									"source" : [ "obj-31", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 1 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"source" : [ "obj-62", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 84.5, 221.0, 226.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p generative_root"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 24.0, 74.5, 29.5, 22.0 ],
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 24.0, 45.5, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 24.0, 175.0, 140.0, 22.0 ],
					"text" : "gate 3 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 1474.0, 373.0, 624.0, 643.0 ],
						"bglocked" : 0,
						"openinpresentation" : 1,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"showontab" : 0,
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-4",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 353.0, 187.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-47",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "shear_depth.maxpat",
									"numinlets" : 0,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 361.0, 17.0, 76.0, 96.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 296.0, 1.0, 76.0, 97.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 21, 20, -11 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-12",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 617.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 601.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 20, 20, 11 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-14",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 587.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 571.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 19, 20, -10 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-42",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 557.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 541.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 18, 20, 10 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-43",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 527.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 511.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 17, 20, -9 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-21",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 497.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 481.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 16, 20, 9 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-22",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 467.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 451.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 15, 20, -8 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-19",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 437.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 421.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 14, 20, 8 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-20",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 407.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 391.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 13, 20, -7 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-10",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 377.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 361.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 12, 20, 7 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-11",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 347.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 331.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 11, 20, -6 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-7",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 317.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 301.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 10, 20, 6 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-8",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 287.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 271.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 9, 20, -5 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-3",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 257.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 241.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 8, 20, 5 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-5",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 227.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 211.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 7, 20, -4 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-1",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 197.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 181.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 6, 20, 4 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-2",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 167.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 151.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 5, 20, -3 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-13",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 137.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 121.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 4, 20, 3 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-35",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 107.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 91.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 3, 20, -2 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-9",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 77.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 61.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 2, 20, 2 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-31",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 47.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 31.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 1, 20, 1 ],
									"bgmode" : 0,
									"border" : 0,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-29",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "cellblock_mirrored.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 23.0, 17.0, 289.0, 27.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 2.0, 1.0, 289.0, 27.0 ],
									"viewvisibility" : 1
								}

							}
 ],
						"lines" : [  ]
					}
,
					"patching_rect" : [ 1530.25, 145.0, 65.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p structure"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-44",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1583.0, 567.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1515.0, 673.0, 103.0, 22.0 ],
					"text" : "bach.mc2f @out t"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-36",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1515.0, 632.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1515.0, 598.0, 103.0, 22.0 ],
					"text" : "bach.f2mc @out t"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-55",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1515.0, 567.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 1465.0, 79.0, 1612.0, 960.0 ],
						"bglocked" : 0,
						"openinpresentation" : 1,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"showontab" : 0,
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 159.0, -6.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 822.5, 343.0, 69.0, 22.0 ],
									"text" : "r cell2block"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 822.5, 377.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 822.5, 417.0, 79.0, 22.0 ],
									"text" : "cellblock2coll"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.882486820220947, 0.637522101402283, 0.242347598075867, 1.0 ],
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 882.5, 449.0, 70.0, 22.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"precision" : 6
									}
,
									"text" : "coll 88keys"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 11.5, 685.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"linecount" : 3,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 21.5, 624.0, 150.0, 47.0 ],
									"text" : "if there is a zero, it means there is no value for this key"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 11.5, 597.0, 104.5, 22.0 ],
									"text" : "route 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 11.5, 560.0, 53.0, 22.0 ],
									"text" : "route 38"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 11.5, 488.0, 29.5, 22.0 ],
									"text" : "t l l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 11.5, 530.0, 55.0, 22.0 ],
									"text" : "$2 $1 $3"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.941176, 0.690196, 0.196078, 1.0 ],
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 736.5, 201.0, 103.0, 22.0 ],
									"text" : "s shear_cellblock"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 901.8333740234375, 190.666671752929688, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-59",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 901.8333740234375, 218.333328247070312, 143.0, 22.0 ],
									"text" : "cellblock_cleanup-format"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.941176, 0.690196, 0.196078, 1.0 ],
									"id" : "obj-27",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 11.5, 6.0, 101.0, 22.0 ],
									"text" : "r shear_cellblock"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 822.5, 128.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-102",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 822.5, 157.0, 49.0, 22.0 ],
									"text" : "cols $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-97",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 736.5, 14.666664123535156, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-95",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 2,
											"revision" : 2,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-8",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 241.0, 227.0, 29.5, 22.0 ],
													"text" : "+ 1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-7",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "bang", "" ],
													"patching_rect" : [ 230.0, 100.0, 30.0, 22.0 ],
													"text" : "t b l"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-4",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 241.0, 488.0, 53.0, 22.0 ],
													"text" : "rows $1"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-1",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 50.0, 55.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-79",
													"maxclass" : "number",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 118.0, 368.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-77",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 118.0, 332.0, 29.5, 22.0 ],
													"text" : "- 1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-76",
													"maxclass" : "number",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 241.0, 258.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-70",
													"maxclass" : "newobj",
													"numinlets" : 3,
													"numoutlets" : 3,
													"outlettype" : [ "", "", "" ],
													"patching_rect" : [ 241.0, 193.0, 148.0, 22.0 ],
													"text" : "route range offset"
												}

											}
, 											{
												"box" : 												{
													"color" : [ 0.941176, 0.690196, 0.196078, 1.0 ],
													"id" : "obj-27",
													"maxclass" : "newobj",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 230.0, 55.0, 87.0, 22.0 ],
													"text" : "r shear_global"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-69",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 488.0, 70.0, 22.0 ],
													"text" : "set 0 $1 $2"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-67",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 177.0, 436.0, 50.0, 22.0 ],
													"text" : "88 123"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-65",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 441.0, 48.0, 22.0 ],
													"text" : "pack i i"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-64",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 79.0, 410.0, 58.0, 22.0 ],
													"text" : "+ 35"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-63",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "int", "int" ],
													"patching_rect" : [ 50.0, 368.0, 48.0, 22.0 ],
													"text" : "t i i"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-62",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 50.0, 100.0, 24.0, 24.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-60",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "bang", "int" ],
													"patching_rect" : [ 50.0, 133.0, 93.0, 22.0 ],
													"text" : "t b 0"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-59",
													"maxclass" : "number",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 124.0, 258.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-57",
													"maxclass" : "number",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 50.0, 332.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-54",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 50.0, 293.0, 93.0, 22.0 ],
													"text" : "+ 0"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-53",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 258.0, 29.5, 22.0 ],
													"text" : "1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-50",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 3,
													"outlettype" : [ "bang", "bang", "int" ],
													"patching_rect" : [ 50.0, 227.0, 43.0, 22.0 ],
													"text" : "uzi 88"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-94",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 50.0, 570.0, 30.0, 30.0 ]
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-62", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-7", 0 ],
													"source" : [ "obj-27", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-94", 0 ],
													"midpoints" : [ 250.5, 555.0, 59.5, 555.0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-53", 0 ],
													"source" : [ "obj-50", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-54", 0 ],
													"source" : [ "obj-53", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-57", 0 ],
													"source" : [ "obj-54", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-59", 0 ],
													"order" : 0,
													"source" : [ "obj-57", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-63", 0 ],
													"order" : 1,
													"source" : [ "obj-57", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-54", 1 ],
													"source" : [ "obj-59", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-50", 0 ],
													"source" : [ "obj-60", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-59", 0 ],
													"source" : [ "obj-60", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-60", 0 ],
													"source" : [ "obj-62", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-64", 0 ],
													"source" : [ "obj-63", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-65", 0 ],
													"source" : [ "obj-63", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-65", 1 ],
													"source" : [ "obj-64", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-67", 1 ],
													"order" : 0,
													"source" : [ "obj-65", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-69", 0 ],
													"order" : 1,
													"source" : [ "obj-65", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-94", 0 ],
													"source" : [ "obj-69", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-62", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-70", 0 ],
													"source" : [ "obj-7", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-77", 0 ],
													"midpoints" : [ 315.0, 327.0, 127.5, 327.0 ],
													"source" : [ "obj-70", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-8", 0 ],
													"source" : [ "obj-70", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"order" : 0,
													"source" : [ "obj-76", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-50", 1 ],
													"order" : 1,
													"source" : [ "obj-76", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-79", 0 ],
													"source" : [ "obj-77", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-64", 1 ],
													"source" : [ "obj-79", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-76", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 736.5, 52.0, 115.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p cellblock_indexes"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 911.5, 23.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 967.0, 23.0, 50.0, 22.0 ],
									"text" : "Level-3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-45",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 967.0, 52.0, 115.0, 22.0 ],
									"text" : "prepend set current"
								}

							}
, 							{
								"box" : 								{
									"celldef" : [ [ 1, -2, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 1, 24, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 2, 10, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 3, -14, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 3, -11, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 3, 12, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 4, 17, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 5, -21, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 5, -18, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 5, 5, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 6, 22, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 7, -998, 0, 0, 0.980392, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 7, -26, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 7, -23, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 7, 0, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 9, -30, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 9, -27, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 9, -4, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 11, -33, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 11, -30, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 11, -7, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 13, -36, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 13, -33, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 13, -9, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 13, 24, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 15, -38, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 15, -35, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 15, -12, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 15, 22, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 15, 25, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 17, -40, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 17, -14, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 17, 20, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 17, 23, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 19, -42, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 19, -16, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 19, 18, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ], [ 19, 21, 0, 0, 0.980392156862745, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ] ],
									"cellmap" : [ [ 1, 0, "ROOT" ], [ 1, -6, 4098.044999861659562 ], [ 1, -13, 3.397734615720398 ], [ 1, -18, 2.642682478893643 ], [ 1, -22, -157.094620068384927 ], [ 1, -25, 1.698867307860199 ], [ 1, -28, 1.486508894377674 ], [ 1, -30, 1.251796963686462 ], [ 1, -32, 1.132578205240133 ], [ 1, -34, 1311.731285996825136 ], [ 1, -35, 1200.000000727047336 ], [ 1, -36, 1095.044591226638886 ], [ 1, -37, 996.08999899627122 ], [ 1, -39, 813.686286862211773 ], [ 1, -40, 729.219093392533978 ], [ 1, -41, 11.892071155021387 ], [ 1, -42, -2206.477718276411906 ], [ 1, -43, -2325.920526537510341 ], [ 1, -44, -2437.651811807288141 ], [ 1, -45, -2542.607221307693635 ], [ 1, -46, -2641.56181353806096 ], [ 1, -5, -701.954998199548186 ], [ 1, -10, -1199.999997334160071 ], [ 1, -14, -1586.313711198995634 ], [ 1, -17, 379.85615229708003 ], [ 1, -20, 90.646432892526718 ], [ 1, -24, 1.82954940846483 ], [ 1, -26, 1.585609487336186 ], [ 1, -29, 1.399067194708399 ], [ 1, -33, 1431.174094257921979 ], [ 1, -38, 902.48698459474349 ], [ 1, -4, 5.946035577510696 ], [ 1, -8, -968.825903803285428 ], [ 1, -11, 3.964023718340464 ], [ 1, -16, 2.973017788755348 ], [ 1, -23, 1.982011859170232 ], [ 1, -27, 2013.686286862211773 ], [ 1, -31, 1.189207115502139 ], [ 1, -15, 3213.686286862211546 ], [ 1, -21, 2.162194755458435 ], [ 1, -1, -270.780905799634297 ], [ 1, -2, -386.313712168391987 ], [ 1, -3, -470.780905638069669 ], [ 1, -7, 4.756828462008557 ], [ 1, -9, -1088.268712064382271 ], [ 1, -12, -1403.909999064934709 ], [ 1, -19, 2.378414231004279 ], [ 1, -47, 7.928047436680925 ], [ 1, -48, -2823.965525672122112 ], [ 1, -49, -2908.43271914179968 ], [ 1, -50, 1.484831197870561 ], [ 1, -51, 1.370613413418979 ], [ 1, -52, 5.946035577510694 ], [ 1, -53, 1.187864958296448 ], [ 1, -54, -770.780905395720538 ], [ 1, -55, 4.756828462008555 ], [ 1, -56, 1.048116139673337 ], [ 1, -57, 0.93778812497088 ], [ 1, -58, 0.890898718722336 ], [ 1, -59, 3.964023718340462 ], [ 1, -60, 0.833333334266575 ], [ 1, -62, 3.3333333370663 ], [ 1, -63, 0.66666666741326 ], [ 1, -64, 2.973017788755347 ], [ 1, -65, 0.625000000699931 ], [ 1, -66, 2.642682478893642 ], [ 1, -67, 2.378414231004277 ], [ 1, -68, 0.500000000559945 ], [ 1, -69, 2.162194755458434 ], [ 1, -61, 3.397734615720396 ], [ 1, -70, 2.00000000223978 ], [ 1, -72, 1.829549408464829 ], [ 1, -74, 1.585609487336185 ], [ 1, -76, 1.486508894377673 ], [ 1, -78, 1.251796963686462 ], [ 1, -79, 1.189207115502139 ], [ 1, -80, 1.132578205240132 ], [ 1, -82, 1.00000000111989 ], [ 1, -83, 0.476190476723757 ], [ 1, -84, 0.909090910108991 ], [ 1, -85, 0.424716827440686 ], [ 1, -86, 0.833333334266575 ], [ 1, -87, 0.769230770092223 ], [ 1, -88, 0.714285715085636 ], [ 1, -89, 0.66666666741326 ], [ 1, -71, 1.982011859170231 ], [ 1, -73, 1.698867307860198 ], [ 1, -75, 0.769230770092223 ], [ 1, -77, 1.399067194708399 ], [ 1, -81, 1.111111112355434 ], [ 1, -90, 0.312949241272084 ], [ 1, -91, 0.625000000699931 ], [ 1, -92, 0.588235294776406 ], [ 1, -93, 0.5263157900631 ], [ 1, -94, 0.500000000559945 ], [ 1, -95, 0.476190476723757 ], [ 1, -96, 0.232525094861855 ], [ 1, -316, 11.892071155021387 ], [ 1, -322, 7.928047436680925 ], [ 1, -327, 5.946035577510694 ], [ 1, -330, 4.756828462008555 ], [ 1, -334, 3.964023718340462 ], [ 1, -336, 3.397734615720396 ], [ 1, -339, 2.973017788755347 ], [ 1, -341, 2.642682478893642 ], [ 1, -342, 2.378414231004277 ], [ 1, -344, 2.162194755458434 ], [ 1, -346, 1.982011859170231 ], [ 1, -347, 1.829549408464829 ], [ 1, -348, 1.698867307860198 ], [ 1, -349, 1.585609487336185 ], [ 1, -351, 1.486508894377673 ], [ 1, -352, 1.399067194708399 ], [ 1, -353, 1.251796963686462 ], [ 1, -354, 1.189207115502139 ], [ 1, -355, 1.132578205240132 ], [ 1, -262, 6000.000000727046427 ], [ 1, -250, 7200.000000727047336 ], [ 1, -243, 7901.955001592434201 ], [ 1, -238, 8400.000000727046427 ], [ 1, -234, 8786.313714591880853 ], [ 1, -231, 9101.955001592434201 ], [ 1, -228, 9368.825907196171102 ], [ 1, -226, 9600.000000727046427 ], [ 1, -224, 9803.910002457821975 ], [ 1, -222, 9986.313714591880853 ], [ 1, -220, 10151.31794309180259 ], [ 1, -219, 10301.955001592434201 ], [ 1, -218, 10440.527662496355333 ], [ 1, -216, 10568.825907196171102 ], [ 1, -215, 10688.268715457268627 ], [ 1, -214, 10800.000000727046427 ], [ 1, -213, 10904.955410227454195 ], [ 1, -212, 11003.910002457821975 ], [ 1, -211, 11097.513016859349591 ], [ 1, -210, 11186.313714591880853 ], [ 1, -274, 4800.000000727046427 ], [ 1, -281, 4098.044999861659562 ], [ 1, -286, 3600.000000727046427 ], [ 1, -290, 3213.686286862211546 ], [ 1, -293, 2898.044999861659107 ], [ 1, -296, 2631.174094257922206 ], [ 1, -298, 2400.000000727046427 ], [ 1, -300, 2196.089998996271333 ], [ 1, -302, 2013.686286862211773 ], [ 1, -304, 1848.682058362289354 ], [ 1, -305, 1698.044999861659107 ], [ 1, -306, 1559.472338957736156 ], [ 1, -308, 1431.174094257921979 ], [ 1, -309, 1311.731285996825136 ], [ 1, -310, 1200.000000727047336 ], [ 1, -311, 1095.044591226638886 ], [ 1, -312, 996.08999899627122 ], [ 1, -313, 902.48698459474349 ], [ 1, -314, 813.686286862211773 ], [ 1, -315, 729.219093392533978 ], [ 1, 25, 2400.000000727046427 ], [ 1, 23, 2196.089998996271333 ], [ 1, 21, 2013.686286862211773 ], [ 1, 19, 1848.682058362289354 ], [ 1, 18, 1698.044999861659107 ], [ 1, 17, 1559.472338957736156 ], [ 1, 15, 1431.174094257921979 ], [ 1, 14, 1311.731285996825136 ], [ 1, 13, 1200.000000727047336 ], [ 1, 12, 1095.044591226638886 ], [ 1, 11, 996.08999899627122 ], [ 1, 10, 902.48698459474349 ], [ 1, 9, 813.686286862211773 ], [ 1, 8, 729.219093392533978 ], [ 1, 7, 11.892071155021393 ], [ 1, 1, 7.928047436680928 ], [ 2, 0, "Level-1" ], [ 2, -2, 24.916720515282908 ], [ 2, -6, 4098.044999861659562 ], [ 2, -5, 20.125043493113118 ], [ 2, -10, 15.389739141792385 ], [ 2, -14, 12.458360257641452 ], [ 2, -17, 10.465022616418821 ], [ 2, -20, 8.720852180349018 ], [ 2, -22, 7.267376816957515 ], [ 2, -24, 1.82954940846483 ], [ 2, -26, 1.585609487336186 ], [ 2, -28, 1.486508894377674 ], [ 2, -29, 1.399067194708399 ], [ 2, -30, 1.251796963686462 ], [ 2, -32, 1.132578205240133 ], [ 2, -33, 3.964023718340462 ], [ 2, -34, 3.737508077292436 ], [ 2, -35, 3.397734615720396 ], [ 2, -36, 3.270319567630882 ], [ 2, -37, 3.114590064410363 ], [ 2, -38, 2.973017788755347 ], [ 2, -9, 16.351597838154408 ], [ 2, -12, 13.76976660055108 ], [ 2, -15, 11.892071155021387 ], [ 2, -19, 2.378414231004279 ], [ 2, -21, 2.162194755458435 ], [ 2, -23, 1.982011859170232 ], [ 2, -25, 1.698867307860199 ], [ 2, -27, 5.450532612718136 ], [ 2, -31, 1.189207115502139 ], [ 2, -3, 23.784142310042775 ], [ 2, -11, 3.964023718340464 ], [ 2, -13, 3.397734615720398 ], [ 2, -18, 2.642682478893643 ], [ 2, -1, 26.162556541047053 ], [ 2, -7, 4.756828462008557 ], [ 2, -8, 17.441704360698036 ], [ 2, -16, 2.973017788755348 ], [ 2, -4, 5.946035577510696 ], [ 2, -39, 2.725266306359068 ], [ 2, -41, 2.378414231004277 ], [ 2, -42, 2.236115943679235 ], [ 2, -43, 2.162194755458434 ], [ 2, -44, 2.012504349311312 ], [ 2, -45, 1.982011859170231 ], [ 2, -46, 1.829549408464829 ], [ 2, -47, 1.698867307860198 ], [ 2, -48, 1.585609487336185 ], [ 2, -40, 2.642682478893642 ], [ 2, -49, 1.538973914179238 ], [ 2, -50, 1.486508894377673 ], [ 2, -51, 1.399067194708399 ], [ 2, -52, 1.251796963686462 ], [ 2, -53, 1.189207115502139 ], [ 2, -54, 1.132578205240132 ], [ 2, -55, 4.756828462008555 ], [ 2, -56, 1.048116139673337 ], [ 2, -57, 0.93778812497088 ], [ 2, -58, 0.890898718722336 ], [ 2, -59, 3.964023718340462 ], [ 2, -60, 0.833333334266575 ], [ 2, -62, 3.3333333370663 ], [ 2, -63, 0.66666666741326 ], [ 2, -64, 2.973017788755347 ], [ 2, -65, 0.625000000699931 ], [ 2, -66, 2.642682478893642 ], [ 2, -67, 2.378414231004277 ], [ 2, -68, 0.500000000559945 ], [ 2, -69, 2.162194755458434 ], [ 2, -61, 3.397734615720396 ], [ 2, -70, 2.00000000223978 ], [ 2, -72, 1.829549408464829 ], [ 2, -74, 1.585609487336185 ], [ 2, -76, 1.486508894377673 ], [ 2, -78, 1.251796963686462 ], [ 2, -79, 1.189207115502139 ], [ 2, -80, 1.132578205240132 ], [ 2, -82, 1.00000000111989 ], [ 2, -83, 0.476190476723757 ], [ 2, -84, 0.909090910108991 ], [ 2, -85, 0.424716827440686 ], [ 2, -86, 0.833333334266575 ], [ 2, -87, 0.769230770092223 ], [ 2, -88, 0.714285715085636 ], [ 2, -89, 0.66666666741326 ], [ 2, -71, 1.982011859170231 ], [ 2, -73, 1.698867307860198 ], [ 2, -75, 0.769230770092223 ], [ 2, -77, 1.399067194708399 ], [ 2, -81, 1.111111112355434 ], [ 2, -90, 0.312949241272084 ], [ 2, -91, 0.625000000699931 ], [ 2, -92, 0.588235294776406 ], [ 2, -93, 0.5263157900631 ], [ 2, -94, 0.500000000559945 ], [ 2, -95, 0.476190476723757 ], [ 2, -96, 0.232525094861855 ], [ 2, -316, 11.892071155021387 ], [ 2, -322, 7.928047436680925 ], [ 2, -327, 5.946035577510694 ], [ 2, -330, 4.756828462008555 ], [ 2, -334, 3.964023718340462 ], [ 2, -336, 3.397734615720396 ], [ 2, -339, 2.973017788755347 ], [ 2, -341, 2.642682478893642 ], [ 2, -342, 2.378414231004277 ], [ 2, -344, 2.162194755458434 ], [ 2, -346, 1.982011859170231 ], [ 2, -347, 1.829549408464829 ], [ 2, -348, 1.698867307860198 ], [ 2, -349, 1.585609487336185 ], [ 2, -351, 1.486508894377673 ], [ 2, -352, 1.399067194708399 ], [ 2, -353, 1.251796963686462 ], [ 2, -354, 1.189207115502139 ], [ 2, -355, 1.132578205240132 ], [ 2, -250, 7200.000000727047336 ], [ 2, -238, 8400.000000727046427 ], [ 2, -231, 9101.955001592434201 ], [ 2, -226, 9600.000000727046427 ], [ 2, -222, 9986.313714591880853 ], [ 2, -219, 10301.955001592434201 ], [ 2, -216, 10568.825907196171102 ], [ 2, -214, 10800.000000727046427 ], [ 2, -212, 11003.910002457821975 ], [ 2, -210, 11186.313714591880853 ], [ 2, -208, 11351.31794309180259 ], [ 2, -207, 11501.955001592434201 ], [ 2, -206, 11640.527662496355333 ], [ 2, -204, 11768.825907196171102 ], [ 2, -203, 11888.268715457268627 ], [ 2, -202, 12000.000000727046427 ], [ 2, -201, 12104.955410227454195 ], [ 2, -200, 12203.910002457821975 ], [ 2, -199, 12297.513016859349591 ], [ 2, -198, 12386.313714591880853 ], [ 2, -262, 6000.000000727046427 ], [ 2, -269, 5298.044999861659562 ], [ 2, -274, 4800.000000727046427 ], [ 2, -278, 4413.686286862212 ], [ 2, -281, 4098.044999861659562 ], [ 2, -284, 3831.174094257922206 ], [ 2, -286, 3600.000000727046427 ], [ 2, -288, 3396.089998996271333 ], [ 2, -290, 3213.686286862211546 ], [ 2, -292, 3048.682058362290263 ], [ 2, -293, 2898.044999861659107 ], [ 2, -294, 2759.472338957736156 ], [ 2, -296, 2631.174094257922206 ], [ 2, -297, 2511.731285996824226 ], [ 2, -298, 2400.000000727046427 ], [ 2, -299, 2295.044591226638659 ], [ 2, -300, 2196.089998996271333 ], [ 2, -301, 2102.486984594743717 ], [ 2, -302, 2013.686286862211773 ], [ 2, -303, 1929.219093392533978 ], [ 2, 7, 11.892071155021393 ], [ 2, 1, 7.928047436680928 ], [ 3, 0, "Level-2" ], [ 3, -95, 0.476190476723757 ], [ 3, -88, 0.714285715085636 ], [ 3, -83, 0.476190476723757 ], [ 3, -79, 1.189207115502139 ], [ 3, -76, 1.486508894377673 ], [ 3, -73, 1.698867307860198 ], [ 3, -71, 1.982011859170231 ], [ 3, -69, 2.162194755458434 ], [ 3, -67, 2.378414231004277 ], [ 3, -65, -1886.31371192604297 ], [ 3, -64, 2.973017788755347 ], [ 3, -62, -6386.313708290805153 ], [ 3, -61, 3.397734615720396 ], [ 3, -60, -4023.965525672122112 ], [ 3, -59, 3.964023718340462 ], [ 3, -58, -3841.56181353806096 ], [ 3, -57, -3742.607221307693635 ], [ 3, -56, -3637.651811807288141 ], [ 3, -55, 4.756828462008555 ], [ 3, -54, -3406.477718276411906 ], [ 3, -100, 0.183468 ], [ 3, -93, 0.5263157900631 ], [ 3, -84, 0.909090910108991 ], [ 3, -81, 1.111111112355434 ], [ 3, -78, 1.251796963686462 ], [ 3, -74, 1.585609487336185 ], [ 3, -72, 1.829549408464829 ], [ 3, -70, 2.00000000223978 ], [ 3, -66, 2.642682478893642 ], [ 3, -63, -6470.780901760483175 ], [ 3, -106, 0.127273 ], [ 3, -99, 0.190909 ], [ 3, -94, 0.500000000559945 ], [ 3, -90, 0.312949241272084 ], [ 3, -87, 0.769230770092223 ], [ 3, -82, 1.00000000111989 ], [ 3, -80, 1.132578205240132 ], [ 3, -75, 0.769230770092223 ], [ 3, -68, 0.500000000559945 ], [ 3, -109, 0.107023 ], [ 3, -102, 0.163636 ], [ 3, -97, 0.214046 ], [ 3, -85, 0.424716827440686 ], [ 3, -116, 0.072623 ], [ 3, -104, 0.145455 ], [ 3, -92, 0.588235294776406 ], [ 3, -86, 0.833333334266575 ], [ 3, -77, 1.399067194708399 ], [ 3, -124, 0.045867 ], [ 3, -117, 0.068801 ], [ 3, -112, 0.091734 ], [ 3, -108, 0.114538 ], [ 3, -105, 0.137601 ], [ 3, -98, 0.198757 ], [ 3, -96, 0.232525094861855 ], [ 3, -91, 0.625000000699931 ], [ 3, -89, 0.66666666741326 ], [ 3, -107, 0.122312 ], [ 3, -111, 0.097358 ], [ 3, -114, 0.080267 ], [ 3, -118, 0.064978 ], [ 3, -121, 0.053512 ], [ 3, -131, 0.030578 ], [ 3, -119, 0.061156 ], [ 3, -115, 0.076445 ], [ 3, -103, 0.15289 ], [ 3, -101, 0.168179 ], [ 3, -167, 0.003822 ], [ 3, -160, 0.005733 ], [ 3, -155, 0.007645 ], [ 3, -151, 0.009556 ], [ 3, -148, 0.011467 ], [ 3, -145, 0.013378 ], [ 3, -143, 0.015289 ], [ 3, -141, 0.017181 ], [ 3, -139, 0.019111 ], [ 3, -137, 0.021236 ], [ 3, -136, 0.022934 ], [ 3, -134, 0.025511 ], [ 3, -133, 0.026756 ], [ 3, -132, 0.028635 ], [ 3, -130, 0.032396 ], [ 3, -129, 0.0344 ], [ 3, -128, 0.036364 ], [ 3, -127, 0.038223 ], [ 3, -126, 0.040088 ], [ 3, -154, 0.008099 ], [ 3, -147, 0.012135 ], [ 3, -142, 0.016198 ], [ 3, -138, 0.020408 ], [ 3, -135, 0.02427 ], [ 3, -123, 0.04847 ], [ 3, -120, 0.057334 ], [ 3, -113, 0.085904 ], [ 3, -159, 0.006067 ], [ 3, -152, 0.009101 ], [ 3, -140, 0.018182 ], [ 3, -125, 0.042045 ], [ 3, -122, 0.049689 ], [ 3, -162, 0.005102 ], [ 3, -150, 0.010204 ], [ 3, -146, 0.012755 ], [ 3, -110, 0.1 ], [ 3, -175, 0.002311 ], [ 3, -11, 3.964023718340464 ], [ 3, -9, -1086.313712572307168 ], [ 3, -6, -770.78090636511547 ], [ 3, -2, -386.313713137788284 ], [ 3, -7, 4.756828462008557 ], [ 3, -5, -686.313712895437789 ], [ 3, -1, -297.513015405256454 ], [ 3, -4, 5.946035577510696 ], [ 3, -3, -470.780906607465965 ], [ 3, -8, -970.780906203549421 ], [ 3, -10, -1170.780906041983371 ], [ 3, -12, -1386.31371232995798 ], [ 3, -13, 3.397734615720398 ], [ 3, -14, -1586.31371216839193 ], [ 3, -17, -1901.954998199548299 ], [ 3, -19, 2.378414231004279 ], [ 3, -21, 2.162194755458435 ], [ 3, -23, 1.982011859170232 ], [ 3, -25, 1.698867307860199 ], [ 3, -26, 1.585609487336186 ], [ 3, -27, 2013.686286862211773 ], [ 3, -29, 1.399067194708399 ], [ 3, -30, 1.251796963686462 ], [ 3, -31, 1.189207115502139 ], [ 3, -32, 1.132578205240133 ], [ 3, -33, 1431.174094257921979 ], [ 3, -34, -1357.094620068384984 ], [ 3, -35, 1200.000000727047336 ], [ 3, -16, 2.973017788755348 ], [ 3, -20, -2168.8259038032852 ], [ 3, -28, 1.486508894377674 ], [ 3, -37, 996.08999899627122 ], [ 3, -38, -1766.338921470465039 ], [ 3, -39, 813.686286862211773 ], [ 3, -40, -2002.567716545637268 ], [ 3, -41, 11.892071155021387 ], [ 3, -42, 498.044999861659221 ], [ 3, -43, 359.47233895773536 ], [ 3, -44, -2437.651811807288141 ], [ 3, -24, 1.82954940846483 ], [ 3, -36, -1573.78131483856987 ], [ 3, -45, 231.174094257922036 ], [ 3, -18, 2.642682478893643 ], [ 3, -22, -2399.999997334160071 ], [ 3, -15, -1670.780905638069726 ], [ 3, -46, 111.731285996825136 ], [ 3, -47, 7.928047436680925 ], [ 3, -48, -203.91000100372878 ], [ 3, -49, -297.513015405256454 ], [ 3, -50, -386.313713137788284 ], [ 3, -51, -470.780906607465965 ], [ 3, -52, 5.946035577510694 ], [ 3, -53, -3278.179473576598411 ], [ 3, -316, 648.682058362289354 ], [ 3, -322, -104.955408773361114 ], [ 3, -327, 5.946035577510694 ], [ 3, -330, 4.756828462008555 ], [ 3, -334, 3.964023718340462 ], [ 3, -336, 3.397734615720396 ], [ 3, -339, 2.973017788755347 ], [ 3, -341, 2.642682478893642 ], [ 3, -342, 2.378414231004277 ], [ 3, -344, 2.162194755458434 ], [ 3, -346, 1.982011859170231 ], [ 3, -347, 1.829549408464829 ], [ 3, -348, 1.698867307860198 ], [ 3, -349, 1.585609487336185 ], [ 3, -351, 1.486508894377673 ], [ 3, -352, 1.399067194708399 ], [ 3, -353, 1.251796963686462 ], [ 3, -354, 1.189207115502139 ], [ 3, -355, 1.132578205240132 ], [ 3, -274, 4800.000000727046427 ], [ 3, -262, 6000.000000727046427 ], [ 3, -255, 6701.955001592434201 ], [ 3, -250, 7200.000000727047336 ], [ 3, -246, 7586.313714591880853 ], [ 3, -243, 7901.955001592434201 ], [ 3, -240, 8168.825907196171102 ], [ 3, -238, 8400.000000727046427 ], [ 3, -236, 8603.910002457821975 ], [ 3, -234, 8786.313714591880853 ], [ 3, -232, 8951.31794309180259 ], [ 3, -231, 9101.955001592434201 ], [ 3, -230, 9240.527662496355333 ], [ 3, -228, 9368.825907196171102 ], [ 3, -227, 9488.268715457268627 ], [ 3, -226, 9600.000000727046427 ], [ 3, -225, 9704.955410227454195 ], [ 3, -224, 9803.910002457821975 ], [ 3, -223, 9897.513016859349591 ], [ 3, -222, 9986.313714591880853 ], [ 3, -286, 3600.000000727046427 ], [ 3, -293, 2898.044999861659107 ], [ 3, -298, 2400.000000727046427 ], [ 3, -302, 2013.686286862211773 ], [ 3, -305, 1698.044999861659107 ], [ 3, -308, 1431.174094257921979 ], [ 3, -310, 1200.000000727047336 ], [ 3, -312, 996.08999899627122 ], [ 3, -314, 813.686286862211773 ], [ 3, -317, 498.044999861659221 ], [ 3, -318, 359.47233895773536 ], [ 3, -320, 231.174094257922036 ], [ 3, -321, 111.731285996825136 ], [ 3, -323, -203.91000100372878 ], [ 3, -324, -297.513015405256454 ], [ 3, -325, -386.313713137788284 ], [ 3, -326, -470.780906607465965 ], [ 3, 25, 2400.000000727046427 ], [ 3, 21, 2013.686286862211773 ], [ 3, 18, 1698.044999861659107 ], [ 3, 15, 1431.174094257921979 ], [ 3, 13, 1200.000000727047336 ], [ 3, 11, 996.08999899627122 ], [ 3, 9, 813.686286862211773 ], [ 3, 7, 11.892071155021393 ], [ 3, 6, 498.044999861659221 ], [ 3, 5, 359.47233895773536 ], [ 3, 3, 231.174094257922036 ], [ 3, 2, 111.731285996825136 ], [ 3, 1, 7.928047436680928 ], [ 4, 0, "Level-3" ], [ 4, -1, 1992.60143375791472 ], [ 4, -2, 4533.129095123308616 ], [ 4, -3, 1837.039097257998947 ], [ 4, -4, 5.946035577510696 ], [ 4, -6, 4098.044999861659562 ], [ 4, -9, 3750.637059227677582 ], [ 4, -11, 3.964023718340464 ], [ 4, -13, 3.397734615720398 ], [ 4, -15, 3213.686286862211546 ], [ 4, -17, 2996.999592092026887 ], [ 4, -18, 2.642682478893643 ], [ 4, -19, 2.378414231004279 ], [ 4, -21, 2.162194755458435 ], [ 4, -22, -176.224472711210467 ], [ 4, -23, 1.982011859170232 ], [ 4, -24, 1.82954940846483 ], [ 4, -25, 1.698867307860199 ], [ 4, -26, 1.585609487336186 ], [ 4, -27, -739.60681267267455 ], [ 4, -28, 1.486508894377674 ], [ 4, -8, 3915.641287727598865 ], [ 4, -10, 1099.387284319750052 ], [ 4, -12, 3461.427339823123475 ], [ 4, -14, 3333.129095123308616 ], [ 4, -16, 2.973017788755348 ], [ 4, -20, 2715.641287727598865 ], [ 4, -5, 1633.129095527224536 ], [ 4, -7, 4.756828462008557 ], [ 4, -30, 1.251796963686462 ], [ 4, -33, 1398.045001073404592 ], [ 4, -35, 1215.641288939345031 ], [ 4, -36, 1131.174095469667463 ], [ 4, -38, 2.969662395741121 ], [ 4, -40, 2.545424910635247 ], [ 4, -41, 11.892071155021387 ], [ 4, -42, 5.291336843844476 ], [ 4, -44, 2.00000000223978 ], [ 4, -45, 1.979774930494081 ], [ 4, -46, 1.781797437444673 ], [ 4, -47, 7.928047436680925 ], [ 4, -48, 1.61981585222243 ], [ 4, -49, 1.570399828948281 ], [ 4, -50, 1.484831197870561 ], [ 4, -51, 1.370613413418979 ], [ 4, -29, 1.399067194708399 ], [ 4, -31, 1.189207115502139 ], [ 4, -34, 3.563594874889346 ], [ 4, -37, 3.964023718340462 ], [ 4, -39, 2.669679709212079 ], [ 4, -32, 1.132578205240133 ], [ 4, -43, 2.227246796805841 ], [ 4, -52, 5.946035577510694 ], [ 4, -53, 1.187864958296448 ], [ 4, -54, 2.642682478893642 ], [ 4, -55, 4.756828462008555 ], [ 4, -56, 1.048116139673337 ], [ 4, -57, 0.93778812497088 ], [ 4, -58, 0.890898718722336 ], [ 4, -59, 3.964023718340462 ], [ 4, -60, 0.833333334266575 ], [ 4, -62, 3.3333333370663 ], [ 4, -63, 0.66666666741326 ], [ 4, -64, 2.973017788755347 ], [ 4, -65, 0.625000000699931 ], [ 4, -66, 2.642682478893642 ], [ 4, -67, 2.378414231004277 ], [ 4, -68, 0.500000000559945 ], [ 4, -69, 2.162194755458434 ], [ 4, -61, 3.397734615720396 ], [ 4, -70, 2.00000000223978 ], [ 4, -72, 1.829549408464829 ], [ 4, -74, 1.585609487336185 ], [ 4, -76, 1.486508894377673 ], [ 4, -78, 1.251796963686462 ], [ 4, -79, 1.189207115502139 ], [ 4, -80, 1.132578205240132 ], [ 4, -82, 1.00000000111989 ], [ 4, -83, 0.476190476723757 ], [ 4, -84, 0.909090910108991 ], [ 4, -85, 0.424716827440686 ], [ 4, -86, 0.833333334266575 ], [ 4, -87, 0.769230770092223 ], [ 4, -88, 0.714285715085636 ], [ 4, -89, 0.66666666741326 ], [ 4, -71, 1.982011859170231 ], [ 4, -73, 1.698867307860198 ], [ 4, -75, 0.769230770092223 ], [ 4, -77, 1.399067194708399 ], [ 4, -81, 1.111111112355434 ], [ 4, -90, 0.312949241272084 ], [ 4, -91, 0.625000000699931 ], [ 4, -92, 0.588235294776406 ], [ 4, -93, 0.5263157900631 ], [ 4, -94, 0.500000000559945 ], [ 4, -95, 0.476190476723757 ], [ 4, -96, 0.232525094861855 ], [ 4, -316, 11.892071155021387 ], [ 4, -322, 7.928047436680925 ], [ 4, -327, 5.946035577510694 ], [ 4, -330, 4.756828462008555 ], [ 4, -334, 3.964023718340462 ], [ 4, -336, 3.397734615720396 ], [ 4, -339, 2.973017788755347 ], [ 4, -341, 2.642682478893642 ], [ 4, -342, 2.378414231004277 ], [ 4, -344, 2.162194755458434 ], [ 4, -346, 1.982011859170231 ], [ 4, -347, 1.829549408464829 ], [ 4, -348, 1.698867307860198 ], [ 4, -349, 1.585609487336185 ], [ 4, -351, 1.486508894377673 ], [ 4, -352, 1.399067194708399 ], [ 4, -353, 1.251796963686462 ], [ 4, -354, 1.189207115502139 ], [ 4, -355, 1.132578205240132 ], [ 4, -243, 7901.955001592434201 ], [ 4, -231, 9101.955001592434201 ], [ 4, -224, 9803.910002457821975 ], [ 4, -219, 10301.955001592434201 ], [ 4, -215, 10688.268715457268627 ], [ 4, -212, 11003.910002457821975 ], [ 4, -209, 11270.780908061558875 ], [ 4, -207, 11501.955001592434201 ], [ 4, -205, 11705.865003323207929 ], [ 4, -203, 11888.268715457268627 ], [ 4, -201, 12053.272943957190364 ], [ 4, -200, 12203.910002457821975 ], [ 4, -199, 12342.482663361744926 ], [ 4, -197, 12470.780908061558875 ], [ 4, -196, 12590.223716322656401 ], [ 4, -195, 12701.95500159243602 ], [ 4, -194, 12806.91041109284015 ], [ 4, -193, 12905.865003323207929 ], [ 4, -192, 12999.468017724737365 ], [ 4, -191, 13088.268715457268627 ], [ 4, -255, 6701.955001592434201 ], [ 4, -262, 6000.000000727046427 ], [ 4, -267, 5501.955001592434201 ], [ 4, -271, 5115.641287727598865 ], [ 4, -274, 4800.000000727046427 ], [ 4, -277, 4533.129095123308616 ], [ 4, -279, 4301.955001592434201 ], [ 4, -281, 4098.044999861659562 ], [ 4, -283, 3915.641287727598865 ], [ 4, -284, 3750.637059227677582 ], [ 4, -286, 3600.000000727046427 ], [ 4, -287, 3461.427339823123475 ], [ 4, -289, 3333.129095123308616 ], [ 4, -290, 3213.686286862211546 ], [ 4, -291, 3101.955001592433746 ], [ 4, -292, 2996.999592092026887 ], [ 4, -293, 2898.044999861659107 ], [ 4, -294, 2804.441985460131491 ], [ 4, -295, 2715.641287727598865 ], [ 4, -296, 2631.174094257922206 ], [ 4, 7, 11.892071155021393 ], [ 4, 1, 7.928047436680928 ], [ 0, 1, 36 ], [ 0, 2, 37 ], [ 0, 3, 38 ], [ 0, 4, 39 ], [ 0, 5, 40 ], [ 0, 6, 41 ], [ 0, 7, 42 ], [ 0, 8, 43 ], [ 0, 9, 44 ], [ 0, 10, 45 ], [ 0, 11, 46 ], [ 0, 12, 47 ], [ 0, 13, 48 ], [ 0, 14, 49 ], [ 0, 15, 50 ], [ 0, 16, 51 ], [ 0, 17, 52 ], [ 0, 18, 53 ], [ 0, 19, 54 ], [ 0, 20, 55 ], [ 0, 21, 56 ], [ 0, 22, 57 ], [ 0, 23, 58 ], [ 0, 24, 59 ], [ 0, 25, 60 ], [ 5, -6, -806.910409638747751 ], [ 5, -11, 3.964023718340464 ], [ 5, -15, -1672.735907068937422 ], [ 5, -18, 2.642682478893643 ], [ 5, -21, 2.162194755458435 ], [ 5, -23, 1.982011859170232 ], [ 5, -25, 1.698867307860199 ], [ 5, -27, -2870.780904668672065 ], [ 5, -29, 1.399067194708399 ], [ 5, -30, 1.251796963686462 ], [ 5, -31, 1.189207115502139 ], [ 5, -33, -3488.268712064383635 ], [ 5, -34, 1311.731285996823772 ], [ 5, -35, -1522.098848568306494 ], [ 5, -36, -1588.268713599259627 ], [ 5, -37, 996.08999899627122 ], [ 5, -38, -1811.308567972861283 ], [ 5, -39, -1939.606812672675915 ], [ 5, -40, 729.219093392533978 ], [ 5, -41, 11.892071155021387 ], [ 5, -42, 498.044999861659221 ], [ 5, -44, 294.13499813088464 ], [ 5, -45, -2468.29392233585304 ], [ 5, -46, 111.731285996823715 ], [ 5, -47, 7.928047436680925 ], [ 5, -48, -203.91000100372878 ], [ 5, -49, -342.48266190765122 ], [ 5, -50, 1.484831197870561 ], [ 5, -51, -470.780906607465965 ], [ 5, -3, -470.780906607465965 ], [ 5, -9, -1088.268714003176228 ], [ 5, -13, 3.397734615720398 ], [ 5, -17, -1872.735906907371373 ], [ 5, -19, 2.378414231004279 ], [ 5, -22, -2372.735906503456135 ], [ 5, -24, 1.82954940846483 ], [ 5, -26, 1.585609487336186 ], [ 5, -28, 1.486508894377674 ], [ 5, -1, -342.48266190765122 ], [ 5, -2, -405.865002273030484 ], [ 5, -4, 5.946035577510696 ], [ 5, -5, -701.955000138340779 ], [ 5, -8, -999.468016270643034 ], [ 5, -10, -1172.735907472852432 ], [ 5, -12, -1388.268713760825904 ], [ 5, -14, -1588.268713599259627 ], [ 5, -7, 4.756828462008557 ], [ 5, -16, 2.973017788755348 ], [ 5, -20, -2172.735906665022412 ], [ 5, -32, 1.132578205240133 ], [ 5, -43, -2275.736315703957644 ], [ 5, -53, -701.955000138340779 ], [ 5, -54, -806.910409638747751 ], [ 5, -55, 4.756828462008555 ], [ 5, -56, -999.468016270643034 ], [ 5, -57, -1088.268714003176228 ], [ 5, -58, -1172.735907472852432 ], [ 5, -59, 3.964023718340462 ], [ 5, -60, -3980.134474441984821 ], [ 5, -52, 5.946035577510694 ], [ 5, -61, 3.397734615720396 ], [ 5, -62, -4227.875527402895386 ], [ 5, -63, -4339.606812672675005 ], [ 5, -64, 2.973017788755347 ], [ 5, -65, -4543.516814403448734 ], [ 5, -66, 2.642682478893642 ], [ 5, -67, 2.378414231004277 ], [ 5, -68, -4810.387720007186545 ], [ 5, -69, 2.162194755458434 ], [ 5, -70, -7172.735902625870949 ], [ 5, -71, 1.982011859170231 ], [ 5, -72, 1.829549408464829 ], [ 5, -73, 1.698867307860198 ], [ 5, -74, 1.585609487336185 ], [ 5, -75, 0.769230770092223 ], [ 5, -76, 1.486508894377673 ], [ 5, -77, 1.399067194708399 ], [ 5, -78, 1.251796963686462 ], [ 5, -79, 1.189207115502139 ], [ 5, -80, 1.132578205240132 ], [ 5, -81, 1.111111112355434 ], [ 5, -82, 1.00000000111989 ], [ 5, -83, 0.476190476723757 ], [ 5, -84, 0.909090910108991 ], [ 5, -85, 0.424716827440686 ], [ 5, -86, 0.833333334266575 ], [ 5, -87, 0.769230770092223 ], [ 5, -88, 0.714285715085636 ], [ 5, -89, 0.66666666741326 ], [ 5, -90, 0.312949241272084 ], [ 5, -91, 0.625000000699931 ], [ 5, -92, 0.588235294776406 ], [ 5, -93, 0.5263157900631 ], [ 5, -94, 0.500000000559945 ], [ 5, -95, 0.476190476723757 ], [ 5, -96, 0.232525094861855 ], [ 5, -316, 11.892071155021387 ], [ 5, -322, -53.272942503097909 ], [ 5, -327, -590.223714868562865 ], [ 5, -330, -905.865001869115304 ], [ 5, -334, 3.964023718340462 ], [ 5, -336, 3.397734615720396 ], [ 5, -339, 2.973017788755347 ], [ 5, -341, 2.642682478893642 ], [ 5, -342, 2.378414231004277 ], [ 5, -344, 2.162194755458434 ], [ 5, -346, 1.982011859170231 ], [ 5, -347, 1.829549408464829 ], [ 5, -348, 1.698867307860198 ], [ 5, -349, 1.585609487336185 ], [ 5, -351, 1.486508894377673 ], [ 5, -352, 1.399067194708399 ], [ 5, -353, 1.251796963686462 ], [ 5, -354, 1.189207115502139 ], [ 5, -355, 1.132578205240132 ], [ 5, -281, 4098.044999861659562 ], [ 5, -269, 5298.044999861659562 ], [ 5, -262, 6000.000000727046427 ], [ 5, -257, 6498.044999861659562 ], [ 5, -253, 6884.358713726493079 ], [ 5, -250, 7200.000000727047336 ], [ 5, -247, 7466.870906330784237 ], [ 5, -245, 7698.044999861659562 ], [ 5, -243, 7901.955001592434201 ], [ 5, -241, 8084.358713726493079 ], [ 5, -240, 8249.362942226414816 ], [ 5, -238, 8400.000000727046427 ], [ 5, -237, 8538.572661630969378 ], [ 5, -235, 8666.870906330783328 ], [ 5, -234, 8786.313714591880853 ], [ 5, -233, 8898.044999861658653 ], [ 5, -232, 9003.000409362066421 ], [ 5, -231, 9101.955001592434201 ], [ 5, -230, 9195.558015993961817 ], [ 5, -229, 9284.358713726493079 ], [ 5, -293, 2898.044999861659107 ], [ 5, -300, 2196.089998996271333 ], [ 5, -305, 1698.044999861659107 ], [ 5, -309, 1311.731285996823772 ], [ 5, -312, 996.08999899627122 ], [ 5, -315, 729.219093392533978 ], [ 5, -317, 498.044999861659221 ], [ 5, -319, 294.13499813088464 ], [ 5, -321, 111.731285996823715 ], [ 5, -323, -203.91000100372878 ], [ 5, -324, -342.48266190765122 ], [ 5, -326, -470.780906607465965 ], [ 5, -328, -701.955000138340779 ], [ 5, -329, -806.910409638747751 ], [ 5, -331, -999.468016270643034 ], [ 5, -332, -1088.268714003176228 ], [ 5, -333, -1172.735907472852432 ], [ 5, 23, 2196.089998996271333 ], [ 5, 18, 1698.044999861659107 ], [ 5, 14, 1311.731285996823772 ], [ 5, 11, 996.08999899627122 ], [ 5, 8, 729.219093392533978 ], [ 5, 6, 498.044999861659221 ], [ 5, 4, 294.13499813088464 ], [ 5, 2, 111.731285996823715 ], [ 5, 1, 7.928047436680928 ], [ 5, 7, 11.892071155021393 ], [ 6, -6, 4098.044999861659562 ], [ 6, -13, 3.397734615720398 ], [ 6, -18, 2.642682478893643 ], [ 6, -22, -241.561813538061188 ], [ 6, -25, 1.698867307860199 ], [ 6, -28, 1.486508894377674 ], [ 6, -30, 1.251796963686462 ], [ 6, -32, 1.132578205240133 ], [ 6, -34, 3.563594874889346 ], [ 6, -35, 7.928047436680925 ], [ 6, -36, 3.3333333370663 ], [ 6, -37, 3.964023718340462 ], [ 6, -39, 2.669679709212079 ], [ 6, -40, 2.545424910635247 ], [ 6, -41, 11.892071155021387 ], [ 6, -42, 5.291336843844476 ], [ 6, -43, 2.227246796805841 ], [ 6, -44, 2.00000000223978 ], [ 6, -45, 1.979774930494081 ], [ 6, -46, 1.781797437444673 ], [ 6, -11, 3.964023718340464 ], [ 6, -23, 1.982011859170232 ], [ 6, -27, -2870.780901760483175 ], [ 6, -33, 3.813828156017255 ], [ 6, -38, 2.969662395741121 ], [ 6, -47, 7.928047436680925 ], [ 6, -48, 1.61981585222243 ], [ 6, -49, 1.570399828948281 ], [ 6, -50, 1.484831197870561 ], [ 6, -51, 1.370613413418979 ], [ 6, -3, 4413.686286862212 ], [ 6, -9, 3831.174094257922206 ], [ 6, -15, 3213.686286862211546 ], [ 6, -17, 397.432283454362789 ], [ 6, -19, 2.378414231004279 ], [ 6, -21, 2.162194755458435 ], [ 6, -24, 1.82954940846483 ], [ 6, -26, 1.585609487336186 ], [ 6, -1, 4596.089998996270879 ], [ 6, -4, 5.946035577510696 ], [ 6, -5, 4248.682058362290263 ], [ 6, -7, 4.756828462008557 ], [ 6, -8, 1321.417966966106405 ], [ 6, -10, 3711.731285996824226 ], [ 6, -2, 1927.264092931062578 ], [ 6, -12, 3495.044591226638659 ], [ 6, -14, 3302.486984594743717 ], [ 6, -16, 2.973017788755348 ], [ 6, -20, 74.079473462489887 ], [ 6, -29, 1.399067194708399 ], [ 6, -31, 1.189207115502139 ], [ 6, -52, 5.946035577510694 ], [ 6, -53, 1.187864958296448 ], [ 6, -54, 2.642682478893642 ], [ 6, -55, 4.756828462008555 ], [ 6, -56, 1.048116139673337 ], [ 6, -57, 0.93778812497088 ], [ 6, -58, 0.890898718722336 ], [ 6, -59, 3.964023718340462 ], [ 6, -60, 0.833333334266575 ], [ 6, -62, 3.3333333370663 ], [ 6, -63, 0.66666666741326 ], [ 6, -64, 2.973017788755347 ], [ 6, -65, 0.625000000699931 ], [ 6, -66, 2.642682478893642 ], [ 6, -67, 2.378414231004277 ], [ 6, -68, 0.500000000559945 ], [ 6, -69, 2.162194755458434 ], [ 6, -61, 3.397734615720396 ], [ 6, -70, 2.00000000223978 ], [ 6, -72, 1.829549408464829 ], [ 6, -74, 1.585609487336185 ], [ 6, -76, 1.486508894377673 ], [ 6, -78, 1.251796963686462 ], [ 6, -79, 1.189207115502139 ], [ 6, -80, 1.132578205240132 ], [ 6, -82, 1.00000000111989 ], [ 6, -83, 0.476190476723757 ], [ 6, -84, 0.909090910108991 ], [ 6, -85, 0.424716827440686 ], [ 6, -86, 0.833333334266575 ], [ 6, -87, 0.769230770092223 ], [ 6, -88, 0.714285715085636 ], [ 6, -89, 0.66666666741326 ], [ 6, -71, 1.982011859170231 ], [ 6, -73, 1.698867307860198 ], [ 6, -75, 0.769230770092223 ], [ 6, -77, 1.399067194708399 ], [ 6, -81, 1.111111112355434 ], [ 6, -90, 0.312949241272084 ], [ 6, -91, 0.625000000699931 ], [ 6, -92, 0.588235294776406 ], [ 6, -93, 0.5263157900631 ], [ 6, -94, 0.500000000559945 ], [ 6, -95, 0.476190476723757 ], [ 6, -96, 0.232525094861855 ], [ 6, -316, 11.892071155021387 ], [ 6, -322, 7.928047436680925 ], [ 6, -327, 5.946035577510694 ], [ 6, -330, 4.756828462008555 ], [ 6, -334, 3.964023718340462 ], [ 6, -336, 3.397734615720396 ], [ 6, -339, 2.973017788755347 ], [ 6, -341, 2.642682478893642 ], [ 6, -342, 2.378414231004277 ], [ 6, -344, 2.162194755458434 ], [ 6, -346, 1.982011859170231 ], [ 6, -347, 1.829549408464829 ], [ 6, -348, 1.698867307860198 ], [ 6, -349, 1.585609487336185 ], [ 6, -351, 1.486508894377673 ], [ 6, -352, 1.399067194708399 ], [ 6, -353, 1.251796963686462 ], [ 6, -354, 1.189207115502139 ], [ 6, -355, 1.132578205240132 ], [ 6, -238, 8400.000000727046427 ], [ 6, -226, 9600.000000727046427 ], [ 6, -219, 10301.955001592434201 ], [ 6, -214, 10800.000000727046427 ], [ 6, -210, 11186.313714591880853 ], [ 6, -207, 11501.955001592434201 ], [ 6, -204, 11768.825907196171102 ], [ 6, -202, 12000.000000727046427 ], [ 6, -200, 12203.910002457821975 ], [ 6, -198, 12386.313714591880853 ], [ 6, -196, 12551.31794309180259 ], [ 6, -195, 12701.95500159243602 ], [ 6, -194, 12840.527662496355333 ], [ 6, -192, 12968.825907196171102 ], [ 6, -191, 13088.268715457268627 ], [ 6, -190, 13200.000000727046427 ], [ 6, -189, 13304.955410227456014 ], [ 6, -188, 13403.910002457820156 ], [ 6, -187, 13497.513016859349591 ], [ 6, -186, 13586.313714591880853 ], [ 6, -250, 7200.000000727047336 ], [ 6, -257, 6498.044999861659562 ], [ 6, -262, 6000.000000727046427 ], [ 6, -266, 5613.686286862212 ], [ 6, -269, 5298.044999861659562 ], [ 6, -272, 5031.174094257921752 ], [ 6, -274, 4800.000000727046427 ], [ 6, -276, 4596.089998996270879 ], [ 6, -278, 4413.686286862212 ], [ 6, -280, 4248.682058362290263 ], [ 6, -281, 4098.044999861659562 ], [ 6, -282, 3959.472338957736156 ], [ 6, -284, 3831.174094257922206 ], [ 6, -285, 3711.731285996824226 ], [ 6, -286, 3600.000000727046427 ], [ 6, -287, 3495.044591226638659 ], [ 6, -288, 3396.089998996271333 ], [ 6, -289, 3302.486984594743717 ], [ 6, -290, 3213.686286862211546 ], [ 6, -291, 3129.219093392533978 ], [ 6, 7, 11.892071155021393 ], [ 6, 1, 7.928047436680928 ], [ 7, -11, 3.964023718340464 ], [ 7, -18, 2.642682478893643 ], [ 7, -23, 1.982011859170232 ], [ 7, -27, -2870.780905638068361 ], [ 7, -30, 1.251796963686462 ], [ 7, -33, -3488.268712064383635 ], [ 7, -35, 1200.000000727047336 ], [ 7, -36, -3803.909999064934709 ], [ 7, -38, -3986.313711198994497 ], [ 7, -40, -2020.143847702920084 ], [ 7, -41, 11.892071155021387 ], [ 7, -42, 498.044999861659221 ], [ 7, -44, -2437.651811807288141 ], [ 7, -45, 231.174094257922036 ], [ 7, -46, -2557.094620068384756 ], [ 7, -47, 7.928047436680925 ], [ 7, -48, -203.91000100372878 ], [ 7, -49, -2872.735907068937195 ], [ 7, -50, -386.313713137788284 ], [ 7, -51, -3139.60681267267455 ], [ 7, -3, -468.825906145993088 ], [ 7, -6, -840.52766104226464 ], [ 7, -9, -1088.268714003174864 ], [ 7, -13, 3.397734615720398 ], [ 7, -15, -1670.780906607466022 ], [ 7, -17, -1886.313712895437675 ], [ 7, -19, 2.378414231004279 ], [ 7, -21, 2.162194755458435 ], [ 7, -22, -2370.780906041983144 ], [ 7, -24, 1.82954940846483 ], [ 7, -25, 1.698867307860199 ], [ 7, -26, 1.585609487336186 ], [ 7, -28, 1.486508894377674 ], [ 7, -998, 0.0 ], [ 7, -2, -386.313713137788284 ], [ 7, -4, 5.946035577510696 ], [ 7, -5, -701.955000138340779 ], [ 7, -8, -968.825905742078021 ], [ 7, -10, -1199.999999272952664 ], [ 7, -12, -1403.910001003728667 ], [ 7, -14, -1586.313713137788227 ], [ 7, -1, -340.527661446178342 ], [ 7, -16, 2.973017788755348 ], [ 7, -20, -2170.780906203549421 ], [ 7, -29, 1.399067194708399 ], [ 7, -31, 1.189207115502139 ], [ 7, -32, 1.132578205240133 ], [ 7, -34, -3599.999997334160071 ], [ 7, -7, 4.756828462008557 ], [ 7, -37, -3897.513013466462326 ], [ 7, -39, 813.686286862211773 ], [ 7, -43, -2309.353567107473282 ], [ 7, -52, 5.946035577510694 ], [ 7, -53, -701.955000138340779 ], [ 7, -54, -840.52766104226464 ], [ 7, -55, 4.756828462008555 ], [ 7, -57, -1088.268714003174864 ], [ 7, -59, 3.964023718340462 ], [ 7, -60, -1403.910001003728667 ], [ 7, -61, 3.397734615720396 ], [ 7, -63, -1670.780906607466022 ], [ 7, -64, 2.973017788755347 ], [ 7, -65, -4478.179473576598866 ], [ 7, -66, 2.642682478893642 ], [ 7, -67, 2.378414231004277 ], [ 7, -68, -4837.651811807288141 ], [ 7, -69, 2.162194755458434 ], [ 7, -70, -5041.56181353806096 ], [ 7, -56, -968.825905742078021 ], [ 7, -58, -1199.999999272952664 ], [ 7, -62, -1586.313713137788227 ], [ 7, -71, 1.982011859170231 ], [ 7, -72, 1.829549408464829 ], [ 7, -73, 1.698867307860198 ], [ 7, -74, 1.585609487336185 ], [ 7, -75, -7670.780901760483175 ], [ 7, -76, 1.486508894377673 ], [ 7, -77, 1.399067194708399 ], [ 7, -78, 1.251796963686462 ], [ 7, -79, 1.189207115502139 ], [ 7, -80, 1.132578205240132 ], [ 7, -81, 1.111111112355434 ], [ 7, -82, 1.00000000111989 ], [ 7, -83, 0.476190476723757 ], [ 7, -84, 0.909090910108991 ], [ 7, -85, 0.424716827440686 ], [ 7, -86, 0.833333334266575 ], [ 7, -87, 0.769230770092223 ], [ 7, -88, 0.714285715085636 ], [ 7, -89, 0.66666666741326 ], [ 7, -90, 0.312949241272084 ], [ 7, -91, 0.625000000699931 ], [ 7, -92, 0.588235294776406 ], [ 7, -93, 0.5263157900631 ], [ 7, -94, 0.500000000559945 ], [ 7, -95, 0.476190476723757 ], [ 7, -96, 0.232525094861855 ], [ 7, -316, 11.892071155021387 ], [ 7, -322, 0.000000727047222 ], [ 7, -327, -551.317941637711328 ], [ 7, -330, 4.756828462008555 ], [ 7, -334, -1304.955408773361114 ], [ 7, -336, -1497.51301540525651 ], [ 7, -339, 2.973017788755347 ], [ 7, -341, 2.642682478893642 ], [ 7, -342, 2.378414231004277 ], [ 7, -344, 2.162194755458434 ], [ 7, -346, 1.982011859170231 ], [ 7, -347, 1.829549408464829 ], [ 7, -348, 1.698867307860198 ], [ 7, -349, 1.585609487336185 ], [ 7, -351, 1.486508894377673 ], [ 7, -352, 1.399067194708399 ], [ 7, -353, 1.251796963686462 ], [ 7, -354, 1.189207115502139 ], [ 7, -355, 1.132578205240132 ], [ 7, -286, 3600.000000727046427 ], [ 7, -274, 4800.000000727046427 ], [ 7, -267, 5501.955001592434201 ], [ 7, -262, 6000.000000727046427 ], [ 7, -258, 6386.313714591880853 ], [ 7, -255, 6701.955001592434201 ], [ 7, -252, 6968.825907196171102 ], [ 7, -250, 7200.000000727047336 ], [ 7, -248, 7403.910002457821975 ], [ 7, -246, 7586.313714591880853 ], [ 7, -244, 7751.31794309180259 ], [ 7, -243, 7901.955001592434201 ], [ 7, -242, 8040.527662496356243 ], [ 7, -240, 8168.825907196171102 ], [ 7, -239, 8288.268715457268627 ], [ 7, -238, 8400.000000727046427 ], [ 7, -237, 8504.955410227454195 ], [ 7, -236, 8603.910002457821975 ], [ 7, -235, 8697.513016859349591 ], [ 7, -234, 8786.313714591880853 ], [ 7, -298, 2400.000000727046427 ], [ 7, -305, 1698.044999861659107 ], [ 7, -310, 1200.000000727047336 ], [ 7, -314, 813.686286862211773 ], [ 7, -317, 498.044999861659221 ], [ 7, -320, 231.174094257922036 ], [ 7, -323, -203.91000100372878 ], [ 7, -325, -386.313713137788284 ], [ 7, -328, -701.955000138340779 ], [ 7, -329, -840.52766104226464 ], [ 7, -331, -968.825905742078021 ], [ 7, -332, -1088.268714003174864 ], [ 7, -333, -1199.999999272952664 ], [ 7, -335, -1403.910001003728667 ], [ 7, -337, -1586.313713137788227 ], [ 7, -338, -1670.780906607466022 ], [ 7, 25, 2400.000000727046427 ], [ 7, 18, 1698.044999861659107 ], [ 7, 13, 1200.000000727047336 ], [ 7, 9, 813.686286862211773 ], [ 7, 6, 498.044999861659221 ], [ 7, 3, 231.174094257922036 ], [ 7, 1, 7.928047436680928 ], [ 7, 7, 11.892071155021393 ], [ 8, -15, 579.835995588422293 ], [ 8, -22, -2399.999994425971181 ], [ 8, -27, 2015.532808469115025 ], [ 8, -31, 1.189207115502139 ], [ 8, -34, 3.563594874889346 ], [ 8, -36, 3.3333333370663 ], [ 8, -38, 2.969662395741121 ], [ 8, -40, 2.545424910635247 ], [ 8, -42, 5.291336843844476 ], [ 8, -43, 2.227246796805841 ], [ 8, -45, 1.979774930494081 ], [ 8, -46, 1.781797437444673 ], [ 8, -48, 1.61981585222243 ], [ 8, -49, 1.570399828948281 ], [ 8, -50, 1.484831197870561 ], [ 8, -51, 1.370613413418979 ], [ 8, -52, 5.946035577510694 ], [ 8, -53, 1.187864958296448 ], [ 8, -54, 2.642682478893642 ], [ 8, -55, 4.756828462008555 ], [ 8, -2, 4484.358713726493988 ], [ 8, -4, 5.946035577510696 ], [ 8, -6, 4098.044999861659562 ], [ 8, -8, 3881.35830509147354 ], [ 8, -9, 3782.403712861106214 ], [ 8, -11, 3.964023718340464 ], [ 8, -12, 3515.532807257368859 ], [ 8, -13, 3.397734615720398 ], [ 8, -14, 708.13424028823772 ], [ 8, -16, 2.973017788755348 ], [ 8, -17, 447.616494287913724 ], [ 8, -18, 2.642682478893643 ], [ 8, -19, 2.378414231004279 ], [ 8, -1, 4634.99577222712378 ], [ 8, -3, 1779.835995588422293 ], [ 8, -5, 4217.487808122756178 ], [ 8, -7, 4.756828462008557 ], [ 8, -10, 3688.800698459579053 ], [ 8, -25, 1.698867307860199 ], [ 8, -30, 1.251796963686462 ], [ 8, -39, 2.669679709212079 ], [ 8, -41, 11.892071155021387 ], [ 8, -47, 7.928047436680925 ], [ 8, -56, 1.048116139673337 ], [ 8, -57, 0.93778812497088 ], [ 8, -58, 0.890898718722336 ], [ 8, -29, 1.399067194708399 ], [ 8, -35, 7.928047436680925 ], [ 8, -44, 2.00000000223978 ], [ 8, -59, 3.964023718340462 ], [ 8, -60, 0.833333334266575 ], [ 8, -62, 3.3333333370663 ], [ 8, -63, 0.66666666741326 ], [ 8, -64, 2.973017788755347 ], [ 8, -65, 0.625000000699931 ], [ 8, -66, 2.642682478893642 ], [ 8, -67, 2.378414231004277 ], [ 8, -68, 0.500000000559945 ], [ 8, -69, 2.162194755458434 ], [ 8, -20, 51.148885925243803 ], [ 8, -21, 2.162194755458435 ], [ 8, -23, 1.982011859170232 ], [ 8, -33, 3.813828156017255 ], [ 8, -24, 1.82954940846483 ], [ 8, -26, 1.585609487336186 ], [ 8, -28, 1.486508894377674 ], [ 8, -32, 1.132578205240133 ], [ 8, -37, 3.964023718340462 ], [ 8, -61, 3.397734615720396 ], [ 8, -70, 2.00000000223978 ], [ 8, -72, 1.829549408464829 ], [ 8, -74, 1.585609487336185 ], [ 8, -76, 1.486508894377673 ], [ 8, -78, 1.251796963686462 ], [ 8, -79, 1.189207115502139 ], [ 8, -80, 1.132578205240132 ], [ 8, -82, 1.00000000111989 ], [ 8, -83, 0.476190476723757 ], [ 8, -84, 0.909090910108991 ], [ 8, -85, 0.424716827440686 ], [ 8, -86, 0.833333334266575 ], [ 8, -87, 0.769230770092223 ], [ 8, -88, 0.714285715085636 ], [ 8, -89, 0.66666666741326 ], [ 8, -71, 1.982011859170231 ], [ 8, -73, 1.698867307860198 ], [ 8, -75, 0.769230770092223 ], [ 8, -77, 1.399067194708399 ], [ 8, -81, 1.111111112355434 ], [ 8, -90, 0.312949241272084 ], [ 8, -91, 0.625000000699931 ], [ 8, -92, 0.588235294776406 ], [ 8, -93, 0.5263157900631 ], [ 8, -94, 0.500000000559945 ], [ 8, -95, 0.476190476723757 ], [ 8, -96, 0.232525094861855 ], [ 8, -316, 11.892071155021387 ], [ 8, -322, 7.928047436680925 ], [ 8, -327, 5.946035577510694 ], [ 8, -330, 4.756828462008555 ], [ 8, -334, 3.964023718340462 ], [ 8, -336, 3.397734615720396 ], [ 8, -339, 2.973017788755347 ], [ 8, -341, 2.642682478893642 ], [ 8, -342, 2.378414231004277 ], [ 8, -344, 2.162194755458434 ], [ 8, -346, 1.982011859170231 ], [ 8, -347, 1.829549408464829 ], [ 8, -348, 1.698867307860198 ], [ 8, -349, 1.585609487336185 ], [ 8, -351, 1.486508894377673 ], [ 8, -352, 1.399067194708399 ], [ 8, -353, 1.251796963686462 ], [ 8, -354, 1.189207115502139 ], [ 8, -355, 1.132578205240132 ], [ 8, -234, 8786.313714591880853 ], [ 8, -222, 9986.313714591880853 ], [ 8, -215, 10688.268715457268627 ], [ 8, -210, 11186.313714591880853 ], [ 8, -206, 11572.627428456715279 ], [ 8, -203, 11888.268715457268627 ], [ 8, -200, 12155.139621061007347 ], [ 8, -198, 12386.313714591880853 ], [ 8, -196, 12590.223716322656401 ], [ 8, -194, 12772.627428456717098 ], [ 8, -193, 12937.631656956637016 ], [ 8, -191, 13088.268715457268627 ], [ 8, -190, 13226.841376361193397 ], [ 8, -188, 13355.139621061005528 ], [ 8, -187, 13474.582429322103053 ], [ 8, -186, 13586.313714591880853 ], [ 8, -185, 13691.269124092286802 ], [ 8, -184, 13790.223716322656401 ], [ 8, -183, 13883.826730724184017 ], [ 8, -182, 13972.627428456718917 ], [ 8, -246, 7586.313714591880853 ], [ 8, -253, 6884.358713726493079 ], [ 8, -258, 6386.313714591880853 ], [ 8, -262, 6000.000000727046427 ], [ 8, -265, 5684.358713726493079 ], [ 8, -268, 5417.487808122756178 ], [ 8, -270, 5186.313714591880853 ], [ 8, -272, 4982.403712861106214 ], [ 8, -274, 4800.000000727046427 ], [ 8, -276, 4634.99577222712378 ], [ 8, -277, 4484.358713726493988 ], [ 8, -279, 4345.786052822571037 ], [ 8, -280, 4217.487808122756178 ], [ 8, -281, 4098.044999861659562 ], [ 8, -282, 3986.313714591881308 ], [ 8, -283, 3881.35830509147354 ], [ 8, -284, 3782.403712861106214 ], [ 8, -285, 3688.800698459579053 ], [ 8, -286, 3600.000000727046427 ], [ 8, -287, 3515.532807257368859 ], [ 8, 7, 11.892071155021393 ], [ 8, 1, 7.928047436680928 ], [ 9, -3, 4413.686286862212 ], [ 9, -15, -1691.269122638195313 ], [ 9, -22, -2357.094620229951033 ], [ 9, -27, 2013.686286862211773 ], [ 9, -31, 1.189207115502139 ], [ 9, -34, 1311.731285996825136 ], [ 9, -36, -3755.139617668120991 ], [ 9, -38, -3986.313711198994497 ], [ 9, -40, -4190.223712929769135 ], [ 9, -42, -4372.627425063830742 ], [ 9, -43, 427.37257299737621 ], [ 9, -45, -2472.627426598707189 ], [ 9, -46, 111.731285996825136 ], [ 9, -48, -155.13961960691347 ], [ 9, -49, -2943.408333933220547 ], [ 9, -50, -386.313713137788284 ], [ 9, -51, -3055.139619202996982 ], [ 9, -52, 5.946035577510694 ], [ 9, -53, -3259.049620933771621 ], [ 9, -54, -772.62742700262379 ], [ 9, -55, 4.756828462008555 ], [ 9, -10, -1226.841374907098725 ], [ 9, -57, -1088.268714003174864 ], [ 9, -59, 3.964023718340462 ], [ 9, -61, 3.397734615720396 ], [ 9, -62, -1586.313713137788227 ], [ 9, -64, 2.973017788755347 ], [ 9, -65, -1883.826729270092073 ], [ 9, -67, 2.378414231004277 ], [ 9, -68, -4788.881430410471694 ], [ 9, -69, 2.162194755458434 ], [ 9, -70, -4992.791432141246332 ], [ 9, -71, 1.982011859170231 ], [ 9, -72, 1.829549408464829 ], [ 9, -73, 1.698867307860198 ], [ 9, -74, 1.585609487336185 ], [ 9, -11, 3.964023718340464 ], [ 9, -23, 1.982011859170232 ], [ 9, -30, 1.251796963686462 ], [ 9, -35, -1534.995770369116144 ], [ 9, -41, 11.892071155021387 ], [ 9, -44, -2406.457561567754055 ], [ 9, -56, -3588.881430410471239 ], [ 9, -58, -1226.841374907098725 ], [ 9, -60, -1355.139619606913584 ], [ 9, -63, -1691.269122638195313 ], [ 9, -18, 2.642682478893643 ], [ 9, -6, -772.62742700262379 ], [ 9, -66, 2.642682478893642 ], [ 9, -25, 1.698867307860199 ], [ 9, -39, 813.686286862211773 ], [ 9, -1, -272.627427406537493 ], [ 9, -13, 3.397734615720398 ], [ 9, -20, -2183.826729027741294 ], [ 9, -29, 1.399067194708399 ], [ 9, -32, 1.132578205240133 ], [ 9, -47, 7.928047436680925 ], [ 9, -4, 5.946035577510696 ], [ 9, -16, 2.973017788755348 ], [ 9, -26, 1.585609487336186 ], [ 9, -28, 1.486508894377674 ], [ 9, -33, -3488.268712064382271 ], [ 9, -37, -3874.582425929217607 ], [ 9, -7, 4.756828462008557 ], [ 9, -19, 2.378414231004279 ], [ 9, -8, -974.582428271924073 ], [ 9, -14, -1586.313713137788227 ], [ 9, -2, -386.313713137788284 ], [ 9, -5, -726.841375311013849 ], [ 9, -9, -1088.268714003174864 ], [ 9, -12, -1355.139619606913584 ], [ 9, -17, -1883.826729270092073 ], [ 9, -21, 2.162194755458435 ], [ 9, -24, 1.82954940846483 ], [ 9, -75, -5521.478541804424822 ], [ 9, -76, 1.486508894377673 ], [ 9, -77, 1.399067194708399 ], [ 9, -78, 1.251796963686462 ], [ 9, -79, 1.189207115502139 ], [ 9, -80, 1.132578205240132 ], [ 9, -81, -3472.627425790878533 ], [ 9, -82, -3557.094619260556101 ], [ 9, -83, 0.476190476723757 ], [ 9, -84, 0.909090910108991 ], [ 9, -85, 0.424716827440686 ], [ 9, -86, 0.833333334266575 ], [ 9, -87, 0.769230770092223 ], [ 9, -88, 0.714285715085636 ], [ 9, -89, 0.66666666741326 ], [ 9, -90, 0.312949241272084 ], [ 9, -91, 0.625000000699931 ], [ 9, -92, 0.588235294776406 ], [ 9, -93, 0.5263157900631 ], [ 9, -94, 0.500000000559945 ], [ 9, -95, 0.476190476723757 ], [ 9, -96, 0.232525094861855 ], [ 9, -316, 11.892071155021387 ], [ 9, -322, 7.928047436680925 ], [ 9, -327, -590.223714868562865 ], [ 9, -330, -937.631655502545414 ], [ 9, -334, 3.964023718340462 ], [ 9, -336, -1474.582427868010427 ], [ 9, -339, -1790.223714868562865 ], [ 9, -341, -1972.62742700262379 ], [ 9, -342, -2057.094620472301358 ], [ 9, -344, 2.162194755458434 ], [ 9, -346, 1.982011859170231 ], [ 9, -347, 1.829549408464829 ], [ 9, -348, 1.698867307860198 ], [ 9, -349, 1.585609487336185 ], [ 9, -351, 1.486508894377673 ], [ 9, -352, 1.399067194708399 ], [ 9, -353, 1.251796963686462 ], [ 9, -354, 1.189207115502139 ], [ 9, -355, 1.132578205240132 ], [ 9, -290, 3213.686286862211546 ], [ 9, -278, 4413.686286862212 ], [ 9, -271, 5115.641287727598865 ], [ 9, -266, 5613.686286862212 ], [ 9, -262, 6000.000000727046427 ], [ 9, -259, 6315.641287727598865 ], [ 9, -256, 6582.512193331336675 ], [ 9, -254, 6813.686286862212 ], [ 9, -252, 7017.596288592986639 ], [ 9, -250, 7200.000000727047336 ], [ 9, -248, 7365.004229226969073 ], [ 9, -247, 7515.641287727597955 ], [ 9, -245, 7654.213948631521816 ], [ 9, -244, 7782.512193331336675 ], [ 9, -243, 7901.955001592434201 ], [ 9, -242, 8013.686286862212 ], [ 9, -241, 8118.641696362618859 ], [ 9, -240, 8217.596288592985729 ], [ 9, -239, 8311.199302994513346 ], [ 9, -238, 8400.000000727046427 ], [ 9, -302, 2013.686286862211773 ], [ 9, -309, 1311.731285996825136 ], [ 9, -314, 813.686286862211773 ], [ 9, -318, 427.37257299737621 ], [ 9, -321, 111.731285996825136 ], [ 9, -323, -155.13961960691347 ], [ 9, -325, -386.313713137788284 ], [ 9, -329, -772.62742700262379 ], [ 9, -332, -1088.268714003174864 ], [ 9, -333, -1226.841374907098725 ], [ 9, -335, -1355.139619606913584 ], [ 9, -337, -1586.313713137788227 ], [ 9, -338, -1691.269122638195313 ], [ 9, -340, -1883.826729270092073 ], [ 9, 21, 2013.686286862211773 ], [ 9, 14, 1311.731285996825136 ], [ 9, 9, 813.686286862211773 ], [ 9, 5, 427.37257299737621 ], [ 9, 2, 111.731285996825136 ], [ 9, 7, 11.892071155021393 ], [ 9, 1, 7.928047436680928 ], [ 10, -25, 1.698867307860199 ], [ 10, -32, 1.132578205240133 ], [ 10, -36, 3.3333333370663 ], [ 10, -40, 2.545424910635247 ], [ 10, -43, 2.227246796805841 ], [ 10, -46, 1.781797437444673 ], [ 10, -48, 1.61981585222243 ], [ 10, -50, 1.484831197870561 ], [ 10, -52, 5.946035577510694 ], [ 10, -54, 2.642682478893642 ], [ 10, -55, 4.756828462008555 ], [ 10, -56, 1.048116139673337 ], [ 10, -58, 0.890898718722336 ], [ 10, -59, 3.964023718340462 ], [ 10, -60, 0.833333334266575 ], [ 10, -61, 3.397734615720396 ], [ 10, -62, 3.3333333370663 ], [ 10, -63, 0.66666666741326 ], [ 10, -64, 2.973017788755347 ], [ 10, -65, 0.625000000699931 ], [ 10, -1, 2023.372967831493042 ], [ 10, -2, 4533.129095123308616 ], [ 10, -3, 4413.686286862212 ], [ 10, -4, 5.946035577510696 ], [ 10, -5, 4196.999592092026433 ], [ 10, -6, 4098.044999861659562 ], [ 10, -7, 4.756828462008557 ], [ 10, -8, 3915.641287727598865 ], [ 10, -30, 1.251796963686462 ], [ 10, -41, 11.892071155021387 ], [ 10, -45, 1.979774930494081 ], [ 10, -51, 1.370613413418979 ], [ 10, -53, 1.187864958296448 ], [ 10, -57, 0.93778812497088 ], [ 10, -66, 2.642682478893642 ], [ 10, -67, 2.378414231004277 ], [ 10, -68, 0.500000000559945 ], [ 10, -69, 2.162194755458434 ], [ 10, -70, 2.00000000223978 ], [ 10, -9, 3831.174094257922206 ], [ 10, -72, 1.829549408464829 ], [ 10, -74, 1.585609487336185 ], [ 10, -76, 1.486508894377673 ], [ 10, -78, 1.251796963686462 ], [ 10, -79, 1.189207115502139 ], [ 10, -80, 1.132578205240132 ], [ 10, -82, 1.00000000111989 ], [ 10, -83, 0.476190476723757 ], [ 10, -84, 0.909090910108991 ], [ 10, -85, 0.424716827440686 ], [ 10, -86, 0.833333334266575 ], [ 10, -87, 0.769230770092223 ], [ 10, -88, 0.714285715085636 ], [ 10, -89, 0.66666666741326 ], [ 10, -11, 3.964023718340464 ], [ 10, -12, 895.477282588975527 ], [ 10, -14, 664.303189058099974 ], [ 10, -15, 559.347779557693002 ], [ 10, -16, 2.973017788755348 ], [ 10, -17, 366.790172925797719 ], [ 10, -18, 2.642682478893643 ], [ 10, -19, 2.378414231004279 ], [ 10, -20, 8.908987187223364 ], [ 10, -21, 2.162194755458435 ], [ 10, -71, 1.982011859170231 ], [ 10, -73, 1.698867307860198 ], [ 10, -75, 0.769230770092223 ], [ 10, -77, 1.399067194708399 ], [ 10, -10, 1099.387284319750052 ], [ 10, -13, 3.397734615720398 ], [ 10, -22, 2504.441986671876748 ], [ 10, -29, 1.399067194708399 ], [ 10, -34, 3.563594874889346 ], [ 10, -37, 3.964023718340462 ], [ 10, -47, 7.928047436680925 ], [ 10, -49, 1.570399828948281 ], [ 10, -35, 7.928047436680925 ], [ 10, -38, 2.969662395741121 ], [ 10, -42, 5.291336843844476 ], [ 10, -44, 2.00000000223978 ], [ 10, -26, 1.585609487336186 ], [ 10, -33, 3.813828156017255 ], [ 10, -23, 1.982011859170232 ], [ 10, -24, 1.82954940846483 ], [ 10, -27, 5.339359418424158 ], [ 10, -81, 1.111111112355434 ], [ 10, -90, 0.312949241272084 ], [ 10, -91, 0.625000000699931 ], [ 10, -92, 0.588235294776406 ], [ 10, -93, 0.5263157900631 ], [ 10, -94, 0.500000000559945 ], [ 10, -95, 0.476190476723757 ], [ 10, -28, 1.486508894377674 ], [ 10, -96, 0.232525094861855 ], [ 10, -31, 1.189207115502139 ], [ 10, -39, 2.669679709212079 ], [ 10, -316, 11.892071155021387 ], [ 10, -322, 7.928047436680925 ], [ 10, -327, 5.946035577510694 ], [ 10, -330, 4.756828462008555 ], [ 10, -334, 3.964023718340462 ], [ 10, -336, 3.397734615720396 ], [ 10, -339, 2.973017788755347 ], [ 10, -341, 2.642682478893642 ], [ 10, -342, 2.378414231004277 ], [ 10, -344, 2.162194755458434 ], [ 10, -346, 1.982011859170231 ], [ 10, -347, 1.829549408464829 ], [ 10, -348, 1.698867307860198 ], [ 10, -349, 1.585609487336185 ], [ 10, -351, 1.486508894377673 ], [ 10, -352, 1.399067194708399 ], [ 10, -353, 1.251796963686462 ], [ 10, -354, 1.189207115502139 ], [ 10, -355, 1.132578205240132 ], [ 10, -231, 9101.955001592434201 ], [ 10, -219, 10301.955001592434201 ], [ 10, -212, 11003.910002457821975 ], [ 10, -207, 11501.955001592434201 ], [ 10, -203, 11888.268715457268627 ], [ 10, -200, 12203.910002457821975 ], [ 10, -197, 12470.780908061558875 ], [ 10, -195, 12701.95500159243602 ], [ 10, -193, 12905.865003323207929 ], [ 10, -191, 13088.268715457268627 ], [ 10, -189, 13253.272943957190364 ], [ 10, -188, 13403.910002457820156 ], [ 10, -187, 13542.482663361744926 ], [ 10, -185, 13670.780908061558875 ], [ 10, -184, 13790.223716322656401 ], [ 10, -183, 13901.95500159243602 ], [ 10, -182, 14006.91041109284015 ], [ 10, -181, 14105.865003323207929 ], [ 10, -180, 14199.468017724737365 ], [ 10, -179, 14288.268715457268627 ], [ 10, -243, 7901.955001592434201 ], [ 10, -250, 7200.000000727047336 ], [ 10, -255, 6701.955001592434201 ], [ 10, -259, 6315.641287727598865 ], [ 10, -262, 6000.000000727046427 ], [ 10, -265, 5733.129095123309526 ], [ 10, -267, 5501.955001592434201 ], [ 10, -269, 5298.044999861659562 ], [ 10, -271, 5115.641287727598865 ], [ 10, -272, 4950.637059227677128 ], [ 10, -274, 4800.000000727046427 ], [ 10, -275, 4661.427339823123475 ], [ 10, -277, 4533.129095123308616 ], [ 10, -278, 4413.686286862212 ], [ 10, -279, 4301.955001592434201 ], [ 10, -280, 4196.999592092026433 ], [ 10, -281, 4098.044999861659562 ], [ 10, -282, 4004.441985460131491 ], [ 10, -283, 3915.641287727598865 ], [ 10, -284, 3831.174094257922206 ], [ 10, 7, 11.892071155021393 ], [ 10, 1, 7.928047436680928 ], [ 11, -6, 4098.044999861659562 ], [ 11, -18, 2.642682478893643 ], [ 11, -25, 1.698867307860199 ], [ 11, -30, 1.251796963686462 ], [ 11, -34, -3572.735906503456135 ], [ 11, -36, -3803.909999064934709 ], [ 11, -39, -4070.780904668672065 ], [ 11, -41, 11.892071155021387 ], [ 11, -43, -4505.864999930321574 ], [ 11, -45, -4688.26871206438409 ], [ 11, -47, 7.928047436680925 ], [ 11, -48, -203.91000100372878 ], [ 11, -49, -2872.735907068937195 ], [ 11, -51, -470.780906607465965 ], [ 11, -52, 5.946035577510694 ], [ 11, -53, -701.955000138340779 ], [ 11, -54, -3370.780906203549421 ], [ 11, -55, 4.756828462008555 ], [ 11, -56, -3574.690907934325423 ], [ 11, -57, -1088.268714003176228 ], [ 11, -58, -3841.56181353806096 ], [ 11, -32, 1.132578205240133 ], [ 11, -40, -4190.223712929769135 ], [ 11, -46, 111.731285996823715 ], [ 11, -50, -3011.308567972861056 ], [ 11, -59, 3.964023718340462 ], [ 11, -60, -1403.910001003728667 ], [ 11, -61, 3.397734615720396 ], [ 11, -62, -4180.537033899281596 ], [ 11, -63, -1670.780906607466022 ], [ 11, -64, 2.973017788755347 ], [ 11, -65, -1901.955000138340893 ], [ 11, -4, 5.946035577510696 ], [ 11, -16, 2.973017788755348 ], [ 11, -23, 1.982011859170232 ], [ 11, -28, 1.486508894377674 ], [ 11, -35, -3672.735906422672997 ], [ 11, -37, 996.08999899627122 ], [ 11, -12, -1403.910001003728667 ], [ 11, -24, 1.82954940846483 ], [ 11, -31, 1.189207115502139 ], [ 11, -42, 498.044999861659221 ], [ 11, -11, 3.964023718340464 ], [ 11, -38, 2.969662395741121 ], [ 11, -44, -4599.4680143318501 ], [ 11, -5, -701.955000138340779 ], [ 11, -17, -1901.955000138340893 ], [ 11, -29, 1.399067194708399 ], [ 11, -33, -3488.268713033779932 ], [ 11, -9, -1088.268714003176228 ], [ 11, -21, 2.162194755458435 ], [ 11, -7, 4.756828462008557 ], [ 11, -19, 2.378414231004279 ], [ 11, -26, 1.585609487336186 ], [ 11, -66, 2.642682478893642 ], [ 11, -67, 2.378414231004277 ], [ 11, -68, -2199.468016270643147 ], [ 11, -69, 2.162194755458434 ], [ 11, -15, -1670.780906607466022 ], [ 11, -3, -470.780906607465965 ], [ 11, -27, -2872.735907068937195 ], [ 11, -14, -1605.865002273030541 ], [ 11, -2, -405.865002273030484 ], [ 11, -13, 3.397734615720398 ], [ 11, -1, -270.780906769032015 ], [ 11, -70, -2372.735907472852432 ], [ 11, -10, -1170.780907011381032 ], [ 11, -22, -2372.735907472852432 ], [ 11, -8, -1042.482662311565036 ], [ 11, -20, -2199.468016270643147 ], [ 11, -72, 1.829549408464829 ], [ 11, -74, 1.585609487336185 ], [ 11, -76, 1.486508894377673 ], [ 11, -78, 1.251796963686462 ], [ 11, -79, 1.189207115502139 ], [ 11, -80, 1.132578205240132 ], [ 11, -82, -8372.735902625870949 ], [ 11, -83, -3699.468015058897436 ], [ 11, -84, -3788.268712791430971 ], [ 11, -85, -3872.735906261108539 ], [ 11, -86, 0.833333334266575 ], [ 11, -87, 0.769230770092223 ], [ 11, -88, 0.714285715085636 ], [ 11, -89, 0.66666666741326 ], [ 11, -71, 1.982011859170231 ], [ 11, -73, 1.698867307860198 ], [ 11, -75, -5539.606812672675005 ], [ 11, -77, 1.399067194708399 ], [ 11, -81, -8288.268709156192926 ], [ 11, -90, 0.312949241272084 ], [ 11, -91, 0.625000000699931 ], [ 11, -92, 0.588235294776406 ], [ 11, -93, 0.5263157900631 ], [ 11, -94, 0.500000000559945 ], [ 11, -95, 0.476190476723757 ], [ 11, -96, 0.232525094861855 ], [ 11, -316, 11.892071155021387 ], [ 11, -322, 7.928047436680925 ], [ 11, -327, 5.946035577510694 ], [ 11, -330, -905.865001869115304 ], [ 11, -334, -1253.272942503097966 ], [ 11, -336, -1542.482661907651163 ], [ 11, -339, -1790.223714868562865 ], [ 11, -341, -2006.910409638747751 ], [ 11, -342, -2105.865001869115531 ], [ 11, -344, -2288.268714003176228 ], [ 11, -346, 1.982011859170231 ], [ 11, -347, 1.829549408464829 ], [ 11, -348, 1.698867307860198 ], [ 11, -349, 1.585609487336185 ], [ 11, -351, 1.486508894377673 ], [ 11, -352, 1.399067194708399 ], [ 11, -353, 1.251796963686462 ], [ 11, -354, 1.189207115502139 ], [ 11, -355, 1.132578205240132 ], [ 11, -293, 2898.044999861659107 ], [ 11, -281, 4098.044999861659562 ], [ 11, -274, 4800.000000727046427 ], [ 11, -269, 5298.044999861659562 ], [ 11, -265, 5684.358713726493079 ], [ 11, -262, 6000.000000727046427 ], [ 11, -259, 6266.870906330784237 ], [ 11, -257, 6498.044999861659562 ], [ 11, -255, 6701.955001592434201 ], [ 11, -253, 6884.358713726493079 ], [ 11, -252, 7049.362942226414816 ], [ 11, -250, 7200.000000727047336 ], [ 11, -249, 7338.572661630969378 ], [ 11, -247, 7466.870906330784237 ], [ 11, -246, 7586.313714591880853 ], [ 11, -245, 7698.044999861659562 ], [ 11, -244, 7803.000409362066421 ], [ 11, -243, 7901.955001592434201 ], [ 11, -242, 7995.558015993961817 ], [ 11, -241, 8084.358713726493079 ], [ 11, -305, 1698.044999861659107 ], [ 11, -312, 996.08999899627122 ], [ 11, -317, 498.044999861659221 ], [ 11, -321, 111.731285996823715 ], [ 11, -323, -203.91000100372878 ], [ 11, -326, -470.780906607465965 ], [ 11, -328, -701.955000138340779 ], [ 11, -332, -1088.268714003176228 ], [ 11, -335, -1403.910001003728667 ], [ 11, -338, -1670.780906607466022 ], [ 11, -340, -1901.955000138340893 ], [ 11, -343, -2199.468016270643147 ], [ 11, -345, -2372.735907472852432 ], [ 11, 18, 1698.044999861659107 ], [ 11, 11, 996.08999899627122 ], [ 11, 6, 498.044999861659221 ], [ 11, 2, 111.731285996823715 ], [ 11, 7, 11.892071155021393 ], [ 11, 1, 7.928047436680928 ], [ 12, -25, 1.698867307860199 ], [ 12, -32, 1.132578205240133 ], [ 12, -36, 3.3333333370663 ], [ 12, -40, 2.545424910635247 ], [ 12, -43, 2.227246796805841 ], [ 12, -46, 1.781797437444673 ], [ 12, -48, 1.61981585222243 ], [ 12, -50, 1.484831197870561 ], [ 12, -52, 5.946035577510694 ], [ 12, -54, 2.642682478893642 ], [ 12, -55, 4.756828462008555 ], [ 12, -56, 1.048116139673337 ], [ 12, -58, 0.890898718722336 ], [ 12, -59, 3.964023718340462 ], [ 12, -60, 0.833333334266575 ], [ 12, -61, 3.397734615720396 ], [ 12, -62, 3.3333333370663 ], [ 12, -63, 0.66666666741326 ], [ 12, -64, 2.973017788755347 ], [ 12, -65, 0.625000000699931 ], [ 12, -1, 4568.825907196172011 ], [ 12, -2, 4463.870497695764243 ], [ 12, -3, 4364.915905465396463 ], [ 12, -4, 5.946035577510696 ], [ 12, -5, 4182.512193331336675 ], [ 12, -30, 1.251796963686462 ], [ 12, -41, 11.892071155021387 ], [ 12, -45, 1.979774930494081 ], [ 12, -51, 1.370613413418979 ], [ 12, -53, 1.187864958296448 ], [ 12, -57, 0.93778812497088 ], [ 12, -66, 2.642682478893642 ], [ 12, -67, 2.378414231004277 ], [ 12, -68, 0.500000000559945 ], [ 12, -69, 2.162194755458434 ], [ 12, -70, 2.00000000223978 ], [ 12, -6, 4098.044999861659562 ], [ 12, -72, 1.829549408464829 ], [ 12, -74, 1.585609487336185 ], [ 12, -76, 1.486508894377673 ], [ 12, -78, 1.251796963686462 ], [ 12, -79, 1.189207115502139 ], [ 12, -80, 1.132578205240132 ], [ 12, -82, 1.00000000111989 ], [ 12, -83, 0.476190476723757 ], [ 12, -84, 0.909090910108991 ], [ 12, -85, 0.424716827440686 ], [ 12, -86, 0.833333334266575 ], [ 12, -87, 0.769230770092223 ], [ 12, -88, 0.714285715085636 ], [ 12, -89, 0.66666666741326 ], [ 12, -7, 4.756828462008557 ], [ 12, -8, 1290.646432892526718 ], [ 12, -10, 1135.084096392611627 ], [ 12, -11, 3.964023718340464 ], [ 12, -12, 931.174094661837216 ], [ 12, -13, 3.397734615720398 ], [ 12, -14, 727.264092931062578 ], [ 12, -15, 633.661078529534961 ], [ 12, -16, 2.973017788755348 ], [ 12, -17, -1901.95499529135941 ], [ 12, -18, 2.642682478893643 ], [ 12, -71, 1.982011859170231 ], [ 12, -73, 1.698867307860198 ], [ 12, -75, 0.769230770092223 ], [ 12, -77, 1.399067194708399 ], [ 12, -9, 1162.348188192712769 ], [ 12, -22, 7.928047436680925 ], [ 12, -29, 1.399067194708399 ], [ 12, -34, 3.563594874889346 ], [ 12, -37, 3.964023718340462 ], [ 12, -47, 7.928047436680925 ], [ 12, -49, 1.570399828948281 ], [ 12, -20, 8.908987187223364 ], [ 12, -35, 7.928047436680925 ], [ 12, -38, 2.969662395741121 ], [ 12, -42, 5.291336843844476 ], [ 12, -44, 2.00000000223978 ], [ 12, -26, 1.585609487336186 ], [ 12, -33, 3.813828156017255 ], [ 12, -19, 2.378414231004279 ], [ 12, -21, 2.162194755458435 ], [ 12, -23, 1.982011859170232 ], [ 12, -24, 1.82954940846483 ], [ 12, -81, 1.111111112355434 ], [ 12, -90, 0.312949241272084 ], [ 12, -91, 0.625000000699931 ], [ 12, -92, 0.588235294776406 ], [ 12, -93, 0.5263157900631 ], [ 12, -94, 0.500000000559945 ], [ 12, -95, 0.476190476723757 ], [ 12, -96, 0.232525094861855 ], [ 12, -27, 5.339359418424158 ], [ 12, -28, 1.486508894377674 ], [ 12, -31, 1.189207115502139 ], [ 12, -39, 2.669679709212079 ], [ 12, -316, 11.892071155021387 ], [ 12, -322, 7.928047436680925 ], [ 12, -327, 5.946035577510694 ], [ 12, -330, 4.756828462008555 ], [ 12, -334, 3.964023718340462 ], [ 12, -336, 3.397734615720396 ], [ 12, -339, 2.973017788755347 ], [ 12, -341, 2.642682478893642 ], [ 12, -342, 2.378414231004277 ], [ 12, -344, 2.162194755458434 ], [ 12, -346, 1.982011859170231 ], [ 12, -347, 1.829549408464829 ], [ 12, -348, 1.698867307860198 ], [ 12, -349, 1.585609487336185 ], [ 12, -351, 1.486508894377673 ], [ 12, -352, 1.399067194708399 ], [ 12, -353, 1.251796963686462 ], [ 12, -354, 1.189207115502139 ], [ 12, -355, 1.132578205240132 ], [ 12, -228, 9368.825907196171102 ], [ 12, -216, 10568.825907196171102 ], [ 12, -209, 11270.780908061558875 ], [ 12, -204, 11768.825907196171102 ], [ 12, -200, 12155.139621061007347 ], [ 12, -197, 12470.780908061558875 ], [ 12, -195, 12737.651813665295776 ], [ 12, -192, 12968.825907196171102 ], [ 12, -190, 13172.735908926946649 ], [ 12, -188, 13355.139621061005528 ], [ 12, -187, 13520.143849560929084 ], [ 12, -185, 13670.780908061558875 ], [ 12, -184, 13809.353568965480008 ], [ 12, -183, 13937.651813665297595 ], [ 12, -181, 14057.094621926393302 ], [ 12, -180, 14168.825907196171102 ], [ 12, -179, 14273.78131669657887 ], [ 12, -178, 14372.735908926946649 ], [ 12, -177, 14466.338923328472447 ], [ 12, -176, 14555.139621061005528 ], [ 12, -240, 8168.825907196171102 ], [ 12, -247, 7466.870906330784237 ], [ 12, -252, 6968.825907196171102 ], [ 12, -256, 6582.512193331336675 ], [ 12, -259, 6266.870906330784237 ], [ 12, -262, 6000.000000727046427 ], [ 12, -264, 5768.825907196172011 ], [ 12, -266, 5564.915905465396463 ], [ 12, -268, 5382.512193331336675 ], [ 12, -270, 5217.507964831414938 ], [ 12, -271, 5066.870906330783328 ], [ 12, -273, 4928.298245426861286 ], [ 12, -274, 4800.000000727046427 ], [ 12, -275, 4680.557192465948901 ], [ 12, -276, 4568.825907196172011 ], [ 12, -277, 4463.870497695764243 ], [ 12, -278, 4364.915905465396463 ], [ 12, -279, 4271.312891063868847 ], [ 12, -280, 4182.512193331336675 ], [ 12, -281, 4098.044999861659562 ], [ 12, 7, 11.892071155021393 ], [ 12, 1, 7.928047436680928 ], [ 13, -9, 3831.174094257921297 ], [ 13, -21, 2.162194755458435 ], [ 13, -28, 1.486508894377674 ], [ 13, -33, 1431.174094257921979 ], [ 13, -36, -3839.606812107194855 ], [ 13, -39, -4070.780904668672065 ], [ 13, -41, 11.892071155021387 ], [ 13, -44, -4568.825903803283836 ], [ 13, -46, -4772.735905534060294 ], [ 13, -48, -155.13961960691347 ], [ 13, -49, -2908.43271914179968 ], [ 13, -51, -470.780906607465965 ], [ 13, -52, 5.946035577510694 ], [ 13, -53, -737.651812211203151 ], [ 13, -55, 4.756828462008555 ], [ 13, -56, -968.825905742078021 ], [ 13, -57, -3742.607221307693635 ], [ 13, -58, -1172.735907472852432 ], [ 13, -59, 3.964023718340462 ], [ 13, -60, -1355.139619606913584 ], [ 13, -25, 1.698867307860199 ], [ 13, -32, 1.132578205240133 ], [ 13, -40, 729.219093392533978 ], [ 13, -43, -4457.094618533508765 ], [ 13, -50, -2988.969754172045214 ], [ 13, -54, -3406.477718276411906 ], [ 13, -61, 3.397734615720396 ], [ 13, -62, -4171.393623014762852 ], [ 13, -63, -1670.780906607466022 ], [ 13, -64, 2.973017788755347 ], [ 13, -65, -1937.651812211203151 ], [ 13, -7, 4.756828462008557 ], [ 13, -19, 2.378414231004279 ], [ 13, -26, 1.585609487336186 ], [ 13, -31, 1.189207115502139 ], [ 13, -35, -3666.338920904985571 ], [ 13, -37, -3939.606812026411717 ], [ 13, -42, -2206.47771827641327 ], [ 13, -47, 7.928047436680925 ], [ 13, -15, -1670.780906607466022 ], [ 13, -3, -470.780906607465965 ], [ 13, -27, -2939.606812834239463 ], [ 13, -34, -3639.606812268761132 ], [ 13, -38, 2.969662395741121 ], [ 13, -45, 231.174094257922036 ], [ 13, -66, 2.642682478893642 ], [ 13, -14, -1557.094620876215231 ], [ 13, -2, 4533.129095123308616 ], [ 13, -8, -968.825905742078021 ], [ 13, -20, -2168.825905742077794 ], [ 13, -12, -1355.139619606913584 ], [ 13, -24, 1.82954940846483 ], [ 13, -10, -1172.735907472852432 ], [ 13, -22, -2372.735907472852432 ], [ 13, -29, 1.399067194708399 ], [ 13, -1, -270.780906769030594 ], [ 13, -67, 2.378414231004277 ], [ 13, -68, -2168.825905742077794 ], [ 13, -69, 2.162194755458434 ], [ 13, -70, -2372.735907472852432 ], [ 13, -71, 1.982011859170231 ], [ 13, -18, 2.642682478893643 ], [ 13, -6, -768.82590590364407 ], [ 13, -30, 1.251796963686462 ], [ 13, -17, -1937.651812211203151 ], [ 13, -5, -737.651812211203151 ], [ 13, -16, 2.973017788755348 ], [ 13, -4, 5.946035577510696 ], [ 13, -72, 1.829549408464829 ], [ 13, -13, 3.397734615720398 ], [ 13, -11, 3.964023718340464 ], [ 13, -23, 1.982011859170232 ], [ 13, -74, 1.585609487336185 ], [ 13, -76, 1.486508894377673 ], [ 13, -78, 1.251796963686462 ], [ 13, -79, 1.189207115502139 ], [ 13, -80, 1.132578205240132 ], [ 13, -82, -6192.791432141249061 ], [ 13, -83, -6277.258625610923445 ], [ 13, -84, -8639.606808229606031 ], [ 13, -85, -3872.735906261108539 ], [ 13, -86, -3966.338920662636156 ], [ 13, -87, -4139.60681186484544 ], [ 13, -88, 0.714285715085636 ], [ 13, -89, 0.66666666741326 ], [ 13, -73, 1.698867307860198 ], [ 13, -75, -5490.836431275860377 ], [ 13, -77, 1.399067194708399 ], [ 13, -81, -6103.990734408714161 ], [ 13, -90, 0.312949241272084 ], [ 13, -91, 0.625000000699931 ], [ 13, -92, 0.588235294776406 ], [ 13, -93, 0.5263157900631 ], [ 13, -94, 0.500000000559945 ], [ 13, -95, 0.476190476723757 ], [ 13, -96, 0.232525094861855 ], [ 13, -316, 11.892071155021387 ], [ 13, -322, 7.928047436680925 ], [ 13, -327, 5.946035577510694 ], [ 13, -330, 4.756828462008555 ], [ 13, -334, 3.964023718340462 ], [ 13, -336, -1520.143848106835094 ], [ 13, -339, -1809.353567511389883 ], [ 13, -341, 2.642682478893642 ], [ 13, -342, -2057.094620472301358 ], [ 13, -344, -2273.781315242485107 ], [ 13, -346, -2466.338921874381413 ], [ 13, -347, -2639.606813076591152 ], [ 13, -348, 1.698867307860198 ], [ 13, -349, 1.585609487336185 ], [ 13, -351, 1.486508894377673 ], [ 13, -352, 1.399067194708399 ], [ 13, -353, 1.251796963686462 ], [ 13, -354, 1.189207115502139 ], [ 13, -355, 1.132578205240132 ], [ 13, -296, 2631.174094257921297 ], [ 13, -284, 3831.174094257921297 ], [ 13, -277, 4533.129095123308616 ], [ 13, -272, 5031.174094257921752 ], [ 13, -268, 5417.487808122756178 ], [ 13, -265, 5733.129095123309526 ], [ 13, -262, 6000.000000727045517 ], [ 13, -260, 6231.174094257921752 ], [ 13, -258, 6435.08409598869639 ], [ 13, -256, 6617.487808122756178 ], [ 13, -254, 6782.492036622677915 ], [ 13, -253, 6933.129095123308616 ], [ 13, -251, 7071.701756027232477 ], [ 13, -250, 7200.000000727045517 ], [ 13, -249, 7319.442808988143952 ], [ 13, -248, 7431.174094257920842 ], [ 13, -247, 7536.12950375832861 ], [ 13, -246, 7635.08409598869639 ], [ 13, -245, 7728.687110390224007 ], [ 13, -244, 7817.487808122756178 ], [ 13, -308, 1431.174094257921979 ], [ 13, -315, 729.219093392533978 ], [ 13, -320, 231.174094257922036 ], [ 13, -323, -155.13961960691347 ], [ 13, -326, -470.780906607465965 ], [ 13, -328, -737.651812211203151 ], [ 13, -331, -968.825905742078021 ], [ 13, -333, -1172.735907472852432 ], [ 13, -335, -1355.139619606913584 ], [ 13, -338, -1670.780906607466022 ], [ 13, -340, -1937.651812211203151 ], [ 13, -343, -2168.825905742077794 ], [ 13, -345, -2372.735907472852432 ], [ 13, 15, 1431.174094257921979 ], [ 13, 8, 729.219093392533978 ], [ 13, 3, 231.174094257922036 ], [ 13, 7, 11.892071155021393 ], [ 13, 1, 7.928047436680928 ], [ 14, -25, 1.698867307860199 ], [ 14, -32, 1.132578205240133 ], [ 14, -36, 3.3333333370663 ], [ 14, -40, 2.545424910635247 ], [ 14, -43, 2.227246796805841 ], [ 14, -46, 1.781797437444673 ], [ 14, -48, 1.61981585222243 ], [ 14, -50, 1.484831197870561 ], [ 14, -52, 5.946035577510694 ], [ 14, -54, 2.642682478893642 ], [ 14, -55, 4.756828462008555 ], [ 14, -56, 1.048116139673337 ], [ 14, -58, 0.890898718722336 ], [ 14, -59, 3.964023718340462 ], [ 14, -60, 0.833333334266575 ], [ 14, -61, 3.397734615720396 ], [ 14, -62, 3.3333333370663 ], [ 14, -63, 0.66666666741326 ], [ 14, -64, 2.973017788755347 ], [ 14, -65, 0.625000000699931 ], [ 14, -1, 4596.089998996270879 ], [ 14, -2, 4502.486984594743262 ], [ 14, -3, 4413.686286862212 ], [ 14, -30, 1.251796963686462 ], [ 14, -41, 11.892071155021387 ], [ 14, -45, 1.979774930494081 ], [ 14, -51, 1.370613413418979 ], [ 14, -53, 1.187864958296448 ], [ 14, -57, 0.93778812497088 ], [ 14, -66, 2.642682478893642 ], [ 14, -67, 2.378414231004277 ], [ 14, -68, 0.500000000559945 ], [ 14, -69, 2.162194755458434 ], [ 14, -70, 2.00000000223978 ], [ 14, -4, 5.946035577510696 ], [ 14, -72, 1.829549408464829 ], [ 14, -74, 1.585609487336185 ], [ 14, -76, 1.486508894377673 ], [ 14, -78, 1.251796963686462 ], [ 14, -79, 1.189207115502139 ], [ 14, -80, 1.132578205240132 ], [ 14, -82, 1.00000000111989 ], [ 14, -83, 0.476190476723757 ], [ 14, -84, 0.909090910108991 ], [ 14, -85, 0.424716827440686 ], [ 14, -86, 0.833333334266575 ], [ 14, -87, 0.769230770092223 ], [ 14, -88, 0.714285715085636 ], [ 14, -89, 0.66666666741326 ], [ 14, -5, 1597.432283454362732 ], [ 14, -6, 1521.820526423402953 ], [ 14, -7, 4.756828462008557 ], [ 14, -9, 1162.348188192712769 ], [ 14, -10, 1057.392778692305683 ], [ 14, -11, 3.964023718340464 ], [ 14, -12, 864.8351720604104 ], [ 14, -13, 3.397734615720398 ], [ 14, -14, 691.567280858200888 ], [ 14, -15, -1670.780901760483175 ], [ 14, -16, 2.973017788755348 ], [ 14, -71, 1.982011859170231 ], [ 14, -73, 1.698867307860198 ], [ 14, -75, 0.769230770092223 ], [ 14, -77, 1.399067194708399 ], [ 14, -8, 1274.079473462489887 ], [ 14, -22, 7.928047436680925 ], [ 14, -29, 1.399067194708399 ], [ 14, -34, 3.563594874889346 ], [ 14, -37, 3.964023718340462 ], [ 14, -47, 7.928047436680925 ], [ 14, -49, 1.570399828948281 ], [ 14, -20, 8.908987187223364 ], [ 14, -35, 7.928047436680925 ], [ 14, -38, 2.969662395741121 ], [ 14, -42, 5.291336843844476 ], [ 14, -44, 2.00000000223978 ], [ 14, -26, 1.585609487336186 ], [ 14, -33, 3.813828156017255 ], [ 14, -17, 3002.486985806488974 ], [ 14, -18, 2.642682478893643 ], [ 14, -19, 2.378414231004279 ], [ 14, -21, 2.162194755458435 ], [ 14, -81, 1.111111112355434 ], [ 14, -90, 0.312949241272084 ], [ 14, -91, 0.625000000699931 ], [ 14, -92, 0.588235294776406 ], [ 14, -93, 0.5263157900631 ], [ 14, -94, 0.500000000559945 ], [ 14, -95, 0.476190476723757 ], [ 14, -23, 1.982011859170232 ], [ 14, -96, 0.232525094861855 ], [ 14, -24, 1.82954940846483 ], [ 14, -27, 5.339359418424158 ], [ 14, -28, 1.486508894377674 ], [ 14, -31, 1.189207115502139 ], [ 14, -39, 2.669679709212079 ], [ 14, -316, 11.892071155021387 ], [ 14, -322, 7.928047436680925 ], [ 14, -327, 5.946035577510694 ], [ 14, -330, 4.756828462008555 ], [ 14, -334, 3.964023718340462 ], [ 14, -336, 3.397734615720396 ], [ 14, -339, 2.973017788755347 ], [ 14, -341, 2.642682478893642 ], [ 14, -342, 2.378414231004277 ], [ 14, -344, 2.162194755458434 ], [ 14, -346, 1.982011859170231 ], [ 14, -347, 1.829549408464829 ], [ 14, -348, 1.698867307860198 ], [ 14, -349, 1.585609487336185 ], [ 14, -351, 1.486508894377673 ], [ 14, -352, 1.399067194708399 ], [ 14, -353, 1.251796963686462 ], [ 14, -354, 1.189207115502139 ], [ 14, -355, 1.132578205240132 ], [ 14, -226, 9600.000000727046427 ], [ 14, -214, 10800.000000727046427 ], [ 14, -207, 11501.955001592434201 ], [ 14, -202, 12000.000000727046427 ], [ 14, -198, 12386.313714591880853 ], [ 14, -195, 12701.95500159243602 ], [ 14, -192, 12968.825907196171102 ], [ 14, -190, 13200.000000727046427 ], [ 14, -188, 13403.910002457820156 ], [ 14, -186, 13586.313714591880853 ], [ 14, -184, 13751.31794309180259 ], [ 14, -183, 13901.95500159243602 ], [ 14, -182, 14040.527662496355333 ], [ 14, -180, 14168.825907196171102 ], [ 14, -179, 14288.268715457268627 ], [ 14, -178, 14400.000000727046427 ], [ 14, -177, 14504.955410227456014 ], [ 14, -176, 14603.910002457820156 ], [ 14, -175, 14697.513016859349591 ], [ 14, -174, 14786.313714591880853 ], [ 14, -238, 8400.000000727046427 ], [ 14, -245, 7698.044999861659562 ], [ 14, -250, 7200.000000727047336 ], [ 14, -254, 6813.686286862212 ], [ 14, -257, 6498.044999861659562 ], [ 14, -260, 6231.174094257921752 ], [ 14, -262, 6000.000000727046427 ], [ 14, -264, 5796.089998996270879 ], [ 14, -266, 5613.686286862212 ], [ 14, -268, 5448.682058362290263 ], [ 14, -269, 5298.044999861659562 ], [ 14, -270, 5159.472338957735701 ], [ 14, -272, 5031.174094257921752 ], [ 14, -273, 4911.731285996824226 ], [ 14, -274, 4800.000000727046427 ], [ 14, -275, 4695.044591226638659 ], [ 14, -276, 4596.089998996270879 ], [ 14, -277, 4502.486984594743262 ], [ 14, -278, 4413.686286862212 ], [ 14, -279, 4329.219093392533978 ], [ 14, 7, 11.892071155021393 ], [ 14, 1, 7.928047436680928 ], [ 15, -11, 3.964023718340464 ], [ 15, -23, 1.982011859170232 ], [ 15, -30, 1.251796963686462 ], [ 15, -35, 1200.000000727047336 ], [ 15, -38, -3986.313712168390339 ], [ 15, -41, 11.892071155021387 ], [ 15, -44, -4568.825903803283836 ], [ 15, -46, -4799.999997334160071 ], [ 15, -48, -5003.909999064934709 ], [ 15, -50, -386.313713137788284 ], [ 15, -52, 5.946035577510694 ], [ 15, -53, -701.955000138340779 ], [ 15, -54, -3370.780906203549421 ], [ 15, -56, -968.825905742078021 ], [ 15, -57, -3664.915903607388373 ], [ 15, -58, -1199.999999272952664 ], [ 15, -59, 3.964023718340462 ], [ 15, -60, -1403.910001003728667 ], [ 15, -61, 3.397734615720396 ], [ 15, -62, -1586.313713137788227 ], [ 15, -63, -4339.606812672675005 ], [ 15, -25, 1.698867307860199 ], [ 15, -32, 1.132578205240133 ], [ 15, -36, -3786.31371232995798 ], [ 15, -40, -4170.780905557287042 ], [ 15, -43, 2.227246796805841 ], [ 15, -55, 4.756828462008555 ], [ 15, -64, 2.973017788755347 ], [ 15, -65, -1901.955000138340893 ], [ 15, -9, -1086.313713541703464 ], [ 15, -21, 2.162194755458435 ], [ 15, -28, 1.486508894377674 ], [ 15, -33, -3486.31371257230694 ], [ 15, -39, -4070.780905638068361 ], [ 15, -42, 498.044999861659221 ], [ 15, -51, -5270.78090466867161 ], [ 15, -17, -1901.955000138340893 ], [ 15, -5, -701.955000138340779 ], [ 15, -29, 1.399067194708399 ], [ 15, -47, 7.928047436680925 ], [ 15, -66, 2.642682478893642 ], [ 15, -67, 2.378414231004277 ], [ 15, -68, -2168.825905742077794 ], [ 15, -69, 2.162194755458434 ], [ 15, -16, 2.973017788755348 ], [ 15, -4, 5.946035577510696 ], [ 15, -49, -5097.513013466462326 ], [ 15, -10, -1199.999999272952664 ], [ 15, -22, -2399.999999272952664 ], [ 15, -34, -3570.780906041984963 ], [ 15, -37, -3870.780905799635548 ], [ 15, -45, -4688.26871206438409 ], [ 15, -14, -1586.313713137788227 ], [ 15, -2, -386.313713137788284 ], [ 15, -26, 1.585609487336186 ], [ 15, -12, -1403.910001003728667 ], [ 15, -24, 1.82954940846483 ], [ 15, -31, 1.189207115502139 ], [ 15, -3, -468.825906145993088 ], [ 15, -70, -2399.999999272952664 ], [ 15, -71, 1.982011859170231 ], [ 15, -72, 1.829549408464829 ], [ 15, -73, 1.698867307860198 ], [ 15, -74, 1.585609487336185 ], [ 15, -20, -2168.825905742077794 ], [ 15, -8, -968.825905742078021 ], [ 15, -1, -268.825906307559137 ], [ 15, -19, 2.378414231004279 ], [ 15, -7, 4.756828462008557 ], [ 15, -18, 2.642682478893643 ], [ 15, -6, -768.82590590364407 ], [ 15, -75, -2870.780906607467386 ], [ 15, -15, -1668.825906145993031 ], [ 15, -27, -2870.780906607467386 ], [ 15, -13, 3.397734615720398 ], [ 15, -76, 1.486508894377673 ], [ 15, -78, 1.251796963686462 ], [ 15, -79, 1.189207115502139 ], [ 15, -80, 1.132578205240132 ], [ 15, -82, -6241.56181353806096 ], [ 15, -83, -6335.164827939590396 ], [ 15, -84, -6423.965525672121657 ], [ 15, -85, -6508.43271914179968 ], [ 15, -86, -8786.313708290805153 ], [ 15, -87, -8870.780901760483175 ], [ 15, -88, -4197.513014193509662 ], [ 15, -89, -4286.313711926044562 ], [ 15, -77, 1.399067194708399 ], [ 15, -81, -6142.60722130769318 ], [ 15, -90, -4370.780905395721675 ], [ 15, -91, 0.625000000699931 ], [ 15, -92, 0.588235294776406 ], [ 15, -93, 0.5263157900631 ], [ 15, -94, 0.500000000559945 ], [ 15, -95, 0.476190476723757 ], [ 15, -96, 0.232525094861855 ], [ 15, -316, 11.892071155021387 ], [ 15, -322, 0.000000727047222 ], [ 15, -327, 5.946035577510694 ], [ 15, -330, 4.756828462008555 ], [ 15, -334, 3.964023718340462 ], [ 15, -336, 3.397734615720396 ], [ 15, -339, -1751.317941637711328 ], [ 15, -341, -2040.527661042264754 ], [ 15, -342, 2.378414231004277 ], [ 15, -344, -2288.268714003174864 ], [ 15, -346, -2504.955408773361341 ], [ 15, -347, -2603.910001003728667 ], [ 15, -348, -2697.513015405256283 ], [ 15, -349, -2786.313713137789819 ], [ 15, -351, 1.486508894377673 ], [ 15, -352, 1.399067194708399 ], [ 15, -353, 1.251796963686462 ], [ 15, -354, 1.189207115502139 ], [ 15, -355, 1.132578205240132 ], [ 15, -298, 2400.000000727046427 ], [ 15, -286, 3600.000000727046427 ], [ 15, -279, 4301.955001592434201 ], [ 15, -274, 4800.000000727046427 ], [ 15, -270, 5186.313714591880853 ], [ 15, -267, 5501.955001592434201 ], [ 15, -264, 5768.825907196172011 ], [ 15, -262, 6000.000000727046427 ], [ 15, -260, 6203.910002457821975 ], [ 15, -258, 6386.313714591880853 ], [ 15, -256, 6551.31794309180259 ], [ 15, -255, 6701.955001592434201 ], [ 15, -254, 6840.527662496358062 ], [ 15, -252, 6968.825907196171102 ], [ 15, -251, 7088.268715457267717 ], [ 15, -250, 7200.000000727047336 ], [ 15, -249, 7304.955410227454195 ], [ 15, -248, 7403.910002457821975 ], [ 15, -247, 7497.513016859349591 ], [ 15, -246, 7586.313714591880853 ], [ 15, -310, 1200.000000727047336 ], [ 15, -317, 498.044999861659221 ], [ 15, -325, -386.313713137788284 ], [ 15, -328, -701.955000138340779 ], [ 15, -331, -968.825905742078021 ], [ 15, -333, -1199.999999272952664 ], [ 15, -335, -1403.910001003728667 ], [ 15, -337, -1586.313713137788227 ], [ 15, -340, -1901.955000138340893 ], [ 15, -343, -2168.825905742077794 ], [ 15, -345, -2399.999999272952664 ], [ 15, -350, -2870.780906607467386 ], [ 15, 25, 2400.000000727046427 ], [ 15, 13, 1200.000000727047336 ], [ 15, 6, 498.044999861659221 ], [ 15, 1, 7.928047436680928 ], [ 15, 7, 11.892071155021393 ], [ 16, -25, 1.698867307860199 ], [ 16, -32, 1.132578205240133 ], [ 16, -36, 3.3333333370663 ], [ 16, -40, 2.545424910635247 ], [ 16, -43, 2.227246796805841 ], [ 16, -46, 1.781797437444673 ], [ 16, -48, 1.61981585222243 ], [ 16, -50, 1.484831197870561 ], [ 16, -52, 5.946035577510694 ], [ 16, -54, 2.642682478893642 ], [ 16, -55, 4.756828462008555 ], [ 16, -56, 1.048116139673337 ], [ 16, -58, 0.890898718722336 ], [ 16, -59, 3.964023718340462 ], [ 16, -60, 0.833333334266575 ], [ 16, -61, 3.397734615720396 ], [ 16, -62, 3.3333333370663 ], [ 16, -63, 0.66666666741326 ], [ 16, -64, 2.973017788755347 ], [ 16, -65, 0.625000000699931 ], [ 16, -1, 4617.596288592986639 ], [ 16, -30, 1.251796963686462 ], [ 16, -41, 11.892071155021387 ], [ 16, -45, 1.979774930494081 ], [ 16, -51, 1.370613413418979 ], [ 16, -53, 1.187864958296448 ], [ 16, -57, 0.93778812497088 ], [ 16, -66, 2.642682478893642 ], [ 16, -67, 2.378414231004277 ], [ 16, -68, 0.500000000559945 ], [ 16, -69, 2.162194755458434 ], [ 16, -70, 2.00000000223978 ], [ 16, -2, 4533.129095123308616 ], [ 16, -72, 1.829549408464829 ], [ 16, -74, 1.585609487336185 ], [ 16, -76, 1.486508894377673 ], [ 16, -78, 1.251796963686462 ], [ 16, -79, 1.189207115502139 ], [ 16, -80, 1.132578205240132 ], [ 16, -82, 1.00000000111989 ], [ 16, -83, 0.476190476723757 ], [ 16, -84, 0.909090910108991 ], [ 16, -85, 0.424716827440686 ], [ 16, -86, 0.833333334266575 ], [ 16, -87, 0.769230770092223 ], [ 16, -88, 0.714285715085636 ], [ 16, -89, 0.66666666741326 ], [ 16, -4, 5.946035577510696 ], [ 16, -5, 1597.432283454362732 ], [ 16, -7, 4.756828462008557 ], [ 16, -8, 1261.302780423080321 ], [ 16, -9, 1162.348188192712769 ], [ 16, -10, 1068.745173791184925 ], [ 16, -11, 3.964023718340464 ], [ 16, -12, 895.477282588975527 ], [ 16, -13, 3.397734615720398 ], [ 16, -14, 3300.000001938791684 ], [ 16, -71, 1.982011859170231 ], [ 16, -73, 1.698867307860198 ], [ 16, -75, 0.769230770092223 ], [ 16, -77, 1.399067194708399 ], [ 16, -3, 1801.342285185137371 ], [ 16, -6, 1477.989475193264525 ], [ 16, -22, 7.928047436680925 ], [ 16, -29, 1.399067194708399 ], [ 16, -34, 3.563594874889346 ], [ 16, -37, 3.964023718340462 ], [ 16, -47, 7.928047436680925 ], [ 16, -49, 1.570399828948281 ], [ 16, -15, 3206.396987537263612 ], [ 16, -17, 3033.129096335053873 ], [ 16, -19, 2.378414231004279 ], [ 16, -20, 8.908987187223364 ], [ 16, -21, 2.162194755458435 ], [ 16, -23, 1.982011859170232 ], [ 16, -24, 1.82954940846483 ], [ 16, -26, 1.585609487336186 ], [ 16, -27, 5.339359418424158 ], [ 16, -28, 1.486508894377674 ], [ 16, -35, 7.928047436680925 ], [ 16, -38, 2.969662395741121 ], [ 16, -42, 5.291336843844476 ], [ 16, -44, 2.00000000223978 ], [ 16, -33, 3.813828156017255 ], [ 16, -16, 2.973017788755348 ], [ 16, -18, 2.642682478893643 ], [ 16, -81, 1.111111112355434 ], [ 16, -90, 0.312949241272084 ], [ 16, -91, 0.625000000699931 ], [ 16, -92, 0.588235294776406 ], [ 16, -93, 0.5263157900631 ], [ 16, -94, 0.500000000559945 ], [ 16, -95, 0.476190476723757 ], [ 16, -96, 0.232525094861855 ], [ 16, -31, 1.189207115502139 ], [ 16, -39, 2.669679709212079 ], [ 16, -316, 11.892071155021387 ], [ 16, -322, 7.928047436680925 ], [ 16, -327, 5.946035577510694 ], [ 16, -330, 4.756828462008555 ], [ 16, -334, 3.964023718340462 ], [ 16, -336, 3.397734615720396 ], [ 16, -339, 2.973017788755347 ], [ 16, -341, 2.642682478893642 ], [ 16, -342, 2.378414231004277 ], [ 16, -344, 2.162194755458434 ], [ 16, -346, 1.982011859170231 ], [ 16, -347, 1.829549408464829 ], [ 16, -348, 1.698867307860198 ], [ 16, -349, 1.585609487336185 ], [ 16, -351, 1.486508894377673 ], [ 16, -352, 1.399067194708399 ], [ 16, -353, 1.251796963686462 ], [ 16, -354, 1.189207115502139 ], [ 16, -355, 1.132578205240132 ], [ 16, -224, 9803.910002457821975 ], [ 16, -212, 11003.910002457821975 ], [ 16, -205, 11705.865003323207929 ], [ 16, -200, 12203.910002457821975 ], [ 16, -196, 12590.223716322656401 ], [ 16, -193, 12905.865003323207929 ], [ 16, -190, 13172.735908926946649 ], [ 16, -188, 13403.910002457820156 ], [ 16, -186, 13607.820004188595703 ], [ 16, -184, 13790.223716322656401 ], [ 16, -182, 13955.227944822578138 ], [ 16, -181, 14105.865003323207929 ], [ 16, -180, 14244.4376642271327 ], [ 16, -178, 14372.735908926946649 ], [ 16, -177, 14492.178717188045994 ], [ 16, -176, 14603.910002457820156 ], [ 16, -175, 14708.865411958227924 ], [ 16, -174, 14807.820004188595703 ], [ 16, -173, 14901.42301859012332 ], [ 16, -172, 14990.223716322656401 ], [ 16, -236, 8603.910002457821975 ], [ 16, -243, 7901.955001592434201 ], [ 16, -248, 7403.910002457821975 ], [ 16, -252, 7017.596288592986639 ], [ 16, -255, 6701.955001592434201 ], [ 16, -258, 6435.08409598869639 ], [ 16, -260, 6203.910002457821975 ], [ 16, -262, 6000.000000727046427 ], [ 16, -264, 5817.596288592986639 ], [ 16, -265, 5652.592060093064902 ], [ 16, -267, 5501.955001592434201 ], [ 16, -268, 5363.382340688511249 ], [ 16, -270, 5235.08409598869639 ], [ 16, -271, 5115.641287727598865 ], [ 16, -272, 5003.910002457821975 ], [ 16, -273, 4898.954592957414206 ], [ 16, -274, 4800.000000727046427 ], [ 16, -275, 4706.39698632551881 ], [ 16, -276, 4617.596288592986639 ], [ 16, -277, 4533.129095123308616 ], [ 16, 7, 11.892071155021393 ], [ 16, 1, 7.928047436680928 ], [ 17, -13, 3.397734615720398 ], [ 17, -1, 4596.089998996270879 ], [ 17, -25, 1.698867307860199 ], [ 17, -32, 1.132578205240133 ], [ 17, -36, -3774.690907772759601 ], [ 17, -40, -4190.223713899166796 ], [ 17, -43, -4505.864999930321574 ], [ 17, -46, -4772.735905534060294 ], [ 17, -48, -203.91000100372878 ], [ 17, -50, -5207.820000795709348 ], [ 17, -52, 5.946035577510694 ], [ 17, -54, -3424.053849433696087 ], [ 17, -55, 4.756828462008555 ], [ 17, -56, -3574.690907934325423 ], [ 17, -58, -1172.735907472852432 ], [ 17, -59, 3.964023718340462 ], [ 17, -60, -1403.910001003728667 ], [ 17, -61, 3.397734615720396 ], [ 17, -62, -1607.820002734503305 ], [ 17, -63, -4276.645908799711833 ], [ 17, -64, 2.973017788755347 ], [ 17, -65, -4543.516814403448734 ], [ 17, -11, 3.964023718340464 ], [ 17, -23, 1.982011859170232 ], [ 17, -30, 1.251796963686462 ], [ 17, -35, -3690.223714303081579 ], [ 17, -38, -3990.223714060732618 ], [ 17, -41, 11.892071155021387 ], [ 17, -44, 294.13499813088464 ], [ 17, -53, -5474.690906399448068 ], [ 17, -57, -3713.263568838249284 ], [ 17, -19, 2.378414231004279 ], [ 17, -7, 4.756828462008557 ], [ 17, -31, 1.189207115502139 ], [ 17, -37, 996.08999899627122 ], [ 17, -42, -4374.690907288060771 ], [ 17, -49, -5108.865408565341568 ], [ 17, -66, 2.642682478893642 ], [ 17, -67, 2.378414231004277 ], [ 17, -68, -2244.437662773039392 ], [ 17, -69, 2.162194755458434 ], [ 17, -70, -2372.735907472852432 ], [ 17, -71, 1.982011859170231 ], [ 17, -18, 2.642682478893643 ], [ 17, -6, 4098.044999861659562 ], [ 17, -45, 1.979774930494081 ], [ 17, -51, -5301.423015197236964 ], [ 17, -12, -1403.910001003728667 ], [ 17, -24, 1.82954940846483 ], [ 17, -39, -4074.690907530410186 ], [ 17, -47, 7.928047436680925 ], [ 17, -16, 2.973017788755348 ], [ 17, -4, 5.946035577510696 ], [ 17, -28, 1.486508894377674 ], [ 17, -14, -1607.820002734503305 ], [ 17, -2, -405.865002273030484 ], [ 17, -26, 1.585609487336186 ], [ 17, -33, -3490.223714464647855 ], [ 17, -5, -672.735907876767669 ], [ 17, -72, 1.829549408464829 ], [ 17, -73, 1.698867307860198 ], [ 17, -74, 1.585609487336185 ], [ 17, -75, -2901.423017136030921 ], [ 17, -76, 1.486508894377673 ], [ 17, -22, -2372.735907472852432 ], [ 17, -10, -1172.735907472852432 ], [ 17, -3, -472.735908038333719 ], [ 17, -34, -3574.690907934325423 ], [ 17, -21, 2.162194755458435 ], [ 17, -9, -1107.820003138417178 ], [ 17, -20, -2244.437662773039392 ], [ 17, -8, -972.735907634418595 ], [ 17, -77, 1.399067194708399 ], [ 17, -17, -1872.735907876767669 ], [ 17, -29, 1.399067194708399 ], [ 17, -15, -1744.437663176954402 ], [ 17, -27, -2901.423017136030921 ], [ 17, -78, 1.251796963686462 ], [ 17, -79, 1.189207115502139 ], [ 17, -80, 1.132578205240132 ], [ 17, -82, -6241.56181353806096 ], [ 17, -83, -6346.517223038469638 ], [ 17, -84, -6445.471815268837418 ], [ 17, -85, -6539.074829670363215 ], [ 17, -86, -6627.875527402895386 ], [ 17, -87, -6712.342720872573409 ], [ 17, -88, -8990.2237100215807 ], [ 17, -89, -9074.690903491256904 ], [ 17, -81, -6129.83052826828316 ], [ 17, -90, -4401.42301592428521 ], [ 17, -91, -4490.223713656817381 ], [ 17, -92, -4574.690907126495404 ], [ 17, -93, 0.5263157900631 ], [ 17, -94, 0.500000000559945 ], [ 17, -95, 0.476190476723757 ], [ 17, -96, 0.232525094861855 ], [ 17, -316, 11.892071155021387 ], [ 17, -322, 7.928047436680925 ], [ 17, -327, -590.223714868562865 ], [ 17, -330, -905.865001869115304 ], [ 17, -334, 3.964023718340462 ], [ 17, -336, 3.397734615720396 ], [ 17, -339, -1790.223714868562865 ], [ 17, -341, -1955.227943368484375 ], [ 17, -342, -2105.865001869115531 ], [ 17, -344, 2.162194755458434 ], [ 17, -346, -2492.178715733950867 ], [ 17, -347, -2603.910001003728667 ], [ 17, -348, -2708.865410504136889 ], [ 17, -349, -2807.820002734504669 ], [ 17, -351, -2990.223714868562638 ], [ 17, -352, -3074.690908338240661 ], [ 17, -353, 1.251796963686462 ], [ 17, -354, 1.189207115502139 ], [ 17, -355, 1.132578205240132 ], [ 17, -300, 2196.089998996271333 ], [ 17, -288, 3396.089998996271333 ], [ 17, -281, 4098.044999861659562 ], [ 17, -276, 4596.089998996270879 ], [ 17, -272, 4982.403712861106214 ], [ 17, -269, 5298.044999861659562 ], [ 17, -266, 5564.915905465396463 ], [ 17, -264, 5796.089998996270879 ], [ 17, -262, 6000.000000727046427 ], [ 17, -260, 6182.403712861107124 ], [ 17, -259, 6347.407941361027952 ], [ 17, -257, 6498.044999861659562 ], [ 17, -256, 6636.617660765581604 ], [ 17, -254, 6764.915905465396463 ], [ 17, -253, 6884.358713726494898 ], [ 17, -252, 6996.089998996270879 ], [ 17, -251, 7101.045408496679556 ], [ 17, -250, 7200.000000727047336 ], [ 17, -249, 7293.603015128574953 ], [ 17, -248, 7382.403712861107124 ], [ 17, -312, 996.08999899627122 ], [ 17, -319, 294.13499813088464 ], [ 17, -323, -203.91000100372878 ], [ 17, -333, -1172.735907472852432 ], [ 17, -335, -1403.910001003728667 ], [ 17, -337, -1607.820002734503305 ], [ 17, -343, -2244.437662773039392 ], [ 17, -345, -2372.735907472852432 ], [ 17, -350, -2901.423017136030921 ], [ 17, 23, 2196.089998996271333 ], [ 17, 11, 996.08999899627122 ], [ 17, 4, 294.13499813088464 ], [ 17, 7, 11.892071155021393 ], [ 17, 1, 7.928047436680928 ], [ 18, -1, 2046.706901192160785 ], [ 18, -5, 1647.61649428791452 ], [ 18, -8, 1251.148885925244485 ], [ 18, -11, 3.964023718340464 ], [ 18, -13, 3.397734615720398 ], [ 18, -15, 3215.532808469114116 ], [ 18, -17, 11.892071155021387 ], [ 18, -19, 2.378414231004279 ], [ 18, -20, 8.908987187223364 ], [ 18, -21, 2.162194755458435 ], [ 18, -23, 1.982011859170232 ], [ 18, -24, 1.82954940846483 ], [ 18, -25, 1.698867307860199 ], [ 18, -26, 1.585609487336186 ], [ 18, -27, 5.339359418424158 ], [ 18, -28, 1.486508894377674 ], [ 18, -29, 1.399067194708399 ], [ 18, -30, 1.251796963686462 ], [ 18, -36, 3.3333333370663 ], [ 18, -43, 2.227246796805841 ], [ 18, -48, 1.61981585222243 ], [ 18, -52, 5.946035577510694 ], [ 18, -55, 4.756828462008555 ], [ 18, -58, 0.890898718722336 ], [ 18, -60, 0.833333334266575 ], [ 18, -62, 3.3333333370663 ], [ 18, -64, 2.973017788755347 ], [ 18, -66, 2.642682478893642 ], [ 18, -67, 2.378414231004277 ], [ 18, -68, 0.500000000559945 ], [ 18, -70, 2.00000000223978 ], [ 18, -71, 1.982011859170231 ], [ 18, -72, 1.829549408464829 ], [ 18, -73, 1.698867307860198 ], [ 18, -74, 1.585609487336185 ], [ 18, -75, 0.769230770092223 ], [ 18, -76, 1.486508894377673 ], [ 18, -77, 1.399067194708399 ], [ 18, -32, 1.132578205240133 ], [ 18, -35, 7.928047436680925 ], [ 18, -38, 2.969662395741121 ], [ 18, -40, 2.545424910635247 ], [ 18, -42, 5.291336843844476 ], [ 18, -44, 2.00000000223978 ], [ 18, -46, 1.781797437444673 ], [ 18, -47, 7.928047436680925 ], [ 18, -49, 1.570399828948281 ], [ 18, -50, 1.484831197870561 ], [ 18, -51, 1.370613413418979 ], [ 18, -53, 1.187864958296448 ], [ 18, -33, 3.813828156017255 ], [ 18, -37, 3.964023718340462 ], [ 18, -41, 11.892071155021387 ], [ 18, -56, 1.048116139673337 ], [ 18, -57, 0.93778812497088 ], [ 18, -59, 3.964023718340462 ], [ 18, -61, 3.397734615720396 ], [ 18, -63, 0.66666666741326 ], [ 18, -65, 0.625000000699931 ], [ 18, -2, 1908.134240288237834 ], [ 18, -3, 1779.835995588422293 ], [ 18, -4, 5.946035577510696 ], [ 18, -6, 1548.661902057547422 ], [ 18, -7, 4.756828462008557 ], [ 18, -9, 1162.348188192712769 ], [ 18, -10, 1077.880994723034974 ], [ 18, -12, 3482.403714072851926 ], [ 18, -14, 3300.000001938791684 ], [ 18, -16, 2.973017788755348 ], [ 18, -18, 2.642682478893643 ], [ 18, -69, 2.162194755458434 ], [ 18, -78, 1.251796963686462 ], [ 18, -79, 1.189207115502139 ], [ 18, -80, 1.132578205240132 ], [ 18, -81, 1.111111112355434 ], [ 18, -82, 1.00000000111989 ], [ 18, -83, 0.476190476723757 ], [ 18, -84, 0.909090910108991 ], [ 18, -85, 0.424716827440686 ], [ 18, -86, 0.833333334266575 ], [ 18, -87, 0.769230770092223 ], [ 18, -88, 0.714285715085636 ], [ 18, -89, 0.66666666741326 ], [ 18, -90, 0.312949241272084 ], [ 18, -91, 0.625000000699931 ], [ 18, -92, 0.588235294776406 ], [ 18, -93, 0.5263157900631 ], [ 18, -94, 0.500000000559945 ], [ 18, -95, 0.476190476723757 ], [ 18, -96, 0.232525094861855 ], [ 18, -45, 1.979774930494081 ], [ 18, -54, 2.642682478893642 ], [ 18, -22, 7.928047436680925 ], [ 18, -31, 1.189207115502139 ], [ 18, -34, 3.563594874889346 ], [ 18, -39, 2.669679709212079 ], [ 18, -316, 11.892071155021387 ], [ 18, -322, 7.928047436680925 ], [ 18, -327, 5.946035577510694 ], [ 18, -330, 4.756828462008555 ], [ 18, -334, 3.964023718340462 ], [ 18, -336, 3.397734615720396 ], [ 18, -339, 2.973017788755347 ], [ 18, -341, 2.642682478893642 ], [ 18, -342, 2.378414231004277 ], [ 18, -344, 2.162194755458434 ], [ 18, -346, 1.982011859170231 ], [ 18, -347, 1.829549408464829 ], [ 18, -348, 1.698867307860198 ], [ 18, -349, 1.585609487336185 ], [ 18, -351, 1.486508894377673 ], [ 18, -352, 1.399067194708399 ], [ 18, -353, 1.251796963686462 ], [ 18, -354, 1.189207115502139 ], [ 18, -355, 1.132578205240132 ], [ 18, -222, 9986.313714591880853 ], [ 18, -210, 11186.313714591880853 ], [ 18, -203, 11888.268715457268627 ], [ 18, -198, 12386.313714591880853 ], [ 18, -194, 12772.627428456717098 ], [ 18, -191, 13088.268715457268627 ], [ 18, -188, 13355.139621061005528 ], [ 18, -186, 13586.313714591880853 ], [ 18, -184, 13790.223716322656401 ], [ 18, -182, 13972.627428456718917 ], [ 18, -181, 14137.631656956637016 ], [ 18, -179, 14288.268715457268627 ], [ 18, -178, 14426.841376361193397 ], [ 18, -176, 14555.139621061005528 ], [ 18, -175, 14674.582429322103053 ], [ 18, -174, 14786.313714591880853 ], [ 18, -173, 14891.269124092286802 ], [ 18, -172, 14990.223716322656401 ], [ 18, -171, 15083.826730724184017 ], [ 18, -170, 15172.627428456718917 ], [ 18, -234, 8786.313714591880853 ], [ 18, -241, 8084.358713726493079 ], [ 18, -246, 7586.313714591880853 ], [ 18, -250, 7200.000000727047336 ], [ 18, -253, 6884.358713726493079 ], [ 18, -256, 6617.487808122756178 ], [ 18, -258, 6386.313714591880853 ], [ 18, -260, 6182.403712861107124 ], [ 18, -262, 6000.000000727046427 ], [ 18, -264, 5834.99577222712378 ], [ 18, -265, 5684.358713726493079 ], [ 18, -267, 5545.786052822571037 ], [ 18, -268, 5417.487808122756178 ], [ 18, -269, 5298.044999861659562 ], [ 18, -270, 5186.313714591880853 ], [ 18, -271, 5081.358305091473994 ], [ 18, -272, 4982.403712861106214 ], [ 18, -273, 4888.800698459579507 ], [ 18, -274, 4800.000000727046427 ], [ 18, -275, 4715.532807257368404 ], [ 18, 7, 11.892071155021393 ], [ 18, 1, 7.928047436680928 ], [ 19, -1, -272.627427406537493 ], [ 19, -5, -655.139620172393279 ], [ 19, -8, 3915.641287727598865 ], [ 19, -11, 3.964023718340464 ], [ 19, -13, 3.397734615720398 ], [ 19, -15, 3213.686286862211546 ], [ 19, -17, -1926.841375311013962 ], [ 19, -19, 2.378414231004279 ], [ 19, -20, -2174.582428271924073 ], [ 19, -21, 2.162194755458435 ], [ 19, -23, 1.982011859170232 ], [ 19, -24, 1.82954940846483 ], [ 19, -25, 1.698867307860199 ], [ 19, -26, 1.585609487336186 ], [ 19, -27, -2891.269122638195313 ], [ 19, -28, 1.486508894377674 ], [ 19, -29, 1.399067194708399 ], [ 19, -30, 1.251796963686462 ], [ 19, -9, -1088.268714003174864 ], [ 19, -12, -1355.139619606913584 ], [ 19, -22, -2426.841374907098725 ], [ 19, -31, 1.189207115502139 ], [ 19, -32, 1.132578205240133 ], [ 19, -33, -3472.627426760272101 ], [ 19, -34, -3557.094620229952397 ], [ 19, -2, -386.313713137788284 ], [ 19, -7, 4.756828462008557 ], [ 19, -14, -1586.313713137788227 ], [ 19, -35, -3672.627426598705824 ], [ 19, -36, -3757.094620068383392 ], [ 19, -4, 5.946035577510696 ], [ 19, -16, 2.973017788755348 ], [ 19, -18, 2.642682478893643 ], [ 19, -6, -772.62742700262379 ], [ 19, -10, -1155.139619768479406 ], [ 19, -37, -3872.627426437141366 ], [ 19, -38, -3957.094619906818934 ], [ 19, -3, 4413.686286862212 ], [ 19, -39, 813.686286862211773 ], [ 19, -40, -4172.627426194791951 ], [ 19, -41, 11.892071155021387 ], [ 19, -42, -4372.627426033227493 ], [ 19, -43, -4457.094619502905516 ], [ 19, -48, -4955.139617668120991 ], [ 19, -52, 5.946035577510694 ], [ 19, -55, 4.756828462008555 ], [ 19, -58, -3757.094620068383392 ], [ 19, -60, -1355.139619606913584 ], [ 19, -62, -1586.313713137788227 ], [ 19, -64, 2.973017788755347 ], [ 19, -66, 2.642682478893642 ], [ 19, -67, 2.378414231004277 ], [ 19, -68, -4788.881430410471694 ], [ 19, -70, -2426.841374907098725 ], [ 19, -71, 1.982011859170231 ], [ 19, -72, 1.829549408464829 ], [ 19, -73, 1.698867307860198 ], [ 19, -74, 1.585609487336185 ], [ 19, -75, -2891.269122638195313 ], [ 19, -76, 1.486508894377673 ], [ 19, -77, 1.399067194708399 ], [ 19, -45, -4688.268712064382271 ], [ 19, -50, -386.313713137788284 ], [ 19, -54, -772.62742700262379 ], [ 19, -57, -1088.268714003174864 ], [ 19, -69, 2.162194755458434 ], [ 19, -78, 1.251796963686462 ], [ 19, -79, 1.189207115502139 ], [ 19, -44, -4557.094619422120559 ], [ 19, -46, 111.731285996825136 ], [ 19, -47, 7.928047436680925 ], [ 19, -49, -5074.582425929217607 ], [ 19, -51, -5291.269120699402265 ], [ 19, -53, -5483.82672733129948 ], [ 19, -56, -3606.457561567754055 ], [ 19, -59, 3.964023718340462 ], [ 19, -61, 3.397734615720396 ], [ 19, -63, -4255.139619202996982 ], [ 19, -65, -4459.049620933771621 ], [ 19, -80, 1.132578205240132 ], [ 19, -81, -6064.493187441434202 ], [ 19, -82, -6192.791432141246332 ], [ 19, -83, -6312.234240402343858 ], [ 19, -84, -6423.965525672121657 ], [ 19, -85, -6528.920935172527606 ], [ 19, -86, -6627.875527402895386 ], [ 19, -87, -6721.478541804424822 ], [ 19, -88, -6810.279239536958812 ], [ 19, -89, -6894.746433006634106 ], [ 19, -90, -9172.627422155641398 ], [ 19, -91, -9257.09461562531942 ], [ 19, -92, -4583.826728058346816 ], [ 19, -93, -4672.627425790878078 ], [ 19, -94, -4757.094619260556101 ], [ 19, -95, 0.476190476723757 ], [ 19, -96, 0.232963821455934 ], [ 19, -97, 0.220483616735081 ], [ 19, -98, 0.207513992221252 ], [ 19, -316, 11.892071155021387 ], [ 19, -322, 7.928047436680925 ], [ 19, -327, 5.946035577510694 ], [ 19, -330, 4.756828462008555 ], [ 19, -334, 3.964023718340462 ], [ 19, -336, 3.397734615720396 ], [ 19, -339, -1790.223714868562865 ], [ 19, -341, -1972.62742700262379 ], [ 19, -342, -2137.631655502545527 ], [ 19, -344, -2288.268714003174864 ], [ 19, -346, 1.982011859170231 ], [ 19, -347, -2555.139619606913584 ], [ 19, -348, -2674.5824278680102 ], [ 19, -349, -2786.313713137789819 ], [ 19, -351, -2990.223714868562638 ], [ 19, -352, -3083.826729270092073 ], [ 19, -353, -3172.62742700262379 ], [ 19, -354, -3257.094620472301358 ], [ 19, -355, 1.132578205240132 ], [ 19, -302, 2013.686286862211773 ], [ 19, -290, 3213.686286862211546 ], [ 19, -283, 3915.641287727598865 ], [ 19, -278, 4413.686286862212 ], [ 19, -274, 4800.000000727046427 ], [ 19, -271, 5115.641287727598865 ], [ 19, -268, 5382.512193331336675 ], [ 19, -266, 5613.686286862212 ], [ 19, -264, 5817.596288592986639 ], [ 19, -262, 6000.000000727046427 ], [ 19, -260, 6165.004229226969073 ], [ 19, -259, 6315.641287727598865 ], [ 19, -257, 6454.213948631521816 ], [ 19, -256, 6582.512193331336675 ], [ 19, -255, 6701.955001592434201 ], [ 19, -254, 6813.686286862212 ], [ 19, -253, 6918.641696362618859 ], [ 19, -252, 7017.596288592986639 ], [ 19, -251, 7111.199302994514255 ], [ 19, -250, 7200.000000727047336 ], [ 19, -314, 813.686286862211773 ], [ 19, -321, 111.731285996825136 ], [ 19, -325, -386.313713137788284 ], [ 19, -329, -772.62742700262379 ], [ 19, -332, -1088.268714003174864 ], [ 19, -335, -1355.139619606913584 ], [ 19, -337, -1586.313713137788227 ], [ 19, -345, -2426.841374907098725 ], [ 19, -350, -2891.269122638195313 ], [ 19, 21, 2013.686286862211773 ], [ 19, 9, 813.686286862211773 ], [ 19, 2, 111.731285996825136 ], [ 19, 7, 11.892071155021393 ], [ 19, 1, 7.928047436680928 ], [ 20, -5, 1608.710721057062074 ], [ 20, -8, 1327.352416692634279 ], [ 20, -12, 3465.004230438713421 ], [ 20, -14, 3.964023718340462 ], [ 20, -17, 11.892071155021387 ], [ 20, -19, 2.378414231004279 ], [ 20, -20, 8.908987187223364 ], [ 20, -22, 7.928047436680925 ], [ 20, -24, 1.82954940846483 ], [ 20, -25, 1.698867307860199 ], [ 20, -26, 1.585609487336186 ], [ 20, -27, 5.339359418424158 ], [ 20, -29, 1.399067194708399 ], [ 20, -30, 1.251796963686462 ], [ 20, -31, 1.189207115502139 ], [ 20, -32, 1.132578205240133 ], [ 20, -33, 3.813828156017255 ], [ 20, -7, 4.756828462008557 ], [ 20, -15, 11.892071155021387 ], [ 20, -21, 2.162194755458435 ], [ 20, -34, 3.563594874889346 ], [ 20, -36, 3.3333333370663 ], [ 20, -37, 3.964023718340462 ], [ 20, -38, 2.969662395741121 ], [ 20, -39, 2.669679709212079 ], [ 20, -40, 2.545424910635247 ], [ 20, -4, 5.946035577510696 ], [ 20, -11, 3.964023718340464 ], [ 20, -16, 2.973017788755348 ], [ 20, -23, 1.982011859170232 ], [ 20, -28, 1.486508894377674 ], [ 20, -35, 7.928047436680925 ], [ 20, -41, 11.892071155021387 ], [ 20, -42, 5.291336843844476 ], [ 20, -43, 2.227246796805841 ], [ 20, -44, 2.00000000223978 ], [ 20, -45, 1.979774930494081 ], [ 20, -47, 7.928047436680925 ], [ 20, -48, 1.61981585222243 ], [ 20, -50, 1.484831197870561 ], [ 20, -52, 5.946035577510694 ], [ 20, -53, 1.187864958296448 ], [ 20, -54, 2.642682478893642 ], [ 20, -55, 4.756828462008555 ], [ 20, -57, 0.93778812497088 ], [ 20, -58, 0.890898718722336 ], [ 20, -59, 3.964023718340462 ], [ 20, -60, 0.833333334266575 ], [ 20, -61, 3.397734615720396 ], [ 20, -51, 1.370613413418979 ], [ 20, -63, 0.66666666741326 ], [ 20, -65, 0.625000000699931 ], [ 20, -66, 2.642682478893642 ], [ 20, -68, 0.500000000559945 ], [ 20, -70, 2.00000000223978 ], [ 20, -71, 1.982011859170231 ], [ 20, -72, 1.829549408464829 ], [ 20, -73, 1.698867307860198 ], [ 20, -75, 0.769230770092223 ], [ 20, -76, 1.486508894377673 ], [ 20, -77, 1.399067194708399 ], [ 20, -78, 1.251796963686462 ], [ 20, -79, 1.189207115502139 ], [ 20, -2, 1944.840224088345394 ], [ 20, -3, 1825.397415827246959 ], [ 20, -1, 2029.307417558021598 ], [ 20, -62, 3.3333333370663 ], [ 20, -74, 1.585609487336185 ], [ 20, -80, 1.132578205240132 ], [ 20, -81, 1.111111112355434 ], [ 20, -83, 0.476190476723757 ], [ 20, -84, 0.909090910108991 ], [ 20, -85, 0.424716827440686 ], [ 20, -86, 0.833333334266575 ], [ 20, -87, 0.769230770092223 ], [ 20, -6, 1509.756128826694521 ], [ 20, -88, 0.714285715085636 ], [ 20, -89, 0.66666666741326 ], [ 20, -90, 0.312949241272084 ], [ 20, -64, 2.973017788755347 ], [ 20, -67, 2.378414231004277 ], [ 20, -69, 2.162194755458434 ], [ 20, -82, 1.00000000111989 ], [ 20, -9, 1242.885223222957393 ], [ 20, -10, 3746.362534803141443 ], [ 20, -49, 1.570399828948281 ], [ 20, -56, 1.048116139673337 ], [ 20, -46, 1.781797437444673 ], [ 20, -13, 3.397734615720398 ], [ 20, -18, 2.642682478893643 ], [ 20, -316, 11.892071155021387 ], [ 20, -322, 7.928047436680925 ], [ 20, -327, 5.946035577510694 ], [ 20, -330, 4.756828462008555 ], [ 20, -334, 3.964023718340462 ], [ 20, -336, 3.397734615720396 ], [ 20, -339, 2.973017788755347 ], [ 20, -341, 2.642682478893642 ], [ 20, -342, 2.378414231004277 ], [ 20, -344, 2.162194755458434 ], [ 20, -346, 1.982011859170231 ], [ 20, -347, 1.829549408464829 ], [ 20, -348, 1.698867307860198 ], [ 20, -349, 1.585609487336185 ], [ 20, -351, 1.486508894377673 ], [ 20, -352, 1.399067194708399 ], [ 20, -353, 1.251796963686462 ], [ 20, -354, 1.189207115502139 ], [ 20, -355, 1.132578205240132 ], [ 20, -220, 10151.31794309180259 ], [ 20, -208, 11351.31794309180259 ], [ 20, -201, 12053.272943957190364 ], [ 20, -196, 12551.31794309180259 ], [ 20, -193, 12937.631656956637016 ], [ 20, -189, 13253.272943957190364 ], [ 20, -187, 13520.143849560929084 ], [ 20, -184, 13751.31794309180259 ], [ 20, -182, 13955.227944822578138 ], [ 20, -181, 14137.631656956637016 ], [ 20, -179, 14302.635885456562391 ], [ 20, -177, 14453.272943957190364 ], [ 20, -176, 14591.845604861115135 ], [ 20, -175, 14720.143849560929084 ], [ 20, -174, 14839.58665782202479 ], [ 20, -172, 14951.31794309180259 ], [ 20, -171, 15056.273352592212177 ], [ 20, -170, 15248.830959224107573 ], [ 20, -169, 15337.631656956637016 ], [ 20, -232, 8951.31794309180259 ], [ 20, -240, 8249.362942226416635 ], [ 20, -244, 7751.31794309180259 ], [ 20, -248, 7365.004229226969073 ], [ 20, -252, 7049.362942226416635 ], [ 20, -254, 6782.492036622677915 ], [ 20, -256, 6551.31794309180259 ], [ 20, -259, 6347.407941361027952 ], [ 20, -260, 6165.004229226969073 ], [ 20, -262, 6000.000000727046427 ], [ 20, -264, 5849.362942226416635 ], [ 20, -265, 5710.790281322492774 ], [ 20, -266, 5582.492036622677915 ], [ 20, -267, 5463.04922836158039 ], [ 20, -268, 5351.31794309180259 ], [ 20, -270, 5246.362533591395731 ], [ 20, -271, 5053.804926959500335 ], [ 20, -272, 4965.004229226969073 ], [ 20, -273, 4880.537035757291051 ], [ 20, -91, 0.625000000699931 ], [ 20, -92, 0.588235294776406 ], [ 20, -93, 0.5263157900631 ], [ 20, -94, 0.500000000559945 ], [ 20, -95, 0.476190476723757 ], [ 20, 7, 11.892071155021393 ], [ 20, 1, 7.928047436680928 ] ],
									"coldef" : [ [ 0, 36, 1, 0.0, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ] ],
									"colhead" : 1,
									"cols" : 21,
									"colwidth" : 92,
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-18",
									"just" : 1,
									"maxclass" : "jit.cellblock",
									"numinlets" : 2,
									"numoutlets" : 4,
									"outlettype" : [ "list", "", "", "" ],
									"patching_rect" : [ 11.5, 34.0, 607.0, 441.0 ],
									"precision" : 4,
									"presentation" : 1,
									"presentation_rect" : [ 4.0, 4.0, 1903.0, 2026.16650390625 ],
									"rowhead" : 1,
									"rowheight" : 22,
									"rows" : 26,
									"savemode" : 1
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"midpoints" : [ 832.0, 185.0, 746.0, 185.0 ],
									"source" : [ "obj-102", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-102", 0 ],
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"midpoints" : [ 976.5, 81.0, 746.0, 81.0 ],
									"source" : [ "obj-45", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-45", 0 ],
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-45", 0 ],
									"midpoints" : [ 921.0, 48.0, 976.5, 48.0 ],
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-5", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"midpoints" : [ 746.0, 86.0, 746.0, 86.0 ],
									"source" : [ "obj-95", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"source" : [ "obj-97", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1459.75, 145.0, 48.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p sheet"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 240.0, 323.0, 103.0, 22.0 ],
					"text" : "prepend root"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.941176, 0.690196, 0.196078, 1.0 ],
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 240.0, 352.0, 103.0, 22.0 ],
					"text" : "s shear_structure"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-37",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "shear_roll.maxpat",
					"numinlets" : 2,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 24.0, 384.0, 246.0, 179.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 37.0, 247.5, 230.0, 166.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-50",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 240.0, 287.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 2,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 55.0, 201.0, 731.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-50",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 332.0, 440.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 175.5, 50.0, 83.0, 22.0 ],
									"text" : "t b l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 175.5, 82.0, 30.0, 22.0 ],
									"text" : "i 60"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 219.5, 374.0, 70.0, 22.0 ],
									"text" : "clear $2 $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-42",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "int" ],
									"patching_rect" : [ 525.0, 219.0, 93.0, 22.0 ],
									"text" : "t b 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 471.0, 50.0, 72.0, 22.0 ],
									"text" : "loadmess 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 525.0, 82.0, 150.0, 20.0 ],
									"text" : "ROOT Column"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.995808, 0.800103, 0.399985, 1.0 ],
									"id" : "obj-33",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 471.0, 80.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-65",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 525.0, 440.0, 52.0, 22.0 ],
									"text" : "pack i 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-59",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 599.0, 334.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-57",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 525.0, 398.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-54",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 525.0, 369.0, 93.0, 22.0 ],
									"text" : "+ 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 525.0, 334.0, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 525.0, 175.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 239.5, 175.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "int" ],
									"patching_rect" : [ 525.0, 298.0, 43.0, 22.0 ],
									"text" : "uzi 88"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 91.0, 324.0, 109.0, 22.0 ],
									"text" : "pack i f 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "int", "bang" ],
									"patching_rect" : [ 91.0, 175.0, 109.0, 22.0 ],
									"text" : "t i i b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 91.0, 269.0, 226.5, 22.0 ],
									"text" : "expr $f1 - $f2 + 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 298.5, 175.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 239.5, 135.0, 137.0, 22.0 ],
									"text" : "route range offset"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.941176, 0.690196, 0.196078, 1.0 ],
									"id" : "obj-27",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 175.5, 21.0, 87.0, 22.0 ],
									"text" : "r shear_global"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 91.0, 135.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-6",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 91.0, 437.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 91.0, 374.0, 77.0, 22.0 ],
									"text" : "set $3 $1 $2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 11.0, 215.0, 105.5, 22.0 ],
									"text" : "261.625565"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 136.0, 215.0, 34.0, 22.0 ],
									"text" : "mtof"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 91.0, 21.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"source" : [ "obj-15", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 1 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"midpoints" : [ 190.5, 207.0, 513.0, 207.0, 513.0, 171.0, 534.5, 171.0 ],
									"source" : [ "obj-23", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"source" : [ "obj-23", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 1 ],
									"midpoints" : [ 249.0, 252.0, 558.5, 252.0 ],
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 2 ],
									"midpoints" : [ 480.5, 309.0, 190.5, 309.0 ],
									"order" : 1,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-65", 1 ],
									"midpoints" : [ 480.5, 432.0, 567.5, 432.0 ],
									"order" : 0,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 1 ],
									"order" : 1,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 1 ],
									"order" : 2,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"midpoints" : [ 145.5, 260.0, 341.5, 260.0 ],
									"order" : 0,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"source" : [ "obj-42", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"midpoints" : [ 229.0, 423.0, 100.5, 423.0 ],
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"midpoints" : [ 185.0, 120.0, 100.5, 120.0 ],
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-49", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 0 ],
									"source" : [ "obj-53", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-57", 0 ],
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"order" : 0,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-65", 0 ],
									"order" : 1,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 1 ],
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"source" : [ "obj-65", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"order" : 1,
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"order" : 0,
									"source" : [ "obj-8", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 55.0, 255.0, 204.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p cellblock_root"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 24.0, 221.0, 50.0, 22.0 ],
					"text" : "t i i"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "korg" ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-6",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "shear_kslider.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 145.0, 83.0, 424.0, 72.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 141.0, 3.0, 423.0, 68.0 ],
					"varname" : "KeyboardLayout",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"hkeycolor" : [ 0.104613155126572, 0.802422881126404, 0.399308800697327, 1.0 ],
					"id" : "obj-68",
					"ignoreclick" : 1,
					"maxclass" : "kslider",
					"mode" : 1,
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : 22,
					"outlettype" : [ "int", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 89.375, 727.0, 416.0, 38.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 143.0, 117.5, 416.0, 38.0 ],
					"range" : 89,
					"selectioncolor" : [ 0.788235294117647, 0.898039215686275, 0.156862745098039, 1.0 ],
					"whitekeycolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 355.375, 687.0, 150.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 143.0, 151.0, 416.0, 20.0 ],
					"text" : "|",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.811543345451355, 0.811519026756287, 0.811532855033875, 1.0 ],
					"id" : "obj-13",
					"ignoreclick" : 1,
					"knobcolor" : [ 0.105019092559814, 0.806525588035583, 0.435014843940735, 1.0 ],
					"knobshape" : 5,
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 89.375, 769.0, 416.0, 10.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 143.0, 157.0, 416.0, 11.0 ],
					"size" : 100.0
				}

			}
, 			{
				"box" : 				{
					"hkeycolor" : [ 0.155868142843246, 0.777949690818787, 0.896290421485901, 1.0 ],
					"id" : "obj-33",
					"ignoreclick" : 1,
					"maxclass" : "kslider",
					"mode" : 1,
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : 22,
					"outlettype" : [ "int", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 89.375, 584.0, 416.0, 38.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 143.0, 66.5, 416.0, 38.0 ],
					"range" : 89,
					"selectioncolor" : [ 0.788235294117647, 0.898039215686275, 0.156862745098039, 1.0 ],
					"whitekeycolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ]
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-137", 1 ],
					"order" : 0,
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-138", 0 ],
					"order" : 1,
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-110", 1 ],
					"source" : [ "obj-102", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"source" : [ "obj-103", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"source" : [ "obj-103", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"source" : [ "obj-103", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"source" : [ "obj-103", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 0 ],
					"source" : [ "obj-103", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-173", 0 ],
					"order" : 1,
					"source" : [ "obj-103", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"order" : 0,
					"source" : [ "obj-103", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"source" : [ "obj-105", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 4 ],
					"order" : 0,
					"source" : [ "obj-106", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 2 ],
					"order" : 1,
					"source" : [ "obj-106", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 1 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-181", 0 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-138", 0 ],
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 0 ],
					"source" : [ "obj-111", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 3 ],
					"order" : 0,
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 1 ],
					"order" : 1,
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 0 ],
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"source" : [ "obj-115", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 0 ],
					"source" : [ "obj-116", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 1 ],
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 0 ],
					"source" : [ "obj-119", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"order" : 1,
					"source" : [ "obj-121", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 2 ],
					"source" : [ "obj-121", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 1 ],
					"order" : 0,
					"source" : [ "obj-121", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 0 ],
					"order" : 0,
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 5 ],
					"order" : 1,
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-158", 1 ],
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-139", 0 ],
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"source" : [ "obj-127", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-135", 1 ],
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 0 ],
					"source" : [ "obj-130", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 0 ],
					"source" : [ "obj-131", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-127", 0 ],
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"source" : [ "obj-133", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"source" : [ "obj-135", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-135", 0 ],
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-206", 0 ],
					"source" : [ "obj-139", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-139", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 0 ],
					"source" : [ "obj-140", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-147", 0 ],
					"source" : [ "obj-141", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-144", 0 ],
					"source" : [ "obj-143", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 1 ],
					"source" : [ "obj-144", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-182", 0 ],
					"order" : 0,
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 0 ],
					"order" : 1,
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-148", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-153", 0 ],
					"source" : [ "obj-151", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-152", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-134", 0 ],
					"order" : 0,
					"source" : [ "obj-153", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"order" : 1,
					"source" : [ "obj-153", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-167", 0 ],
					"source" : [ "obj-154", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-148", 0 ],
					"order" : 1,
					"source" : [ "obj-155", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-187", 0 ],
					"order" : 0,
					"source" : [ "obj-155", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"source" : [ "obj-157", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 1 ],
					"order" : 1,
					"source" : [ "obj-158", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-138", 0 ],
					"order" : 0,
					"source" : [ "obj-158", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-163", 0 ],
					"order" : 1,
					"source" : [ "obj-159", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-163", 0 ],
					"source" : [ "obj-159", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-163", 0 ],
					"source" : [ "obj-159", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 1 ],
					"order" : 0,
					"source" : [ "obj-159", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-161", 0 ],
					"source" : [ "obj-160", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-162", 0 ],
					"source" : [ "obj-161", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-159", 1 ],
					"order" : 1,
					"source" : [ "obj-162", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 1 ],
					"order" : 0,
					"source" : [ "obj-162", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-164", 0 ],
					"order" : 4,
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-166", 2 ],
					"order" : 1,
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-166", 1 ],
					"order" : 2,
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-166", 0 ],
					"order" : 3,
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 2 ],
					"order" : 0,
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-168", 1 ],
					"source" : [ "obj-166", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-158", 0 ],
					"source" : [ "obj-167", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 0 ],
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-177", 0 ],
					"source" : [ "obj-170", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-173", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-152", 0 ],
					"source" : [ "obj-174", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"source" : [ "obj-175", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"source" : [ "obj-176", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-180", 0 ],
					"order" : 1,
					"source" : [ "obj-177", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-189", 0 ],
					"order" : 1,
					"source" : [ "obj-177", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-191", 0 ],
					"order" : 0,
					"source" : [ "obj-177", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-208", 0 ],
					"order" : 0,
					"source" : [ "obj-177", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 0 ],
					"midpoints" : [ 619.0, 758.0, 741.0, 758.0 ],
					"source" : [ "obj-178", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-209", 0 ],
					"source" : [ "obj-180", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-181", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-99", 0 ],
					"source" : [ "obj-183", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-154", 0 ],
					"source" : [ "obj-184", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"source" : [ "obj-185", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"order" : 0,
					"source" : [ "obj-188", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-193", 0 ],
					"order" : 1,
					"source" : [ "obj-188", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-186", 0 ],
					"order" : 0,
					"source" : [ "obj-190", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-215", 0 ],
					"order" : 1,
					"source" : [ "obj-190", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-174", 0 ],
					"source" : [ "obj-192", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-203", 0 ],
					"source" : [ "obj-194", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-193", 0 ],
					"source" : [ "obj-195", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-155", 0 ],
					"source" : [ "obj-197", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"source" : [ "obj-198", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 1 ],
					"order" : 0,
					"source" : [ "obj-2", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-143", 0 ],
					"order" : 1,
					"source" : [ "obj-2", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"source" : [ "obj-2", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"order" : 1,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 1 ],
					"order" : 0,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-192", 0 ],
					"source" : [ "obj-200", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-185", 0 ],
					"source" : [ "obj-201", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-193", 0 ],
					"source" : [ "obj-202", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-193", 0 ],
					"source" : [ "obj-204", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-193", 0 ],
					"source" : [ "obj-205", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 0 ],
					"source" : [ "obj-206", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-139", 0 ],
					"source" : [ "obj-207", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-209", 0 ],
					"source" : [ "obj-208", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-209", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-197", 0 ],
					"source" : [ "obj-210", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-139", 0 ],
					"source" : [ "obj-211", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-214", 0 ],
					"order" : 1,
					"source" : [ "obj-212", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-225", 0 ],
					"order" : 0,
					"source" : [ "obj-212", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-213", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-216", 1 ],
					"order" : 0,
					"source" : [ "obj-214", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 2 ],
					"midpoints" : [ 1029.0, 1014.0, 507.0, 1014.0, 507.0, 981.0, 354.0, 981.0, 354.0, 906.0, 216.0, 906.0, 216.0, 846.0, 131.208333333333343, 846.0 ],
					"order" : 1,
					"source" : [ "obj-214", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-220", 1 ],
					"order" : 0,
					"source" : [ "obj-215", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"order" : 1,
					"source" : [ "obj-215", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-218", 0 ],
					"source" : [ "obj-217", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-190", 0 ],
					"source" : [ "obj-218", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-221", 0 ],
					"source" : [ "obj-219", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-193", 0 ],
					"source" : [ "obj-223", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-219", 0 ],
					"source" : [ "obj-225", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"midpoints" : [ 867.5, 570.0, 98.875, 570.0 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-23", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 1 ],
					"midpoints" : [ 736.5, 256.0, 976.5, 256.0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 1 ],
					"source" : [ "obj-24", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 1 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-30", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-66", 0 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-140", 0 ],
					"source" : [ "obj-32", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-171", 1 ],
					"order" : 0,
					"source" : [ "obj-32", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-199", 0 ],
					"order" : 1,
					"source" : [ "obj-32", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-32", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 1 ],
					"midpoints" : [ 495.875, 633.0, 125.875, 633.0 ],
					"source" : [ "obj-33", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 1 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 1 ],
					"order" : 0,
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-212", 0 ],
					"source" : [ "obj-4", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 1 ],
					"source" : [ "obj-4", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 2 ],
					"order" : 1,
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 1 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"midpoints" : [ 301.0, 316.0, 249.5, 316.0 ],
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 0 ],
					"order" : 0,
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"order" : 1,
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 2 ],
					"source" : [ "obj-52", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 1 ],
					"source" : [ "obj-52", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"order" : 0,
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"order" : 1,
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-188", 0 ],
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 1 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 1 ],
					"source" : [ "obj-6", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"source" : [ "obj-61", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"source" : [ "obj-61", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-179", 0 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"source" : [ "obj-65", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"source" : [ "obj-65", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"order" : 0,
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"order" : 1,
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-66", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-90", 0 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 0 ],
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-149", 1 ],
					"order" : 0,
					"source" : [ "obj-70", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-156", 1 ],
					"order" : 2,
					"source" : [ "obj-70", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 0 ],
					"order" : 1,
					"source" : [ "obj-70", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"source" : [ "obj-70", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"order" : 1,
					"source" : [ "obj-71", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"order" : 0,
					"source" : [ "obj-71", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"source" : [ "obj-72", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 0 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 2 ],
					"source" : [ "obj-76", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 1 ],
					"source" : [ "obj-76", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 0 ],
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 2 ],
					"midpoints" : [ 1255.25, 255.0, 1015.5, 255.0 ],
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 2 ],
					"midpoints" : [ 1082.5, 132.0, 836.5, 132.0 ],
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 1 ],
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"source" : [ "obj-86", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-87", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"midpoints" : [ 465.0, 273.0, 867.5, 273.0 ],
					"source" : [ "obj-87", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"source" : [ "obj-90", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"source" : [ "obj-90", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"midpoints" : [ 758.5, 437.0, 722.0, 437.0, 722.0, 165.0, 301.0, 165.0 ],
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-103", 0 ],
					"source" : [ "obj-94", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-75", 0 ],
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 1 ],
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"source" : [ "obj-98", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-66", 0 ],
					"source" : [ "obj-98", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"source" : [ "obj-99", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-112" : [ "ST-range", "STrange", 0 ],
			"obj-118" : [ "PBdecode", "pb", 0 ],
			"obj-121::obj-4" : [ "live.menu[2]", "live.menu[2]", 0 ],
			"obj-121::obj-48" : [ "live.numbox[5]", "live.numbox", 0 ],
			"obj-127" : [ "BaseFreq", "BaseFreq", 0 ],
			"obj-128" : [ "FreqHz", "FreqHz", 0 ],
			"obj-133" : [ "nano_pedal", "nanoPedal", 0 ],
			"obj-136" : [ "NoteName", "NoteName", 0 ],
			"obj-140" : [ "Flush", "flush", 0 ],
			"obj-144" : [ "CenterNote", "center", 0 ],
			"obj-167" : [ "notesOn/OFF", "noteON/OFF", 0 ],
			"obj-17" : [ "CentDeviation", "Cents", 0 ],
			"obj-173" : [ "live.menu[1]", "live.menu[1]", 0 ],
			"obj-175" : [ "live.text", "live.text", 0 ],
			"obj-219" : [ "live.button", "live.button", 0 ],
			"obj-27" : [ "Ped67", "ped67", 0 ],
			"obj-29" : [ "Pedal64", "ped64", 0 ],
			"obj-49::obj-190" : [ "osc-receive", "oscreceive", 0 ],
			"obj-49::obj-7" : [ "OSCsend-toggle", "OSCsend", 0 ],
			"obj-4::obj-101" : [ "startButton", "/start", 0 ],
			"obj-4::obj-104" : [ "StopButton", "/stop", 0 ],
			"obj-4::obj-112" : [ "factorNumbox", "factorNumbox", 0 ],
			"obj-4::obj-114" : [ "FactorRange", "FactorRange", 0 ],
			"obj-4::obj-132" : [ "ProgressSecs", "progress", 0 ],
			"obj-4::obj-134" : [ "live.numbox", "live.numbox", 0 ],
			"obj-4::obj-136" : [ "live.numbox[6]", "live.numbox", 0 ],
			"obj-4::obj-24" : [ "live.toggle", "live.toggle", 0 ],
			"obj-4::obj-31" : [ "StopButton[1]", "/stop", 0 ],
			"obj-4::obj-32" : [ "startButton[1]", "/start", 0 ],
			"obj-4::obj-47" : [ "progressSlider", "progress", 0 ],
			"obj-4::obj-50" : [ "live.numbox[2]", "live.numbox[2]", 0 ],
			"obj-4::obj-52::obj-2" : [ "test-bpm", "bpm", 0 ],
			"obj-4::obj-52::obj-33" : [ "polyTempo", "tempo", 0 ],
			"obj-4::obj-52::obj-34" : [ "BarLength", "bar", 0 ],
			"obj-4::obj-52::obj-36" : [ "betasPerBar", "beats", 0 ],
			"obj-4::obj-52::obj-6" : [ "Miliseconds", "ms", 0 ],
			"obj-4::obj-59" : [ "factor_dial", "factor", 0 ],
			"obj-4::obj-67" : [ "ScoreButton", "ScoreButton", 0 ],
			"obj-4::obj-82" : [ "FactorMin", "FactorMin", 0 ],
			"obj-4::obj-83" : [ "MaxButton", "Max", 0 ],
			"obj-4::obj-84" : [ "MinButton", "Min", 0 ],
			"obj-4::obj-85" : [ "zeroButton", "Zero", 0 ],
			"obj-4::obj-9" : [ "RammTime", "secs", 0 ],
			"obj-58::obj-115" : [ "sib-centernote", "sib/cn", 0 ],
			"obj-58::obj-66" : [ "sib-centernote[1]", "sib/cn", 0 ],
			"obj-58::obj-69" : [ "PB-range", "range", 0 ],
			"obj-58::obj-79" : [ "PBvalue", "pb", 0 ],
			"obj-6::obj-3" : [ "live.menu", "live.menu", 0 ],
			"parameterbanks" : 			{

			}
,
			"parameter_overrides" : 			{
				"obj-121::obj-4" : 				{
					"parameter_invisible" : 0,
					"parameter_longname" : "live.menu[2]",
					"parameter_modmode" : 0,
					"parameter_range" : [ "lowres Bend (0-127)", "hires Bend (-1. to 1.)", "14bit (-8192 to 8191)", "xbend (0 to 16383)", "xbend2" ],
					"parameter_shortname" : "live.menu[2]",
					"parameter_type" : 2,
					"parameter_unitstyle" : 10
				}
,
				"obj-121::obj-48" : 				{
					"parameter_longname" : "live.numbox[5]"
				}
,
				"obj-49::obj-190" : 				{
					"parameter_longname" : "osc-receive"
				}
,
				"obj-49::obj-7" : 				{
					"parameter_linknames" : 1,
					"parameter_longname" : "OSCsend-toggle",
					"parameter_shortname" : "OSCsend"
				}
,
				"obj-4::obj-101" : 				{
					"parameter_invisible" : 0,
					"parameter_longname" : "startButton",
					"parameter_modmode" : 0,
					"parameter_shortname" : "/start",
					"parameter_type" : 2,
					"parameter_unitstyle" : 10
				}
,
				"obj-4::obj-104" : 				{
					"parameter_invisible" : 0,
					"parameter_longname" : "StopButton",
					"parameter_modmode" : 0,
					"parameter_shortname" : "/stop",
					"parameter_type" : 2,
					"parameter_unitstyle" : 10
				}
,
				"obj-4::obj-112" : 				{
					"parameter_linknames" : 1,
					"parameter_longname" : "factorNumbox",
					"parameter_range" : [ -100.0, 100.0 ],
					"parameter_shortname" : "factorNumbox",
					"parameter_unitstyle" : 1
				}
,
				"obj-4::obj-114" : 				{
					"parameter_linknames" : 1,
					"parameter_longname" : "FactorRange",
					"parameter_shortname" : "FactorRange"
				}
,
				"obj-4::obj-132" : 				{
					"parameter_invisible" : 2,
					"parameter_linknames" : 0,
					"parameter_longname" : "ProgressSecs",
					"parameter_modmode" : 0,
					"parameter_range" : [ 0.0, 100.0 ],
					"parameter_shortname" : "progress",
					"parameter_type" : 0,
					"parameter_unitstyle" : 1
				}
,
				"obj-4::obj-134" : 				{
					"parameter_longname" : "live.numbox",
					"parameter_shortname" : "live.numbox"
				}
,
				"obj-4::obj-136" : 				{
					"parameter_longname" : "live.numbox[6]",
					"parameter_unitstyle" : 2
				}
,
				"obj-4::obj-24" : 				{
					"parameter_longname" : "live.toggle",
					"parameter_shortname" : "live.toggle"
				}
,
				"obj-4::obj-31" : 				{
					"parameter_invisible" : 0,
					"parameter_longname" : "StopButton[1]",
					"parameter_modmode" : 0,
					"parameter_type" : 2,
					"parameter_unitstyle" : 10
				}
,
				"obj-4::obj-32" : 				{
					"parameter_invisible" : 0,
					"parameter_longname" : "startButton[1]",
					"parameter_modmode" : 0,
					"parameter_type" : 2,
					"parameter_unitstyle" : 10
				}
,
				"obj-4::obj-47" : 				{
					"parameter_invisible" : 2,
					"parameter_linknames" : 1,
					"parameter_longname" : "progressSlider",
					"parameter_range" : [ 0.0, 100.0 ],
					"parameter_shortname" : "progress",
					"parameter_unitstyle" : 1
				}
,
				"obj-4::obj-50" : 				{
					"parameter_invisible" : 2,
					"parameter_longname" : "live.numbox[2]",
					"parameter_shortname" : "live.numbox[2]"
				}
,
				"obj-4::obj-59" : 				{
					"parameter_initial" : 64.0,
					"parameter_initial_enable" : 1,
					"parameter_invisible" : 2,
					"parameter_linknames" : 1,
					"parameter_longname" : "factor_dial",
					"parameter_shortname" : "factor"
				}
,
				"obj-4::obj-67" : 				{
					"parameter_longname" : "ScoreButton",
					"parameter_shortname" : "ScoreButton"
				}
,
				"obj-4::obj-82" : 				{
					"parameter_longname" : "FactorMin",
					"parameter_shortname" : "FactorMin"
				}
,
				"obj-4::obj-83" : 				{
					"parameter_invisible" : 0,
					"parameter_linknames" : 1,
					"parameter_longname" : "MaxButton",
					"parameter_modmode" : 0,
					"parameter_shortname" : "Max",
					"parameter_type" : 2,
					"parameter_unitstyle" : 10
				}
,
				"obj-4::obj-84" : 				{
					"parameter_invisible" : 0,
					"parameter_linknames" : 1,
					"parameter_longname" : "MinButton",
					"parameter_modmode" : 0,
					"parameter_shortname" : "Min",
					"parameter_type" : 2,
					"parameter_unitstyle" : 10
				}
,
				"obj-4::obj-85" : 				{
					"parameter_invisible" : 0,
					"parameter_linknames" : 1,
					"parameter_longname" : "zeroButton",
					"parameter_modmode" : 0,
					"parameter_shortname" : "Zero",
					"parameter_type" : 2,
					"parameter_unitstyle" : 10
				}
,
				"obj-4::obj-9" : 				{
					"parameter_longname" : "RammTime",
					"parameter_shortname" : "secs",
					"parameter_unitstyle" : 1
				}
,
				"obj-6::obj-3" : 				{
					"parameter_longname" : "live.menu",
					"parameter_shortname" : "live.menu"
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "ASCII-split.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "OSC-routing.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "PTN-control.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.args.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.arithmser.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.collect.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.eq.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.expr.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.ezmidiplay.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.f2mc.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.filter.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.filternull.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.flat.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.gcd.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.geq.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.is.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.iter.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.keys.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.length.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.mc2f.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.mcapprox.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.neq.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.nth.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.pick.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.portal.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.print.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.reg.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.roll.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.times.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.unpacknote.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.wrap.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "cellblock2coll.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "cellblock_cleanup-format.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "cellblock_mirrored.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "cellblock_natural.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "divmod.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "json_function.js",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/code",
				"patcherrelativepath" : "../code",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "json_parser.js",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/code",
				"patcherrelativepath" : "../code",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "midiHz-decode.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "midiHz-encode.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "pb_decode.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "pb_encode.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "polytempo-helper.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "shear_depth.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "shear_kslider.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "shear_roll.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/[Polytempo]/TransForms/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "ksliderWhite",
				"default" : 				{
					"color" : [ 1, 1, 1, 1 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-1",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-1",
				"default" : 				{
					"accentcolor" : [ 0, 0.533333, 0.168627, 1 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
