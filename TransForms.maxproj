{
	"name" : "TransForms",
	"version" : 1,
	"creationdate" : 3700916569,
	"modificationdate" : 3730125689,
	"viewrect" : [ 2770.0, 296.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"TransForms.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"shear_kslider.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"shear_roll.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"cellblock_cleanup-format.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"cellblock2coll.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"cellblock_mirrored.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"cellblock_natural.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"shear_depth.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"PTN-control.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"pb_decode.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"pb_encode.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"midiHz-encode.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"midiHz-decode.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"ASCII-split.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"PTC-control.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"polytempo-helper.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}
,
		"code" : 		{
			"json_parser.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"json_function.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}

		}
,
		"externals" : 		{

		}
,
		"other" : 		{
			"appicon" : 			{
				"kind" : "file",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 1835887981,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
